using UnityEngine;
using System.Collections;

public class PlanetsSunMovements : MonoBehaviour {
	public float ratio=0.001f;
	public GameObject sun;
	public GameObject merkur;
	public GameObject venus;
	public GameObject earth;
	public GameObject mars;
	public GameObject jupiter;
	public GameObject saturn;
	public GameObject uranus;
	public GameObject neptun;
	public GameObject pluto;
	
	public float speedOfMerkur;
	public float speedOfVenus;
	public float speedOfEarth;
	public float speedOfMars;
	public float speedOfJupiter;
	public float speedOfSaturn;
	public float speedOfUranus;
	public float speedOfNeptun;
	public float speedOfPluto;	
	
	// Use this for initialization
	void Start () {
		//Set the default positions of the planets
        merkur.transform.RotateAround(sun.transform.position, Vector3.down, 30f);
        venus.transform.RotateAround(sun.transform.position, Vector3.down, -30f);
        mars.transform.RotateAround(sun.transform.position, Vector3.down, 60f);
        jupiter.transform.RotateAround(sun.transform.position, Vector3.down, 45f);
        saturn.transform.RotateAround(sun.transform.position, Vector3.down, 180f);
        uranus.transform.RotateAround(sun.transform.position, Vector3.down, -20f);
        neptun.transform.RotateAround(sun.transform.position, Vector3.down, -60);
	}
	
	// Update is called once per frame
	void Update () {
       PlanetsMovement();
	}

    private void PlanetsMovement()
    {
        merkur.transform.RotateAround(sun.transform.position, Vector3.down, speedOfMerkur * Time.deltaTime * ratio);
        venus.transform.RotateAround(sun.transform.position, Vector3.down, speedOfVenus * Time.deltaTime * ratio);
        earth.transform.RotateAround(sun.transform.position, Vector3.down, speedOfEarth * Time.deltaTime * ratio);
        mars.transform.RotateAround(sun.transform.position, Vector3.down, speedOfMars * Time.deltaTime * ratio);
        jupiter.transform.RotateAround(sun.transform.position, Vector3.down, speedOfJupiter * Time.deltaTime * ratio);
        saturn.transform.RotateAround(sun.transform.position, Vector3.down, speedOfSaturn * Time.deltaTime * ratio);
        uranus.transform.RotateAround(sun.transform.position, Vector3.down, speedOfUranus * Time.deltaTime * ratio);
        neptun.transform.RotateAround(sun.transform.position, Vector3.down, speedOfNeptun * Time.deltaTime * ratio);
    }
}
