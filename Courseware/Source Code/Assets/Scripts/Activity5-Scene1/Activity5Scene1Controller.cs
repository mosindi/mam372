﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;

public class Activity5Scene1Controller : MonoBehaviour
{

    public TabletController tabletController;
    public GameObject speechBubble;
    public Text speechBubbleText;

    public Animator agentAnim;
    public Animator camAnim;
    public GameObject ToolContainer;
    [Range(0, 10)]
    public float textChangeSpeed;
    [Range(0, 10)]
    public float lunchCountDown;

    public Button IntroPassBtn;

    public Button CheckandApproveBtn;




    Activity5Scene1MessageController sceneMessageController;
    private int agentAnimParamStartId;
    private int activeTaskID = -1;
    private string agentAnimParamStart = "Interaction";
    private Coroutine IntroInteractionCoroutine;


    void Start()
    {
        TaskGenerate();
        sceneMessageController = GetComponent<Activity5Scene1MessageController>();
        HelperController.GameController.isPlayable = false;
        agentAnimParamStartId = Animator.StringToHash(agentAnimParamStart);
        //camAnimParamId = Animator.StringToHash(camAnimParam);
        speechBubble.SetActive(false);
        IntroInteractionCoroutine = StartCoroutine(IntroSpeakInteractionCoroutine());
    }

    private IEnumerator IntroSpeakInteractionCoroutine()
    {
        IntroPassBtn.gameObject.SetActive(true);
        agentAnim.gameObject.SetActive(true);
        yield return new WaitForSeconds(1.2f);
        speechBubble.SetActive(true);
        agentAnim.SetBool(agentAnimParamStartId, true);
        HelperController.GameController.isPlayable = false;
        speechBubbleText.text = "  Ekipteki diğer bilim insanları Mars hakkındaki araştırmalarını sürdürüyorlar.";
        yield return new WaitWhile(() => speechBubble.activeSelf); //yield return new WaitForSeconds(HelperController.SpeechbublpeWaitTime(speechBubbleText.text, textChangeSpeed));

        agentAnim.SetBool(agentAnimParamStartId, false);
        speechBubble.SetActive(false);
        IntroPassBtn.gameObject.SetActive(false);
        HelperController.GameController.isPlayable = true;
        StartCoroutine(SpeakInteractionCoroutine(1));
    }
    public void OnClickPassIntro()
    {

        IntroPassBtn.gameObject.SetActive(false);
        StopCoroutine(IntroInteractionCoroutine);
        StartCoroutine(SpeakInteractionCoroutine(1));
    }

    private IEnumerator SpeakInteractionCoroutine(int interactionID)
    {
        InteractionObjectsActive(true);
        if (interactionID == 1)
        {
            speechBubbleText.text = "Araştırmar tamamlanınca dünya'ya dönüp motorlar hakkında gelecek görevlerde kullanılmak üzere rapor hazırlamalısın.";
            yield return new WaitWhile(() => speechBubble.activeSelf); //yield return new WaitForSeconds(HelperController.SpeechbublpeWaitTime(speechBubbleText.text, textChangeSpeed));
            tabletController.SwitcOn(TabletScreens.Activity4Scene2);
            NextTask();
        }
        InteractionObjectsActive(false);

    }
    private void InteractionObjectsActive(bool status)
    {
        agentAnim.gameObject.SetActive(status);
        speechBubble.SetActive(status);
        agentAnim.SetBool(agentAnimParamStartId, status);
        HelperController.GameController.isPlayable = !status;
    }


    #region TaskOps
    public void NextTask()
    {
        StopAllCoroutines();
        if (activeTaskID + 1 < taskList.Count)
        {
            activeTaskID++;
            //  StartCoroutine(NextTaskInteraction());
            sceneMessageController.ShowMessage(HelperController.MessageType.Task, activeTaskID, HelperController.ButtonType.ClosePanel, true);
        }
        else
        {
            Debug.Log("NextSection");
            CheckandApproveBtn.gameObject.SetActive(true);
            CheckandApproveBtn.interactable = false;
            NextSection();


        }
    }

    public void TaskStarted()
    {

        HelperController.GameController.Playable(true);
        StartCoroutine(TaskProcess());
    }

    private IEnumerator TaskProcess()
    {
        switch (activeTaskID)
        {
            case 0:
                float _s = 0.1f;
                while (_s <= 0.7f)
                {

                    sceneMessageController.ShowMessage(HelperController.MessageType.Task, _s, HelperController.ButtonType.Continue, false);
                    yield return new WaitForSeconds(lunchCountDown);
                    _s += 0.05f;
                    _s = float.Parse(_s.ToString("0.00"));
                }

                //InteractionObjectsActive(true);
                yield return new WaitForEndOfFrame();
                TaskComleted();
                break;
            default:
                break;
        }
    }

    public void TaskComleted()
    {
        StopAllCoroutines();
        sceneMessageController.ShowMessage(HelperController.MessageType.Success, 1, HelperController.ButtonType.ClosePanel, true);
    }

    public void RestartTask()
    {
        if (activeTaskID > -1)
        {
            StopAllCoroutines();
            activeTaskID--;
            NextTask();
        }

    }
    #endregion
    #region Section
    public void NextSection()
    {
        StopAllCoroutines();
        StartCoroutine(NextSectionProcess());
    }

    private IEnumerator NextSectionProcess()
    {
        InteractionObjectsActive(true);
        speechBubbleText.text = "Tüm Araştırmalar tamamlandı artık dünyaya dönebilirsin.";
        yield return new WaitWhile(() => speechBubble.activeSelf); //yield return new WaitForSeconds(HelperController.SpeechbublpeWaitTime(speechBubbleText.text, textChangeSpeed));
        InteractionObjectsActive(false);
        CheckandApproveBtn.interactable = true;

        //sceneMessageController.ShowMessage(HelperController.MessageType.Section, 1f, HelperController.ButtonType.ClosePanel, false);

    }
    public void NextSectionGo()
    {
        StopAllCoroutines();
        HelperController.GameController.sceneController.LoadNextScene();
    }
    #endregion
    #region List
    public List<TaskController.TaskStruct> taskList = new List<TaskController.TaskStruct>();
    private void TaskGenerate()
    {
        taskList.Add(new TaskController.TaskStruct("Araştırmalar tamamlanınca dünyaya dön"));
        HelperController.TaskController.TaskListFillOnSceneStart(taskList);

    }
    #endregion
    #region Helper
    public float SpeechbublpeWaitTime()
    {
        return (speechBubbleText.text.Length * Time.deltaTime / textChangeSpeed) + 1.5f;
    }
    #endregion
}
