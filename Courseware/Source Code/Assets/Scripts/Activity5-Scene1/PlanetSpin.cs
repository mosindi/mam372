﻿using UnityEngine;
using System.Collections;

public class PlanetSpin : MonoBehaviour
{
    //public PlanetsSunMovements psm;
    public float ratio = 0.1f;

    public enum Direction
    {
        Up,
        Down,
        Right,
        Left
    }

    public Direction direction;
    public float speed;

    
    
    // Update is called once per frame
    void Update()
    {
        switch (direction)
        {
            case Direction.Up:
                gameObject.GetComponent<Transform>().Rotate(Vector3.up, speed * ratio * 10f * Time.deltaTime);
                break;
            case Direction.Down:
                gameObject.GetComponent<Transform>().Rotate(Vector3.down, speed * ratio * 10f * Time.deltaTime);
                break;
            case Direction.Right:
                gameObject.GetComponent<Transform>().Rotate(Vector3.right, speed * ratio * 10f * Time.deltaTime);
                break;
            case Direction.Left:
                gameObject.GetComponent<Transform>().Rotate(Vector3.left, speed * ratio * 10f * Time.deltaTime);
                break;
            default:
                break;
        }
    }
}
