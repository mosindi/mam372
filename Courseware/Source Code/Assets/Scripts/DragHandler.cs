﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;

public class DragHandler : MonoBehaviour, IBeginDragHandler , IDragHandler,IEndDragHandler {
    public static GameObject itemBeingDragged;

    public GameObject root;
    public Canvas canvas;
    public bool is_dragable = true;
    RectTransform recTransform;
    Vector3 startPosition;
    Transform startParent;
    public void Start()
    {
        recTransform = GetComponent<RectTransform>();
    }
    public void OnBeginDrag(PointerEventData eventData)
    {
        if (is_dragable && HelperController.GameController.isPlayable)
        {
            itemBeingDragged = gameObject;
            startPosition = transform.position;
            startParent = transform.parent;
           
            itemBeingDragged.transform.SetParent(root.transform);
            GetComponent<CanvasGroup>().blocksRaycasts = false;   
        }
    }

    public void OnDrag(PointerEventData eventData)
    {
        if (is_dragable && HelperController.GameController.isPlayable)
        {

            if (Input.GetMouseButton(0))
            {

                if (canvas.renderMode == RenderMode.ScreenSpaceOverlay)
                {
                    Vector3 new_position = new Vector3(Input.mousePosition.x, Input.mousePosition.y);
                    Camera.main.ScreenToWorldPoint(new_position);
                    recTransform.localPosition = new_position;
                    transform.position = Input.mousePosition;
                }

                if (canvas.renderMode == RenderMode.ScreenSpaceCamera)
                {
                    Vector2 pos;
                    RectTransformUtility.ScreenPointToLocalPointInRectangle(canvas.transform as RectTransform, Input.mousePosition, canvas.worldCamera, out pos);
                    recTransform.localPosition = pos;
                }

            }
        }
    }

    public void OnEndDrag(PointerEventData eventData)
    {
        if (is_dragable && HelperController.GameController.isPlayable)
        {
            itemBeingDragged = null;
            GetComponent<CanvasGroup>().blocksRaycasts = true;
            if (transform.parent == root.transform)
            {
                transform.SetParent(startParent);
            }
            if (transform.parent == startParent)
            {
                transform.position = startPosition;
            }
        }
    }
}
