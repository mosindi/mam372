﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System;
using System.Collections.Generic;

public class Activity4Scene1Controller : MonoBehaviour
{

    public TabletController tabletController;
    public GameObject speechBubble;
    public Text speechBubbleText;
    public Animator agentAnim;
    public Animator camAnim;
    public GameObject ToolContainer;
    [Range(0, 10)]
    public float textChangeSpeed;
    public Button IntroPassBtn;
    public HudMessageStruct taskMessagePanel;

    Activity3Scene1MessageController sceneMessageController;
    private int agentAnimParamStartId;
    private int activeTaskID = -1;
    private string agentAnimParamStart = "Interaction";
    private string camAnimParam = "OnBoard";
    private int camAnimParamId;
    private Coroutine IntroInteractionCoroutine;
    // Use this for initialization
    void Start()
    {
        TaskGenerate();
        sceneMessageController = GetComponent<Activity3Scene1MessageController>();
        HelperController.GameController.isPlayable = false;
        agentAnimParamStartId = Animator.StringToHash(agentAnimParamStart);
        camAnimParamId = Animator.StringToHash(camAnimParam);
        speechBubble.SetActive(false);
        IntroInteractionCoroutine = StartCoroutine(IntroSpeakInteractionCoroutine());
    }

    private IEnumerator IntroSpeakInteractionCoroutine()
    {
        agentAnim.gameObject.SetActive(true);
        speechBubble.SetActive(true);
        agentAnim.SetBool(agentAnimParamStartId, true);
        HelperController.GameController.isPlayable = false;

        speechBubbleText.text = "Tüm hazırlıklar tamam. Artık fırlatmaya dakikalar kaldı. Senin de gemiye binip uçuş öncesi son hazırlıkları yapman gerekiyor.";
        yield return new WaitWhile(() => speechBubble.activeSelf);
       // yield return new WaitForSeconds(HelperController.SpeechbublpeWaitTime(speechBubbleText.text, textChangeSpeed));


        agentAnim.SetBool(agentAnimParamStartId, false);
        speechBubble.SetActive(false);
        IntroPassBtn.gameObject.SetActive(false);
        HelperController.GameController.isPlayable = true;
        StartCoroutine(SpeakInteractionCoroutine());

    }
    #region Intro
    private IEnumerator SpeakInteractionCoroutine()
    {
        yield return new WaitForSeconds(0.1f);
        agentAnim.gameObject.SetActive(true);
        speechBubble.SetActive(true);
        agentAnim.SetBool(agentAnimParamStartId, true);
        HelperController.GameController.isPlayable = false;

        speechBubbleText.text = "Hadi şimdi fırlatma kulesine gidip gemiye bin.";
        yield return new WaitWhile(() => speechBubble.activeSelf);
        // yield return new WaitForSeconds(HelperController.SpeechbublpeWaitTime(speechBubbleText.text, textChangeSpeed));


        HelperController.GameController.isPlayable = true;
        speechBubble.SetActive(false);
        agentAnim.SetBool(agentAnimParamStartId, false);
        ShowTask(0);


    }
    public void OnClickPassIntro()
    {

        IntroPassBtn.gameObject.SetActive(false);
        StopCoroutine(IntroInteractionCoroutine);
        StartCoroutine(SpeakInteractionCoroutine());
    }
    #endregion
    private void ShowTask(int id)
    {
        activeTaskID = id;
        if (id == 0)
        {
           
            taskMessagePanel.Text.text = "Mekik'e binmek için fırlatma kulesine gitmelisin!";
            taskMessagePanel.ButtonLabel.text = "Git!";
            taskMessagePanel.Button.onClick.RemoveAllListeners();
            taskMessagePanel.Button.onClick.AddListener(delegate
            {
                taskMessagePanel.Button.transform.parent.gameObject.SetActive(false);
                camAnim.SetBool(camAnimParamId, true);
                HelperController.GameController.isPlayable = false;
                StartCoroutine(WaitForGoToLunchTower());
            });
            taskMessagePanel.Button.transform.parent.gameObject.SetActive(true);
        }
        if (id == 1)
        {
            taskMessagePanel.Text.text = "Uçuş görevi için mekik'e binmelisin.";
            taskMessagePanel.ButtonLabel.text = "Mekik'e Bin!";
            taskMessagePanel.Button.onClick.RemoveAllListeners();
            taskMessagePanel.Button.onClick.AddListener(delegate
            {
                taskMessagePanel.Button.transform.parent.gameObject.SetActive(false);
                NextSectionGo();

            });
            taskMessagePanel.Button.transform.parent.gameObject.SetActive(true);

        }

    }

    private IEnumerator WaitForGoToLunchTower()
    {
        yield return new WaitForSeconds(3);
        HelperController.TaskController.TaskComplete(activeTaskID);
        HelperController.GameController.isPlayable = true;
        ShowTask(1);
    }

    public void NextSectionGo()
    {
        StopAllCoroutines();
        Debug.Log("nextsection");
        HelperController.GameController.sceneController.LoadNextScene();
    }
    #region List
    public List<TaskController.TaskStruct> taskList = new List<TaskController.TaskStruct>();
    private void TaskGenerate()
    {
        taskList.Add(new TaskController.TaskStruct("Mekik fırlatma kulesine git."));
        HelperController.TaskController.TaskListFillOnSceneStart(taskList);

    }
    #endregion
    #region Helper
    public float SpeechbublpeWaitTime()
    {
        return (speechBubbleText.text.Length * Time.deltaTime / textChangeSpeed) + 1.5f;
    }
    #endregion

}
