﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;

public class Activity2Scene1MessageController : MonoBehaviour
{
    public Text HudBottomMessageText;
    public HudMessageStruct taskMessagePanel;
    public HudMessageStruct successMessagePanel;
    public HudMessageStruct FailulerMessagePanel;

    // Use this for initialization
    void Start()
    {
        HudBottomMessageText.text = "";
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void ShowMessage(HelperController.MessageType mType, float id = 0, HelperController.ButtonType buttonEvent = HelperController.ButtonType.ClosePanel, bool notification = false)
    {

        switch (mType)
        {
            case HelperController.MessageType.Task:
                if (ActivityMessageListTask.Count > id)
                {
                    //   HelperController.GameController.isPlayable = false;
                    if (notification)
                    {
                        ShowNewTaskNotify((int)id);
                    }
                    HudBottomMessageText.text = ActivityMessageListTask[id];
                }

                break;
            case HelperController.MessageType.Warning:
                break;
            case HelperController.MessageType.Success:
                ShowSuccessNotify((int)id);
                HudBottomMessageText.text = "";
                break;
            case HelperController.MessageType.Failure:
                ShowFaillureNotify((int)id);
                break;
            case HelperController.MessageType.Section:
                ShowSectionNotify((int)id);
                HudBottomMessageText.text = ActivityMessageListSection[id];
                break;
            default:
                break;
        }
        AddEventListenerToButton(mType, buttonEvent);

    }
    private void AddEventListenerToButton(HelperController.MessageType mType, HelperController.ButtonType buttonEvent = HelperController.ButtonType.ClosePanel)
    {
        switch (mType)
        {
            #region Task
            case HelperController.MessageType.Task:
                taskMessagePanel.Button.onClick.RemoveAllListeners();
                switch (buttonEvent)
                {

                    case HelperController.ButtonType.ClosePanel:
                        taskMessagePanel.Button.onClick.AddListener(delegate
                        {
                            taskMessagePanel.Button.transform.parent.gameObject.SetActive(false);
                            HelperController.GameController.isPlayable = true;
                            gameObject.BroadcastMessage("TaskStarted");
                        });
                        break;
                    case HelperController.ButtonType.NextSection:
                        taskMessagePanel.Button.onClick.AddListener(delegate
                        {
                            taskMessagePanel.Button.transform.parent.gameObject.SetActive(false);
                            HelperController.GameController.isPlayable = true;
                            //  Debug.Log("Task Message Pane Button");
                            HelperController.GameController.sceneController.LoadNextScene();
                        });

                        break;
                    case HelperController.ButtonType.Continue:
                        taskMessagePanel.Button.onClick.AddListener(delegate
                        {
                            taskMessagePanel.Button.transform.parent.gameObject.SetActive(false);
                            HelperController.GameController.isPlayable = true;
                            //   gameObject.BroadcastMessage("TaskStarted");
                        });
                        break;
                    default:
                        break;
                }
                break;
            #endregion
            case HelperController.MessageType.Warning:
                break;
            case HelperController.MessageType.Success:
                successMessagePanel.Button.onClick.RemoveAllListeners();
                switch (buttonEvent)
                {
                    case HelperController.ButtonType.ClosePanel:
                        successMessagePanel.Button.onClick.AddListener(delegate
                        {
                            successMessagePanel.Button.transform.parent.gameObject.SetActive(false);
                            HelperController.GameController.isPlayable = true;
                            gameObject.BroadcastMessage("NextTask");
                        });
                        break;
                    case HelperController.ButtonType.NextSection:
                        successMessagePanel.Button.onClick.AddListener(delegate
                        {
                            successMessagePanel.Button.transform.parent.gameObject.SetActive(false);
                            HelperController.GameController.isPlayable = true;
                            HelperController.GameController.sceneController.LoadNextScene();
                        });

                        break;
                    case HelperController.ButtonType.Continue:
                        successMessagePanel.Button.onClick.AddListener(delegate
                        {
                            successMessagePanel.Button.transform.parent.gameObject.SetActive(false);
                            HelperController.GameController.isPlayable = true;

                        });
                        break;
                    default:
                        break;
                }
                break;
            case HelperController.MessageType.Failure:
                switch (buttonEvent)
                {
                    case HelperController.ButtonType.ClosePanel:
                        FailulerMessagePanel.Button.onClick.AddListener(delegate
                        {
                            FailulerMessagePanel.Button.transform.parent.gameObject.SetActive(false);
                            HelperController.GameController.isPlayable = true;
                            BroadcastMessage("RestartMission");
                        });

                        break;
                    case HelperController.ButtonType.HudToolBox:
                        break;
                    case HelperController.ButtonType.NextSection:
                        break;
                    default:
                        break;
                }

                break;
            case HelperController.MessageType.Section:
                taskMessagePanel.Button.onClick.RemoveAllListeners();
                switch (buttonEvent)
                {

                    case HelperController.ButtonType.ClosePanel:
                        taskMessagePanel.Button.onClick.AddListener(delegate
                        {
                            taskMessagePanel.Button.transform.parent.gameObject.SetActive(false);
                            HelperController.GameController.isPlayable = true;
                            gameObject.BroadcastMessage("NextSectionGo");
                        });
                        break;
                    case HelperController.ButtonType.NextSection:
                        taskMessagePanel.Button.onClick.AddListener(delegate
                        {
                            taskMessagePanel.Button.transform.parent.gameObject.SetActive(false);
                            HelperController.GameController.isPlayable = true;
                            //  Debug.Log("Task Message Pane Button");
                            HelperController.GameController.sceneController.LoadNextScene();
                        });

                        break;
                    default:
                        break;
                }
                break;
            default:
                break;
        }
    }

    private void ShowNewTaskNotify(int id)
    {
        taskMessagePanel.Button.transform.parent.gameObject.SetActive(true);
        taskMessagePanel.Text.text = ActivityMessageListTask[id];
        HelperController.GameController.isPlayable = false;

    }
    private void ShowSuccessNotify(int id)
    {
        successMessagePanel.Button.transform.parent.gameObject.SetActive(true);
        successMessagePanel.Text.text = ActivityMessageListSuccess[id];
        HelperController.GameController.isPlayable = false;

    }
    private void ShowFaillureNotify(int id)
    {
        FailulerMessagePanel.Button.transform.parent.gameObject.SetActive(true);
        FailulerMessagePanel.Text.text = ActivityMessageListFailure[id];
        HelperController.GameController.isPlayable = false;

    }
    private void ShowSectionNotify(int id)
    {
        taskMessagePanel.Button.transform.parent.gameObject.SetActive(true);
        taskMessagePanel.Text.text = ActivityMessageListSection[id];
        HelperController.GameController.isPlayable = false;
    }


    #region Lists
    #region Task
    public static Dictionary<float, string> ActivityMessageListTask = new Dictionary<float, string>()
    {
         {0.0f,"Fırlatma Motoru A-3'ü test et ve hızını, aldığı yolu, ve geçen zamanı tableti kullanarak sisteme gir." },
         {0.1f,"Önce ölçüm çihazlarını seçmek için Alet Çantasına tıklayarak Alet Çantasını aç ve Ölçüm Cihazlarını seç." },
         {0.2f,"Fırlatma A-3 Taşıyıcısına tıkla ve motoru test aracına yükle." },
         {0.3f,"Test kontrol ünitesinden testi başlat ve testi izle. Tekrar et'e basarak testi tekrarlayabilirsin. \nSoru : Motorun hızını, aldığı yolu, geçen zamanı ölçüm cihazından oku ve tablete gir." },
         {0.4f,"Ölçüm cihazından HIZI, ALDIĞI YOLU ve GEÇEN ZAMANI oku ve Tablet'i aç ve ve sisteme gir." },



         {1.0f,"Fırlatma Motoru B-2'yi test et ve hızını, aldığı yolu, ve geçen zamanı tableti kullanarak sisteme gir." },
         {1.1f,"Önce ölçüm çihazlarını seçmek için Alet Çantasına tıklayarak Alet Çantasını aç ve Ölçüm Cihazlarını seç." },
         {1.2f,"Fırlatma Motoru B-2'yi test et. Aldığı yolu ve geçen zamanı ölçüm cihazından oku hızı hesapla ve tableti kullanarak sisteme gir." },
         {1.3f,"Yol Ölçer'i ve Zaman Ölçer'i seçmek için alet çantasına tıklayarak Alet Çantasını aç ve Ölçüm Cihazlarını seç." },
         {1.4f,"Fırlatma B-2 Taşıyıcısına tıkla ve motoru test aracına yükle." },
         {1.5f,"Test kontrol ünitesinden testi başlat ve testi izle. Tekrar et'e basarak testi tekrarlayabilirsin. \nSoru : Motorun hızını, aldığı yolu, geçen zamanı ölçüm cihazından oku ve tablete gir." },
         {1.6f,"Ölçüm cihazından ALDIĞI YOLU ve GEÇEN ZAMANI oku ve Tablet'i aç ve ve sisteme gir. Hız Ölçüm Cihazından değeri okuyamadığın için hızıda sen hesaplamalısın." },



         {2.0f,"Fırlatma motoru C- 2'yi test et ve hızını, aldığı yolu, ve geçen zamanı tableti kullanarak sisteme gir." },
         {2.1f,"Önce ölçüm çihazlarını seçmek için Alet Çantasına tıklayarak Alet Çantasını aç ve Yol Ölçer'i ve Zaman Ölçer'i seç." },
         {2.2f,"Fırlatma C-2 Taşıyıcısına tıkla ve motoru test aracına yükle." },
         {2.3f,"Test kontrol ünitesinden testi başlat ve testi izle. Tekrar et'e basarak testi tekrarlayabilirsin. \nSoru : Motorun hızını, aldığı yolu, geçen zamanı ölçüm cihazından oku ve tablete gir." },
         {2.4f,"Ölçüm cihazından ALDIĞI YOLU ve GEÇEN ZAMANI oku ve Tablet'i aç ve ve sisteme gir. Hız Ölçüm Cihazından değeri okuyamadığın için hızıda sen hesaplamalısın." },

         {3.0f,"Mekik Ana Motoru A-1'i test et ve hızını, aldığı yolu, ve geçen zamanı tableti kullanarak sisteme gir." },
         {3.1f,"Önce ölçüm çihazlarını seçmek için Alet Çantasına tıklayarak Alet Çantasını aç ve Hız Ölçer'i ve Zaman Ölçer'i seç." },
         {3.2f,"Mekik A-1 Taşıyıcısına tıkla ve motoru test aracına yükle." },
         {3.3f,"Test kontrol ünitesinden testi başlat ve testi izle. Tekrar et'e basarak testi tekrarlayabilirsin. \nSoru : Motorun hızını, aldığı yolu, geçen zamanı ölçüm cihazından oku ve tablete gir." },
         {3.4f,"Ölçüm cihazından HIZI ve GEÇEN ZAMANI oku ve Tablet'i aç ve ve sisteme gir. Yol Ölçüm Cihazından değeri okuyamadığın için aldığı yoluda sen hesaplamalısın." },

         {4.0f,"Mekik Ana Motoru B-1'i test et ve hızını, aldığı yolu, ve geçen zamanı tableti kullanarak sisteme gir." },
         {4.1f,"Önce ölçüm çihazlarını seçmek için Alet Çantasına tıklayarak Alet Çantasını aç ve Hız Ölçer'i ve Zaman Ölçer'i  seç." },
         {4.2f,"Mekik B-1 Taşıyıcısına tıkla ve motoru test aracına yükle." },
         {4.3f,"Test kontrol ünitesinden testi başlat ve testi izle. Tekrar et'e basarak testi tekrarlayabilirsin. \nSoru : Motorun hızını, aldığı yolu, geçen zamanı ölçüm cihazından oku ve tablete gir." },
         {4.4f,"Ölçüm cihazından HIZI ve GEÇEN ZAMANI oku ve Tablet'i aç ve ve sisteme gir. Yol Ölçüm Cihazından değeri okuyamadığın için aldığı yoluda sen hesaplamalısın." },

         {5.0f,"Mekik Ana Motoru C-1'i test et ve hızını, aldığı yolu, ve geçen zamanı tableti kullanarak sisteme gir." },
         {5.1f,"Önce ölçüm çihazlarını seçmek için Alet Çantasına tıklayarak Alet Çantasını aç Hız Ölçer'i ve Zaman Ölçer'i  Cihazlarını seç." },
         {5.2f,"Mekik C-1 Taşıyıcısına tıkla ve motoru test aracına yükle." },
         {5.3f,"Test kontrol ünitesinden testi başlat ve testi izle. Tekrar et'e basarak testi tekrarlayabilirsin. \nSoru : Motorun hızını, aldığı yolu, geçen zamanı ölçüm cihazından oku ve tablete gir." },
         {5.4f,"Ölçüm cihazından HIZI ve GEÇEN ZAMANI oku ve Tablet'i aç ve ve sisteme gir. Yol Ölçüm Cihazından değeri okuyamadığın için aldığı yoluda sen hesaplamalısın." },

         {6.0f," Manevra Motoru A-2'yi test et ve hızını, aldığı yolu, ve geçen zamanı tableti kullanarak sisteme gir." },
         {6.1f,"Önce ölçüm çihazlarını seçmek için Alet Çantasına tıklayarak Alet Çantasını aç Hız Ölçer'i ve Yol Ölçer'i seç." },
         {6.2f,"Manevra A-2 Taşıyıcısına tıkla ve motoru test aracına yükle." },
         {6.3f,"Test kontrol ünitesinden testi başlat ve testi izle. Tekrar et'e basarak testi tekrarlayabilirsin. \nSoru : Motorun hızını, aldığı yolu, geçen zamanı ölçüm cihazından oku ve tablete gir." },
         {6.4f,"Ölçüm cihazından HIZI ve aldığı YOLU oku. Tablet'i aç ve ve sisteme gir. Zaman Ölçüm Cihazından değeri okuyamadığın için geçen zamanıda sen hesaplamalısın." },

         {7.0f," Manevra Motoru B-3'yi test et ve hızını, aldığı yolu, ve geçen zamanı tableti kullanarak sisteme gir." },
         {7.1f,"Önce ölçüm çihazlarını seçmek için Alet Çantasına tıklayarak Alet Çantasını aç Hız Ölçer'i ve Yol Ölçer'i seç." },
         {7.2f,"Manevra B-3 Taşıyıcısına tıkla ve motoru test aracına yükle." },
         {7.3f,"Test kontrol ünitesinden testi başlat ve testi izle. Tekrar et'e basarak testi tekrarlayabilirsin. \nSoru : Motorun hızını, aldığı yolu, geçen zamanı ölçüm cihazından oku ve tablete gir." },
         {7.4f,"Ölçüm cihazından HIZI ve yolu YOLU oku. Tablet'i aç ve ve sisteme gir. Zaman Ölçüm Cihazından değeri okuyamadığın için geçen zamanıda sen hesaplamalısın." },

        { 8.0f," Manevra Motoru C-3'yi test et ve hızını, aldığı yolu, ve geçen zamanı tableti kullanarak sisteme gir." },
         {8.1f,"Önce ölçüm çihazlarını seçmek için Alet Çantasına tıklayarak Alet Çantasını aç Hız Ölçer'i ve Yol Ölçer'i seç." },
         {8.2f,"Manevra C-3 Taşıyıcısına tıkla ve motoru test aracına yükle." },
         {8.3f,"Test kontrol ünitesinden testi başlat ve testi izle. Tekrar et'e basarak testi tekrarlayabilirsin. \nSoru : Motorun hızını, aldığı yolu, geçen zamanı ölçüm cihazından oku ve tablete gir." },
         {8.4f,"Ölçüm cihazından HIZI ve aldığı YOLU oku. Tablet'i aç ve ve sisteme gir. Zaman Ölçüm Cihazından değeri okuyamadığın için geçen zamanıda sen hesaplamalısın." },

    };
    #endregion
    #region Success
    public static Dictionary<int, string> ActivityMessageListSuccess = new Dictionary<int, string>()
    {
        {0,"Doğru cevap tebrikler!" },
        {1,"Görevi başarı ile tamamladın, devam edebiliriz." },
        {2,"Tüm Görevleri başarı ile tamamladın, devam edebiliriz." },
    };
    #endregion
    #region Failure
    public static Dictionary<int, string> ActivityMessageListFailure = new Dictionary<int, string>()
    {
        {0,"Girdiğin değerler doğru değil. Daha dikkatli ol" },
        {1,"Yalnızca rakamlardan oluşan değer gir" },
        {2,"" },
    };
    #endregion
    #region Speech
    public static Dictionary<int, string> ActivityMessageListSpeech = new Dictionary<int, string>()
    {
        {0,"Merhaba" },
        {1,"" },
        {2,"" },
    };
    #endregion
    #region Section
    public static Dictionary<float, string> ActivityMessageListSection = new Dictionary<float, string>()
    {
        {0,"" },
        {1,"Uygun motorları mekiğe montajı için hangara dön."}
    };
    #endregion
    #endregion



}
