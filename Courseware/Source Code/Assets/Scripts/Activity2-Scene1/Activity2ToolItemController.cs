﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Activity2ToolItemController : MonoBehaviour
{
    public MeasurementToolType Type;
    public bool canHover = true;
    public GameObject hoverBig;
    public Text panelTxt;
    public Text HoverPanelTxt;
    public float measuredValue;
    

    // Use this for initialization
    void Start()
    {
        measuredValue = 0;
        panelTxt.text = "---";
    }
    
    // Update is called once per frame
    void Update()
    {
        panelTxt.text = measuredValue.ToString();
        HoverPanelTxt.text = panelTxt.text;
    }

    void OnMouseEnter()
    {
        if (HelperController.GameController.isPlayable && canHover)
        { 
          hoverBig.SetActive(true);
        }
    }
    void OnMouseExit()
    {
        if (HelperController.GameController.isPlayable && canHover)
        {
            hoverBig.SetActive(false);
        }

    }

   
}
