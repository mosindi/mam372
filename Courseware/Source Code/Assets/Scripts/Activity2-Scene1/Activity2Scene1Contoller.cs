﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;
using System;

public class Activity2Scene1Contoller : MonoBehaviour
{

    #region variables
    public TabletController tabletController;
    public GameObject speechBubble;
    public Text speechBubbleText;
    public Animator agentAnim;
    public List<GameObject> MotorsGroupA;
    public List<GameObject> MotorsGroupB;
    public List<GameObject> MotorsGroupC;
    public TrafficLightController trafficLightController;

    public Activity2ForkliftController[] ForkliftController;
    public GameObject ToolContainer;
    public GameObject taskControlDevice;

    public Button TestStartBtn;
    public Button TestRepeatBtn;
    [Range(0, 10)]
    public float textChangeSpeed;
    public Button IntroPassBtn;



    Activity2Scene1MessageController sceneMessageController;
    Activity2ToolController toolController;
    private int agentAnimParamStartId;
    private int activeTaskID = -1;
    private string agentAnimParamStart = "Interaction";
    private Coroutine IntroInteractionCoroutine;
    private bool isTesting = false;
    private Vector3 crawlerPos;

    #endregion
    // Use this for initialization
    void Start()
    {
        TaskGenerate();

        sceneMessageController = GetComponent<Activity2Scene1MessageController>();
        toolController = ToolContainer.GetComponent<Activity2ToolController>();
        tabletController.SwitcOn(TabletScreens.Default);
        HelperController.GameController.isPlayable = false;
        agentAnimParamStartId = Animator.StringToHash(agentAnimParamStart);
        speechBubble.SetActive(false);
        SetForkliftActive(-1, false); // -1 tammamı oluyor
        taskControlDevice.SetActive(false);
        ToolContainer.SetActive(false);
        IntroInteractionCoroutine = StartCoroutine(IntroSpeakInteractionCoroutine());
    }

    // Update is called once per frame
    void Update()
    {

    }
    #region Intro
    private IEnumerator IntroSpeakInteractionCoroutine()
    {
        agentAnim.gameObject.SetActive(true);
        speechBubble.SetActive(true);
        agentAnim.SetBool(agentAnimParamStartId, true);
        HelperController.GameController.isPlayable = false;
        speechBubbleText.text = "Şimdi yapman gereken sınıflara ayırdığın motorlardan birer tane seçmek. Sectiğin motor mekik de kullanılacak diğer iki motor ise yedek olarak hazırda bekletilecek.";
        yield return new WaitWhile(() =>  speechBubble.activeSelf); //yield return new WaitWhile(() => speechBubble.activeSelf); // yield return new WaitForSeconds(HelperController.SpeechbublpeWaitTime(speechBubbleText.text, textChangeSpeed));
        speechBubble.SetActive(false);
        yield return new WaitForSeconds(0.1f);
        speechBubble.SetActive(true);

        speechBubbleText.text = "Hangi motorun kullanılacağına karar vermek için detaylı testler yapacaksın. Her motoru ayrı ayrı test edeceksin. Bu testleri gerçekleştimek için alet çantasının içerisindeki araçları kullanman gerekiyor. ";

       yield return new WaitWhile(()=> speechBubble.activeSelf); // yield return new WaitForSeconds(HelperController.SpeechbublpeWaitTime(speechBubbleText.text, textChangeSpeed));
        speechBubble.SetActive(false);
        yield return new WaitForSeconds(0.1f);
        speechBubble.SetActive(true);




        speechBubbleText.text = "Bu aletleri kullanmak için alet çantasını aç ve kullanmak istediğine alet'e tıkla. Alet deney ölçüm masasına yerleşecek ve test sırasında ölçümleri gerçekleştirecek.";
        //   Debug.Log(SpeechbublpeWaitTime());

       yield return new WaitWhile(() => speechBubble.activeSelf); // yield return new WaitForSeconds(HelperController.SpeechbublpeWaitTime(speechBubbleText.text, textChangeSpeed));
      


        agentAnim.SetBool(agentAnimParamStartId, false);
        speechBubble.SetActive(false);
        IntroPassBtn.gameObject.SetActive(false);
        HelperController.GameController.isPlayable = true;
        StartCoroutine(SpeakInteractionCoroutine());
    }

    private IEnumerator SpeakInteractionCoroutine()
    {
        yield return new WaitForSeconds(0.1f);
        agentAnim.gameObject.SetActive(true);
        speechBubble.SetActive(true);
        agentAnim.SetBool(agentAnimParamStartId, true);
        HelperController.GameController.isPlayable = false;
        speechBubbleText.text = "Her motor için hız, zaman ve yol değerleniri okuyup tablet üzerinden sisteme girmen gerekiyor.";
       yield return new WaitWhile(() => speechBubble.activeSelf); // yield return new WaitForSeconds(HelperController.SpeechbublpeWaitTime(speechBubbleText.text, textChangeSpeed));

        agentAnim.SetBool(agentAnimParamStartId, false);
        speechBubble.SetActive(false);
        agentAnim.gameObject.SetActive(false);
        HelperController.GameController.isPlayable = true;
        SetForkliftActive(-1, true);
        taskControlDevice.SetActive(true);
        ToolContainer.SetActive(true);
        tabletController.SwitcOn(TabletScreens.Activity2Scene1);
        NextTask();
    }
    public void OnClickPassIntro()
    {
        agentAnim.SetBool(agentAnimParamStartId, false);
        speechBubble.SetActive(false);
        IntroPassBtn.gameObject.SetActive(false);
        HelperController.GameController.isPlayable = true;

     
        StopCoroutine(IntroInteractionCoroutine);
        StartCoroutine(SpeakInteractionCoroutine());
    }
    #endregion

    #region TaskOps
    public void NextTask()
    {
        tabletController.gameObject.SetActive(false);
        StopAllCoroutines();
        if (activeTaskID + 1 < taskList.Count)
        {
            activeTaskID++;
            //  StartCoroutine(NextTaskInteraction());
            sceneMessageController.ShowMessage(HelperController.MessageType.Task, activeTaskID, HelperController.ButtonType.ClosePanel, true);
        }
        else
        {
            NextSection();
        }
    }

    public void TaskStarted()
    {

        HelperController.GameController.Playable(true);
        StartCoroutine(TaskProcess());
    }

    private IEnumerator TaskProcess()
    {
        isTesting = false;
        MotorsGroupB[0].transform.parent.GetComponent<CrawlerController>().ReturnStartPosition();
        switch (activeTaskID)
        {
            case 0:
                #region Task Fırlatma 1 A-3
                TestStartBtn.interactable = true;
                TestRepeatBtn.interactable = false;
                TestStartBtn.onClick.RemoveAllListeners();
                ForkliftController[0].ChangeStatus(ForklitStatus.Passive);
                ForkliftController[1].ChangeStatus(ForklitStatus.Passive);
                ForkliftController[2].ChangeStatus(ForklitStatus.Passive);
                tabletController.activity2Scene1Controller.FırlatmaAnswers[0].SetAnswers(1500, 3000, 2);
                sceneMessageController.ShowMessage(HelperController.MessageType.Task, 0.1f, HelperController.ButtonType.ClosePanel, false);
                yield return new WaitUntil(() => toolController.insertedSlotNumber == 3);
                ForkliftController[0].ChangeStatus(ForklitStatus.Active);
                ForkliftController[1].ChangeStatus(ForklitStatus.Passive);
                ForkliftController[2].ChangeStatus(ForklitStatus.Passive);
                ForkliftController[0].SetMotor(0, 'a');
                //ForkliftController[0].motorItemID = 0;
                //ForkliftController[0].group = 'a';

                sceneMessageController.ShowMessage(HelperController.MessageType.Task, 0.2f, HelperController.ButtonType.ClosePanel, false);
                yield return new WaitUntil(() => MotorsGroupA[0].activeInHierarchy);

                TestStartBtn.onClick.AddListener(delegate
                {
                    isTesting = false;
                    StartCoroutine(TestStart('a', 'F'));
                    TestStartBtn.interactable = false;
                });
                sceneMessageController.ShowMessage(HelperController.MessageType.Task, 0.3f, HelperController.ButtonType.ClosePanel, false);

                yield return new WaitUntil(() => isTesting);

                toolController.SetPanelTxt("000");

                while (isTesting)
                {


                    if (MotorsGroupA[0].transform.parent.GetComponent<Rigidbody2D>().IsSleeping())
                    {
                        isTesting = false;
                    }
                    yield return new WaitForEndOfFrame();
                }
                TestRepeatBtn.interactable = true;
                toolController.MeasureDistance(3000);
                toolController.MeasureSpeed(1500);
                toolController.MeasureTime(2);

                tabletController.gameObject.SetActive(true);
                tabletController.activity2Scene1Controller.EnableMenuButton(0);
                tabletController.activity2Scene1Controller.SwitchOnTaskPanel(1);
                tabletController.activity2Scene1Controller.SwitchOnOffAnswerContainer(1, 0, true);
                tabletController.gameObject.SetActive(false);

                sceneMessageController.ShowMessage(HelperController.MessageType.Task, 0.4f, HelperController.ButtonType.ClosePanel, false);
                #endregion
                break;
            case 1:
                #region Task Fırlatma 2 B-2
                TestStartBtn.interactable = true;
                TestRepeatBtn.interactable = false;

                TestStartBtn.onClick.RemoveAllListeners();
                toolController.ToolEject(1);
                toolController.ToolEject(2);
                toolController.ToolEject(3);
                ForkliftController[0].ChangeStatus(ForklitStatus.Done);
                ForkliftController[1].ChangeStatus(ForklitStatus.Passive);
                ForkliftController[2].ChangeStatus(ForklitStatus.Passive);
                tabletController.activity2Scene1Controller.FırlatmaAnswers[1].SetAnswers(1000, 3000, 3);
                sceneMessageController.ShowMessage(HelperController.MessageType.Task, 1.1f, HelperController.ButtonType.ClosePanel, false);
                yield return new WaitUntil(() => toolController.insertedSlotNumber == 3);


                yield return new WaitForSeconds(1);
                toolController.Fire(true);
                yield return new WaitForSeconds(1);

                toolController.ToolEject(1);
                toolController.ToolEject(2);
                toolController.ToolEject(3);

                toolController.MaxWorkSlotNumber = 2;
                HelperController.GameController.isPlayable = false;
                agentAnim.gameObject.SetActive(true);
                speechBubble.SetActive(true);
                agentAnim.SetBool(agentAnimParamStartId, true);
                yield return new WaitForSeconds(0.5f);

                speechBubbleText.text = "Hay aksi deney ölçüm masasında bulunan alet yuvalarından birinde kısa devre oldu artık kullanılamaz durumda. Bundan sonraki gözlemlerde artık aynı anda iki ölçüm cihazı kullanabileceksin.Elimizde ölçüm cihazlarının yedekleri var ancak masanın yok.";

               yield return new WaitWhile(()=>speechBubble.activeSelf); // yield return new WaitForSeconds(HelperController.SpeechbublpeWaitTime(speechBubbleText.text, textChangeSpeed));
                speechBubble.SetActive(false);
                yield return new WaitForSeconds(0.1f);
                speechBubble.SetActive(true);

                speechBubbleText.text = "Tüm ölçüm cihazları tüm motor sınıflarında kullanılamıyor. Fırlatma motorlarında yol ölçer ve zaman ölçer olmazsa hız ölçer çalışmıyor.";
               yield return new WaitWhile(()=>speechBubble.activeSelf); // yield return new WaitForSeconds(HelperController.SpeechbublpeWaitTime(speechBubbleText.text, textChangeSpeed));
                speechBubble.SetActive(false);
                yield return new WaitForSeconds(0.1f);
                speechBubble.SetActive(true);

                speechBubbleText.text = "Fırlatma motorları için Yol Ölçer ve Zamandan Ölçerden okuduğun değerleri kullanarak hızı da kendin hesaplaman gerekecek.";
               yield return new WaitWhile(()=> speechBubble.activeSelf); // yield return new WaitForSeconds(HelperController.SpeechbublpeWaitTime(speechBubbleText.text, textChangeSpeed));



                agentAnim.SetBool(agentAnimParamStartId, false);
                speechBubble.SetActive(false);
                agentAnim.gameObject.SetActive(false);
                HelperController.GameController.isPlayable = true;
                sceneMessageController.ShowMessage(HelperController.MessageType.Task, 1.2f, HelperController.ButtonType.Continue, true);
                yield return new WaitWhile(() => sceneMessageController.taskMessagePanel.Button.transform.parent.gameObject.activeInHierarchy);
                sceneMessageController.ShowMessage(HelperController.MessageType.Task, 1.3f, HelperController.ButtonType.Continue, false);



                yield return new WaitUntil(() => toolController.insertedSlotNumber == 2);
                ForkliftController[0].ChangeStatus(ForklitStatus.Done);
                ForkliftController[1].ChangeStatus(ForklitStatus.Active);
                ForkliftController[2].ChangeStatus(ForklitStatus.Passive);

                ForkliftController[1].SetMotor(0, 'b');
                //ForkliftController[1].motorItemID = 0;
                //ForkliftController[1].group = 'b';

                sceneMessageController.ShowMessage(HelperController.MessageType.Task, 1.4f, HelperController.ButtonType.ClosePanel, false);
                yield return new WaitUntil(() => MotorsGroupB[0].activeInHierarchy);

                TestStartBtn.onClick.AddListener(delegate
                {
                    isTesting = false;
                    StartCoroutine(TestStart('b', 'F'));
                    TestStartBtn.interactable = false;

                });
                sceneMessageController.ShowMessage(HelperController.MessageType.Task, 1.5f, HelperController.ButtonType.ClosePanel, false);

                yield return new WaitUntil(() => isTesting);

                toolController.SetPanelTxt("000");

                while (isTesting)
                {


                    if (MotorsGroupB[0].transform.parent.GetComponent<Rigidbody2D>().IsSleeping())
                    {
                        isTesting = false;
                    }
                    yield return new WaitForEndOfFrame();
                }
                TestRepeatBtn.interactable = true;

                toolController.MeasureDistance(3000);
                toolController.MeasureSpeed(000);
                toolController.MeasureTime(3);

                tabletController.gameObject.SetActive(true);
                tabletController.activity2Scene1Controller.EnableMenuButton(0);
                tabletController.activity2Scene1Controller.SwitchOnTaskPanel(1);
                tabletController.activity2Scene1Controller.SwitchOnOffAnswerContainer(1, 1, true);
                tabletController.gameObject.SetActive(false);

                sceneMessageController.ShowMessage(HelperController.MessageType.Task, 1.6f, HelperController.ButtonType.ClosePanel, false);
                #endregion
                break;
            case 2:
                #region Fırlatma C C-2
                TestStartBtn.interactable = true;
                TestRepeatBtn.interactable = false;
                TestStartBtn.onClick.RemoveAllListeners();
                toolController.ToolEject(1);
                toolController.ToolEject(2);
                toolController.ToolEject(3);
                ForkliftController[0].ChangeStatus(ForklitStatus.Done);
                ForkliftController[1].ChangeStatus(ForklitStatus.Done);
                ForkliftController[2].ChangeStatus(ForklitStatus.Passive);

                tabletController.activity2Scene1Controller.FırlatmaAnswers[2].SetAnswers(3000, 3000, 1);
                sceneMessageController.ShowMessage(HelperController.MessageType.Task, 2.1f, HelperController.ButtonType.ClosePanel, false);
                yield return new WaitUntil(() => toolController.insertedSlotNumber == 2);

                ForkliftController[2].ChangeStatus(ForklitStatus.Active);
                ForkliftController[2].SetMotor(0, 'c');
                //ForkliftController[2].motorItemID = 0;
                //ForkliftController[2].group = 'c';

                sceneMessageController.ShowMessage(HelperController.MessageType.Task, 2.2f, HelperController.ButtonType.ClosePanel, false);
                yield return new WaitUntil(() => MotorsGroupC[0].activeInHierarchy);

                TestStartBtn.onClick.AddListener(delegate
                {

                    isTesting = false;
                    StartCoroutine(TestStart('c', 'F'));
                    TestStartBtn.interactable = false;

                });
                sceneMessageController.ShowMessage(HelperController.MessageType.Task, 2.3f, HelperController.ButtonType.ClosePanel, false);

                yield return new WaitUntil(() => isTesting);
                toolController.SetPanelTxt("000");
                while (isTesting)
                {
                    if (MotorsGroupC[0].transform.parent.GetComponent<Rigidbody2D>().IsSleeping())
                    {
                        isTesting = false;
                    }
                    yield return new WaitForEndOfFrame();
                }
                TestRepeatBtn.interactable = true;

                toolController.MeasureDistance(3000);
                toolController.MeasureSpeed(000);
                toolController.MeasureTime(1);

                tabletController.gameObject.SetActive(true);
                tabletController.activity2Scene1Controller.EnableMenuButton(0);
                tabletController.activity2Scene1Controller.SwitchOnTaskPanel(1);
                tabletController.activity2Scene1Controller.SwitchOnOffAnswerContainer(1, 2, true);
                tabletController.gameObject.SetActive(false);

                sceneMessageController.ShowMessage(HelperController.MessageType.Task, 2.4f, HelperController.ButtonType.ClosePanel, false);
                #endregion
                break;
            case 3:
                #region Mekik A A-1
                TestStartBtn.interactable = true;
                TestRepeatBtn.interactable = false;

                MotorsGroupA[0].SetActive(false);
                MotorsGroupB[0].SetActive(false);
                MotorsGroupC[0].SetActive(false);

                ForkliftController[0].SetMotor(1, 'a');
                ForkliftController[1].SetMotor(1, 'B');
                ForkliftController[2].SetMotor(1, 'C');


                TestStartBtn.onClick.RemoveAllListeners();
                toolController.ToolEject(1);
                toolController.ToolEject(2);
                toolController.ToolEject(3);
                ForkliftController[0].ChangeStatus(ForklitStatus.Passive);
                ForkliftController[1].ChangeStatus(ForklitStatus.Passive);
                ForkliftController[2].ChangeStatus(ForklitStatus.Passive);

                HelperController.GameController.isPlayable = false;
                agentAnim.gameObject.SetActive(true);
                speechBubble.SetActive(true);
                agentAnim.SetBool(agentAnimParamStartId, true);

                speechBubbleText.text = "Fırlatma motorları ile ilgili testleri tamamladın. Şimdi sırada Mekik Ana Motorları ile ilgili testler var. Mekik Ana motorlarında ise yol ölçer, hız ölçer ve zaman ölçer olmadan çalışmıyor.";
               yield return new WaitWhile(() => speechBubble.activeSelf); // yield return new WaitForSeconds(HelperController.SpeechbublpeWaitTime(speechBubbleText.text, textChangeSpeed));
                speechBubble.SetActive(false);
                yield return new WaitForSeconds(0.1f);
                speechBubble.SetActive(true);
                speechBubbleText.text = "Hız ölçerden hızını ve zaman ölçerden geçen zamanı ölçüm cihazından okuyup aldığı yolu da senin hesaplaman gerekecek.";
               yield return new WaitWhile(() => speechBubble.activeSelf); // yield return new WaitForSeconds(HelperController.SpeechbublpeWaitTime(speechBubbleText.text, textChangeSpeed));
                speechBubble.SetActive(false);
                yield return new WaitForSeconds(0.1f);
                speechBubble.SetActive(true);
                speechBubbleText.text = "Ekranın aşağısında yapman gerekenler yazıyor unutma. Kolay gelsin.";
               yield return new WaitWhile(() => speechBubble.activeSelf); // yield return new WaitForSeconds(HelperController.SpeechbublpeWaitTime(speechBubbleText.text, textChangeSpeed));
             

                HelperController.GameController.isPlayable = true;
                agentAnim.gameObject.SetActive(false);
                speechBubble.SetActive(false);
                agentAnim.SetBool(agentAnimParamStartId, false);

                sceneMessageController.ShowMessage(HelperController.MessageType.Task, 3.0f, HelperController.ButtonType.Continue, true);
                yield return new WaitWhile(() => sceneMessageController.taskMessagePanel.Button.transform.parent.gameObject.activeInHierarchy);
                sceneMessageController.ShowMessage(HelperController.MessageType.Task, 3.1f, HelperController.ButtonType.Continue, false);

                yield return new WaitUntil(() => toolController.insertedSlotNumber == 2);

                ForkliftController[0].ChangeStatus(ForklitStatus.Active);
                //ForkliftController[0].motorItemID = 1;
                //ForkliftController[0].group = 'a';
                tabletController.activity2Scene1Controller.MekikAnswers[0].SetAnswers(600, 600, 1);

                sceneMessageController.ShowMessage(HelperController.MessageType.Task, 3.2f, HelperController.ButtonType.ClosePanel, false);
                yield return new WaitUntil(() => MotorsGroupA[1].activeInHierarchy);

                TestStartBtn.onClick.AddListener(delegate
                {

                    isTesting = false;
                    StartCoroutine(TestStart('a', 'A'));
                    TestStartBtn.interactable = false;

                });
                sceneMessageController.ShowMessage(HelperController.MessageType.Task, 3.3f, HelperController.ButtonType.ClosePanel, false);

                yield return new WaitUntil(() => isTesting);
                toolController.SetPanelTxt("000");
                while (isTesting)
                {
                    if (MotorsGroupA[1].transform.parent.GetComponent<Rigidbody2D>().IsSleeping())
                    {
                        isTesting = false;
                    }
                    yield return new WaitForEndOfFrame();
                }
                TestRepeatBtn.interactable = true;

                toolController.MeasureDistance(0); // 600
                toolController.MeasureSpeed(600);
                toolController.MeasureTime(1);

                tabletController.gameObject.SetActive(true);
                tabletController.activity2Scene1Controller.EnableMenuButton(1);
                tabletController.activity2Scene1Controller.SwitchOnTaskPanel(2);
                tabletController.activity2Scene1Controller.SwitchOnOffAnswerContainer(2, 0, true);
                tabletController.gameObject.SetActive(false);

                sceneMessageController.ShowMessage(HelperController.MessageType.Task, 3.4f, HelperController.ButtonType.ClosePanel, false);
                #endregion
                break;
            case 4:
                #region Mekik B B-1
                TestStartBtn.interactable = true;
                TestRepeatBtn.interactable = false;

                TestStartBtn.onClick.RemoveAllListeners();
                toolController.ToolEject(1);
                toolController.ToolEject(2);
                toolController.ToolEject(3);
                ForkliftController[0].ChangeStatus(ForklitStatus.Done);
                ForkliftController[1].ChangeStatus(ForklitStatus.Passive);
                ForkliftController[2].ChangeStatus(ForklitStatus.Passive);

                tabletController.activity2Scene1Controller.MekikAnswers[1].SetAnswers(400, 600, 1.5f);
                sceneMessageController.ShowMessage(HelperController.MessageType.Task, 4.1f, HelperController.ButtonType.ClosePanel, false);
                yield return new WaitUntil(() => toolController.insertedSlotNumber == 2);

                ForkliftController[1].ChangeStatus(ForklitStatus.Active);
                //ForkliftController[1].motorItemID = 1;
                //ForkliftController[1].group = 'b';

                sceneMessageController.ShowMessage(HelperController.MessageType.Task, 4.2f, HelperController.ButtonType.ClosePanel, false);
                yield return new WaitUntil(() => MotorsGroupB[1].activeInHierarchy);

                TestStartBtn.onClick.AddListener(delegate
                {

                    isTesting = false;
                    StartCoroutine(TestStart('b', 'A'));
                    TestStartBtn.interactable = false;

                });
                sceneMessageController.ShowMessage(HelperController.MessageType.Task, 4.3f, HelperController.ButtonType.ClosePanel, false);

                yield return new WaitUntil(() => isTesting);
                toolController.SetPanelTxt("000");
                while (isTesting)
                {
                    if (MotorsGroupB[1].transform.parent.GetComponent<Rigidbody2D>().IsSleeping())
                    {
                        isTesting = false;
                    }
                    yield return new WaitForEndOfFrame();
                }
                TestRepeatBtn.interactable = true;

                toolController.MeasureDistance(0); //600
                toolController.MeasureSpeed(400);
                toolController.MeasureTime(1.5f);

                tabletController.gameObject.SetActive(true);
                tabletController.activity2Scene1Controller.EnableMenuButton(1);
                tabletController.activity2Scene1Controller.SwitchOnTaskPanel(2);
                tabletController.activity2Scene1Controller.SwitchOnOffAnswerContainer(2, 1, true);
                tabletController.gameObject.SetActive(false);

                sceneMessageController.ShowMessage(HelperController.MessageType.Task, 4.4f, HelperController.ButtonType.ClosePanel, false);
                #endregion
                break;
            case 5:
                #region Mekik C C-1
                TestStartBtn.interactable = true;
                TestRepeatBtn.interactable = false;
                TestStartBtn.onClick.RemoveAllListeners();
                toolController.ToolEject(1);
                toolController.ToolEject(2);
                toolController.ToolEject(3);
                ForkliftController[0].ChangeStatus(ForklitStatus.Done);
                ForkliftController[1].ChangeStatus(ForklitStatus.Done);
                ForkliftController[2].ChangeStatus(ForklitStatus.Passive);

                tabletController.activity2Scene1Controller.MekikAnswers[2].SetAnswers(300, 600, 2f);
                sceneMessageController.ShowMessage(HelperController.MessageType.Task, 5.1f, HelperController.ButtonType.ClosePanel, false);
                yield return new WaitUntil(() => toolController.insertedSlotNumber == 2);

                ForkliftController[2].ChangeStatus(ForklitStatus.Active);
                //ForkliftController[1].motorItemID = 1;
                //ForkliftController[1].group = 'C';

                sceneMessageController.ShowMessage(HelperController.MessageType.Task, 5.2f, HelperController.ButtonType.ClosePanel, false);
                yield return new WaitUntil(() => MotorsGroupC[1].activeInHierarchy);

                TestStartBtn.onClick.AddListener(delegate
                {

                    isTesting = false;
                    StartCoroutine(TestStart('c', 'A'));
                    TestStartBtn.interactable = false;

                });
                sceneMessageController.ShowMessage(HelperController.MessageType.Task, 5.3f, HelperController.ButtonType.ClosePanel, false);

                yield return new WaitUntil(() => isTesting);
                toolController.SetPanelTxt("000");
                while (isTesting)
                {
                    if (MotorsGroupC[1].transform.parent.GetComponent<Rigidbody2D>().IsSleeping())
                    {
                        isTesting = false;
                    }
                    yield return new WaitForEndOfFrame();
                }
                TestRepeatBtn.interactable = true;

                toolController.MeasureDistance(0f); //600
                toolController.MeasureSpeed(300f);
                toolController.MeasureTime(2f);

                tabletController.gameObject.SetActive(true);
                tabletController.activity2Scene1Controller.EnableMenuButton(1);
                tabletController.activity2Scene1Controller.SwitchOnTaskPanel(2);
                tabletController.activity2Scene1Controller.SwitchOnOffAnswerContainer(2, 2, true);
                tabletController.gameObject.SetActive(false);

                sceneMessageController.ShowMessage(HelperController.MessageType.Task, 5.4f, HelperController.ButtonType.ClosePanel, false);
                #endregion
                break;
            case 6:
                #region Manevra A A-2
                TestStartBtn.interactable = true;
                TestRepeatBtn.interactable = false;

                MotorsGroupA[1].SetActive(false);
                MotorsGroupB[1].SetActive(false);
                MotorsGroupC[1].SetActive(false);
                ForkliftController[0].SetMotor(2, 'a');
                ForkliftController[1].SetMotor(2, 'b');
                ForkliftController[2].SetMotor(2, 'c');

                TestStartBtn.onClick.RemoveAllListeners();
                toolController.ToolEject(1);
                toolController.ToolEject(2);
                toolController.ToolEject(3);
                ForkliftController[0].ChangeStatus(ForklitStatus.Passive);
                ForkliftController[1].ChangeStatus(ForklitStatus.Passive);
                ForkliftController[2].ChangeStatus(ForklitStatus.Passive);

                HelperController.GameController.isPlayable = false;
                agentAnim.gameObject.SetActive(true);
                speechBubble.SetActive(true);
                agentAnim.SetBool(agentAnimParamStartId, true);

                speechBubbleText.text = "Mekik Ana Motorları ile ilgili testleri tamamladın. Şimdi sırada Manevra Motorları ile ilgili testler var. Manevra motorları çok yavaş olduğu içn zaman ölçer hız ölçer ve yol ölçer olmadan çalışmıyor.";
               yield return new WaitWhile(() => speechBubble.activeSelf); // yield return new WaitForSeconds(HelperController.SpeechbublpeWaitTime(speechBubbleText.text, textChangeSpeed));
                speechBubble.SetActive(false);
                yield return new WaitForSeconds(0.1f);
                speechBubble.SetActive(true);
                speechBubbleText.text = "Zaman Ölçerin sonuç vermesi için bu motorlarda Hız Ölçer ve Yol Ölçer'e ihtiyaç var. Hızını ve aldığı yolu ölçüm cihazından okuyup hızı da senin hesaplaman gerekecek.";
               yield return new WaitWhile(() => speechBubble.activeSelf); // yield return new WaitForSeconds(HelperController.SpeechbublpeWaitTime(speechBubbleText.text, textChangeSpeed));
                speechBubble.SetActive(false);
                yield return new WaitForSeconds(0.1f);
                speechBubble.SetActive(true);
                speechBubbleText.text = "Ekranın aşağısında yapman gerekenler yazıyor unutma. Kolay gelsin.";
                yield return new WaitWhile(() => speechBubble.activeSelf);
                //yield return new WaitForSeconds(SpeechbublpeWaitTime());

                HelperController.GameController.isPlayable = true;
                agentAnim.gameObject.SetActive(false);
                speechBubble.SetActive(false);
                agentAnim.SetBool(agentAnimParamStartId, false);

                sceneMessageController.ShowMessage(HelperController.MessageType.Task, 6.0f, HelperController.ButtonType.Continue, true);
                yield return new WaitWhile(() => sceneMessageController.taskMessagePanel.Button.transform.parent.gameObject.activeInHierarchy);
                sceneMessageController.ShowMessage(HelperController.MessageType.Task, 6.1f, HelperController.ButtonType.Continue, false);

                yield return new WaitUntil(() => toolController.insertedSlotNumber == 2);

                ForkliftController[0].ChangeStatus(ForklitStatus.Active);
            //    ForkliftController[0].motorItemID = 2;
             //   ForkliftController[0].group = 'a';
                tabletController.activity2Scene1Controller.ManevraAnswers[0].SetAnswers(10, 600, 60);

                sceneMessageController.ShowMessage(HelperController.MessageType.Task, 6.2f, HelperController.ButtonType.ClosePanel, false);
                yield return new WaitUntil(() => MotorsGroupA[2].activeInHierarchy);

                TestStartBtn.onClick.AddListener(delegate
                {

                    isTesting = false;
                    StartCoroutine(TestStart('a', 'M'));
                    TestStartBtn.interactable = false;

                });
                sceneMessageController.ShowMessage(HelperController.MessageType.Task, 6.3f, HelperController.ButtonType.ClosePanel, false);

                yield return new WaitUntil(() => isTesting);
                toolController.SetPanelTxt("000");
                while (isTesting)
                {
                    if (MotorsGroupA[2].transform.parent.GetComponent<Rigidbody2D>().IsSleeping())
                    {
                        isTesting = false;
                    }
                    yield return new WaitForEndOfFrame();
                }
                TestRepeatBtn.interactable = true;

                toolController.MeasureSpeed(10);
                toolController.MeasureDistance(600); // 600
                toolController.MeasureTime(0); //60

                tabletController.gameObject.SetActive(true);
                tabletController.activity2Scene1Controller.EnableMenuButton(2);
                tabletController.activity2Scene1Controller.SwitchOnTaskPanel(3);
                tabletController.activity2Scene1Controller.SwitchOnOffAnswerContainer(3, 0, true);
                tabletController.gameObject.SetActive(false);

                sceneMessageController.ShowMessage(HelperController.MessageType.Task, 6.4f, HelperController.ButtonType.ClosePanel, false);

                #endregion
                break;
            case 7:
                #region Manevra B B-3
                TestStartBtn.interactable = true;
                TestRepeatBtn.interactable = false;

                TestStartBtn.onClick.RemoveAllListeners();
                toolController.ToolEject(1);
                toolController.ToolEject(2);
                toolController.ToolEject(3);
                ForkliftController[0].ChangeStatus(ForklitStatus.Done);
                ForkliftController[1].ChangeStatus(ForklitStatus.Passive);
                ForkliftController[2].ChangeStatus(ForklitStatus.Passive);

                tabletController.activity2Scene1Controller.ManevraAnswers[1].SetAnswers(12, 600, 50f);
                sceneMessageController.ShowMessage(HelperController.MessageType.Task, 7.1f, HelperController.ButtonType.ClosePanel, false);
                yield return new WaitUntil(() => toolController.insertedSlotNumber == 2);

                ForkliftController[1].ChangeStatus(ForklitStatus.Active);
              //  ForkliftController[1].motorItemID = 2;
              //  ForkliftController[1].group = 'b';

                sceneMessageController.ShowMessage(HelperController.MessageType.Task, 7.2f, HelperController.ButtonType.ClosePanel, false);
                yield return new WaitUntil(() => MotorsGroupB[2].activeInHierarchy);

                TestStartBtn.onClick.AddListener(delegate
                {

                    isTesting = false;
                    StartCoroutine(TestStart('b', 'M'));
                    TestStartBtn.interactable = false;

                });
                sceneMessageController.ShowMessage(HelperController.MessageType.Task, 7.3f, HelperController.ButtonType.ClosePanel, false);

                yield return new WaitUntil(() => isTesting);
                toolController.SetPanelTxt("000");
                while (isTesting)
                {
                    if (MotorsGroupB[2].transform.parent.GetComponent<Rigidbody2D>().IsSleeping())
                    {
                        isTesting = false;
                    }
                    yield return new WaitForEndOfFrame();
                }
                TestRepeatBtn.interactable = true;

                toolController.MeasureSpeed(12);
                toolController.MeasureDistance(600);
                toolController.MeasureTime(0f); //50

                tabletController.gameObject.SetActive(true);
                tabletController.activity2Scene1Controller.EnableMenuButton(2);
                tabletController.activity2Scene1Controller.SwitchOnTaskPanel(3);
                tabletController.activity2Scene1Controller.SwitchOnOffAnswerContainer(3, 1, true);
                tabletController.gameObject.SetActive(false);

                sceneMessageController.ShowMessage(HelperController.MessageType.Task, 7.4f, HelperController.ButtonType.ClosePanel, false);
                #endregion
                break;
            case 8:
                #region Manevra C C-3
                TestStartBtn.interactable = true;
                TestRepeatBtn.interactable = false;

                TestStartBtn.onClick.RemoveAllListeners();
                toolController.ToolEject(1);
                toolController.ToolEject(2);
                toolController.ToolEject(3);
                ForkliftController[0].ChangeStatus(ForklitStatus.Done);
                ForkliftController[1].ChangeStatus(ForklitStatus.Done);
                ForkliftController[2].ChangeStatus(ForklitStatus.Passive);

                tabletController.activity2Scene1Controller.ManevraAnswers[2].SetAnswers(15, 600, 40f);
                sceneMessageController.ShowMessage(HelperController.MessageType.Task, 8.1f, HelperController.ButtonType.ClosePanel, false);
                yield return new WaitUntil(() => toolController.insertedSlotNumber == 2);

                ForkliftController[2].ChangeStatus(ForklitStatus.Active);
            //    ForkliftController[2].motorItemID = 2;
           //     ForkliftController[2].group = 'C';

                sceneMessageController.ShowMessage(HelperController.MessageType.Task, 7.2f, HelperController.ButtonType.ClosePanel, false);
                yield return new WaitUntil(() => MotorsGroupC[2].activeInHierarchy);

                TestStartBtn.onClick.AddListener(delegate
                {

                    isTesting = false;
                    StartCoroutine(TestStart('c', 'M'));
                    TestStartBtn.interactable = false;

                });
                sceneMessageController.ShowMessage(HelperController.MessageType.Task, 7.3f, HelperController.ButtonType.ClosePanel, false);

                yield return new WaitUntil(() => isTesting);
                toolController.SetPanelTxt("000");
                while (isTesting)
                {
                    if (MotorsGroupC[2].transform.parent.GetComponent<Rigidbody2D>().IsSleeping())
                    {
                        isTesting = false;
                    }
                    yield return new WaitForEndOfFrame();
                }
                TestRepeatBtn.interactable = true;

                toolController.MeasureDistance(600f);
                toolController.MeasureSpeed(15);
                toolController.MeasureTime(0f); //40

                tabletController.gameObject.SetActive(true);
                tabletController.activity2Scene1Controller.EnableMenuButton(2);
                tabletController.activity2Scene1Controller.SwitchOnTaskPanel(3);
                tabletController.activity2Scene1Controller.SwitchOnOffAnswerContainer(3, 2, true);
                tabletController.gameObject.SetActive(false);

                sceneMessageController.ShowMessage(HelperController.MessageType.Task, 7.4f, HelperController.ButtonType.ClosePanel, false);
                #endregion
                break;
        }


        yield return new WaitForEndOfFrame();
    }

    private IEnumerator TestStart(char gr, char engineType)
    {

        string key = engineType.ToString() + gr.ToString();
        key = key.ToUpper();
        trafficLightController.CountDownForTest();
        toolController.SetPanelTxt("---");

        trafficLightController.CountDownForTest();
        yield return new WaitUntil(() => trafficLightController.countDownComplete);

        if (gr == 'A' || gr == 'a')
        {
            MotorsGroupA[0].transform.parent.GetComponent<CrawlerController>().AddForce(engineSpeed[key]);
        }
        if (gr == 'B' || gr == 'b')
        {
            MotorsGroupB[0].transform.parent.GetComponent<CrawlerController>().AddForce(engineSpeed[key]);
        }
        if (gr == 'C' || gr == 'c')
        {
            Debug.Log("testStart--c");

            MotorsGroupC[0].transform.parent.GetComponent<CrawlerController>().AddForce(engineSpeed[key]);
        }
        isTesting = true;
    }
    public void TestFinish()
    {
        switch (activeTaskID)
        {
            case 0:
                toolController.MeasureDistance(3000);
                toolController.MeasureSpeed(1500);
                toolController.MeasureTime(2);
                break;
            case 1:
                toolController.MeasureDistance(3000);
                toolController.MeasureSpeed(000);  // 1000
                toolController.MeasureTime(3);
                break;
            case 2:
                #region Fırlatma C
                toolController.MeasureDistance(3000);
                toolController.MeasureSpeed(000);  // 1000
                toolController.MeasureTime(1);
                #endregion
                break;
            case 3:
                #region Mekik A
                toolController.MeasureDistance(0); // 600
                toolController.MeasureSpeed(600);
                toolController.MeasureTime(1);
                #endregion
                break;
            case 4:
                #region Mekik B
                toolController.MeasureDistance(0); // 600
                toolController.MeasureSpeed(400);
                toolController.MeasureTime(1);
                #endregion
                break;
            case 5:
                #region Mekik C
                toolController.MeasureDistance(0); // 600
                toolController.MeasureSpeed(300);
                toolController.MeasureTime(1);
                #endregion
                break;
            case 6:
                #region Manevra A
                toolController.MeasureDistance(600);
                toolController.MeasureSpeed(10);
                toolController.MeasureTime(0);  //60
                #endregion
                break;
            case 7:
                #region Manevra B
                toolController.MeasureDistance(600);
                toolController.MeasureSpeed(12);
                toolController.MeasureTime(0);  //50
                #endregion
                break;
            case 8:
                #region Manevra C
                toolController.MeasureDistance(600);
                toolController.MeasureSpeed(15);
                toolController.MeasureTime(0);  //40
                #endregion
                break;
            default:
                break;
        }
    }
    public void TaskComleted()
    {
        HelperController.TaskController.TaskComplete(activeTaskID);
        //  tabletController.activity1scene1Controller.MenuButtonComplete(activeTaskID);
        sceneMessageController.ShowMessage(HelperController.MessageType.Success, 1, HelperController.ButtonType.ClosePanel, true);
    }

    public void CheckAnswer()
    {
        switch (activeTaskID)
        {
            case 0:
                #region Fırlatma A
                if (tabletController.activity2Scene1Controller.FırlatmaAnswers[0].CheckAnswer() == TabletActivity2AnswerModel.AnswerResponseType.NotNumeric)
                {
                    sceneMessageController.ShowMessage(HelperController.MessageType.Failure, 1, HelperController.ButtonType.ClosePanel, true);
                }
                else if (tabletController.activity2Scene1Controller.FırlatmaAnswers[0].CheckAnswer() == TabletActivity2AnswerModel.AnswerResponseType.True)
                {
                    TaskComleted();
                    tabletController.activity2Scene1Controller.CompleteAnswer(1, 0, true);
                    //     tabletController.activity1scene1Controller.MenuButtonComplete(0);
                    ForkliftController[0].ChangeStatus(ForklitStatus.Done);
                    ForkliftController[1].ChangeStatus(ForklitStatus.Passive);
                    ForkliftController[2].ChangeStatus(ForklitStatus.Passive);
                    toolController.ToolEject(1);
                    toolController.ToolEject(2);
                    toolController.ToolEject(3);
                    MotorsGroupA[0].SetActive(false);
                }
                else
                {
                    sceneMessageController.ShowMessage(HelperController.MessageType.Failure, 0, HelperController.ButtonType.ClosePanel, true);
                }
                break;
            #endregion
            case 1:
                #region Fırlatma B
                if (tabletController.activity2Scene1Controller.FırlatmaAnswers[1].CheckAnswer() == TabletActivity2AnswerModel.AnswerResponseType.NotNumeric)
                {
                    sceneMessageController.ShowMessage(HelperController.MessageType.Failure, 1, HelperController.ButtonType.ClosePanel, true);
                }
                else if (tabletController.activity2Scene1Controller.FırlatmaAnswers[1].CheckAnswer() == TabletActivity2AnswerModel.AnswerResponseType.True)
                {
                    TaskComleted();
                    tabletController.activity2Scene1Controller.CompleteAnswer(1, 1, true);
                    //     tabletController.activity1scene1Controller.MenuButtonComplete(0);
                    ForkliftController[0].ChangeStatus(ForklitStatus.Done);
                    ForkliftController[1].ChangeStatus(ForklitStatus.Done);
                    ForkliftController[2].ChangeStatus(ForklitStatus.Passive);
                    toolController.ToolEject(1);
                    toolController.ToolEject(2);
                    toolController.ToolEject(3);
                    MotorsGroupB[0].SetActive(false);
                }
                else
                {
                    sceneMessageController.ShowMessage(HelperController.MessageType.Failure, 0, HelperController.ButtonType.ClosePanel, true);
                }
                #endregion
                break;
            case 2:
                #region Fırlatma C
                if (tabletController.activity2Scene1Controller.FırlatmaAnswers[2].CheckAnswer() == TabletActivity2AnswerModel.AnswerResponseType.NotNumeric)
                {
                    sceneMessageController.ShowMessage(HelperController.MessageType.Failure, 1, HelperController.ButtonType.ClosePanel, true);
                }
                else if (tabletController.activity2Scene1Controller.FırlatmaAnswers[2].CheckAnswer() == TabletActivity2AnswerModel.AnswerResponseType.True)
                {
                    TaskComleted();
                    tabletController.activity2Scene1Controller.CompleteAnswer(1, 2, true);
                    tabletController.activity1scene1Controller.MenuButtonComplete(0);
                    ForkliftController[0].ChangeStatus(ForklitStatus.Done);
                    ForkliftController[1].ChangeStatus(ForklitStatus.Done);
                    ForkliftController[2].ChangeStatus(ForklitStatus.Done);
                    toolController.ToolEject(1);
                    toolController.ToolEject(2);
                    toolController.ToolEject(3);
                    MotorsGroupC[0].SetActive(false);
                }
                else
                {
                    sceneMessageController.ShowMessage(HelperController.MessageType.Failure, 0, HelperController.ButtonType.ClosePanel, true);
                }
                #endregion
                break;
            case 3:
                #region Mekik A
                if (tabletController.activity2Scene1Controller.MekikAnswers[0].CheckAnswer() == TabletActivity2AnswerModel.AnswerResponseType.NotNumeric)
                {
                    sceneMessageController.ShowMessage(HelperController.MessageType.Failure, 1, HelperController.ButtonType.ClosePanel, true);
                }
                else if (tabletController.activity2Scene1Controller.MekikAnswers[0].CheckAnswer() == TabletActivity2AnswerModel.AnswerResponseType.True)
                {
                    TaskComleted();
                    tabletController.activity2Scene1Controller.CompleteAnswer(2, 0, true);
                    //     tabletController.activity1scene1Controller.MenuButtonComplete(0);
                    ForkliftController[0].ChangeStatus(ForklitStatus.Done);
                    ForkliftController[1].ChangeStatus(ForklitStatus.Passive);
                    ForkliftController[2].ChangeStatus(ForklitStatus.Passive);
                    toolController.ToolEject(1);
                    toolController.ToolEject(2);
                    toolController.ToolEject(3);
                    MotorsGroupA[1].SetActive(false);
                }
                else
                {
                    sceneMessageController.ShowMessage(HelperController.MessageType.Failure, 0, HelperController.ButtonType.ClosePanel, true);
                }
                #endregion
                break;
            case 4:
                #region Mekik B
                if (tabletController.activity2Scene1Controller.MekikAnswers[1].CheckAnswer() == TabletActivity2AnswerModel.AnswerResponseType.NotNumeric)
                {
                    sceneMessageController.ShowMessage(HelperController.MessageType.Failure, 1, HelperController.ButtonType.ClosePanel, true);
                }
                else if (tabletController.activity2Scene1Controller.MekikAnswers[1].CheckAnswer() == TabletActivity2AnswerModel.AnswerResponseType.True)
                {
                    TaskComleted();
                    tabletController.activity2Scene1Controller.CompleteAnswer(2, 1, true);
                    //     tabletController.activity1scene1Controller.MenuButtonComplete(0);
                    ForkliftController[0].ChangeStatus(ForklitStatus.Done);
                    ForkliftController[1].ChangeStatus(ForklitStatus.Done);
                    ForkliftController[2].ChangeStatus(ForklitStatus.Passive);
                    toolController.ToolEject(1);
                    toolController.ToolEject(2);
                    toolController.ToolEject(3);
                    MotorsGroupB[1].SetActive(false);
                }
                else
                {
                    sceneMessageController.ShowMessage(HelperController.MessageType.Failure, 0, HelperController.ButtonType.ClosePanel, true);
                }
                #endregion
                break;
            case 5:
                #region Mekik C
                if (tabletController.activity2Scene1Controller.MekikAnswers[2].CheckAnswer() == TabletActivity2AnswerModel.AnswerResponseType.NotNumeric)
                {
                    sceneMessageController.ShowMessage(HelperController.MessageType.Failure, 1, HelperController.ButtonType.ClosePanel, true);
                }
                else if (tabletController.activity2Scene1Controller.MekikAnswers[2].CheckAnswer() == TabletActivity2AnswerModel.AnswerResponseType.True)
                {
                    TaskComleted();
                    tabletController.activity2Scene1Controller.CompleteAnswer(2, 2, true);
                    tabletController.activity1scene1Controller.MenuButtonComplete(1);
                    ForkliftController[0].ChangeStatus(ForklitStatus.Done);
                    ForkliftController[1].ChangeStatus(ForklitStatus.Done);
                    ForkliftController[2].ChangeStatus(ForklitStatus.Done);
                    toolController.ToolEject(1);
                    toolController.ToolEject(2);
                    toolController.ToolEject(3);
                    MotorsGroupA[2].SetActive(false);
                }
                else
                {
                    sceneMessageController.ShowMessage(HelperController.MessageType.Failure, 0, HelperController.ButtonType.ClosePanel, true);
                }
                #endregion
                break;
            case 6:
                #region Manevra A
                if (tabletController.activity2Scene1Controller.ManevraAnswers[0].CheckAnswer() == TabletActivity2AnswerModel.AnswerResponseType.NotNumeric)
                {
                    sceneMessageController.ShowMessage(HelperController.MessageType.Failure, 1, HelperController.ButtonType.ClosePanel, true);
                }
                else if (tabletController.activity2Scene1Controller.ManevraAnswers[0].CheckAnswer() == TabletActivity2AnswerModel.AnswerResponseType.True)
                {
                    TaskComleted();
                    tabletController.activity2Scene1Controller.CompleteAnswer(3, 0, true);
                    //     tabletController.activity1scene1Controller.MenuButtonComplete(0);
                    ForkliftController[0].ChangeStatus(ForklitStatus.Done);
                    ForkliftController[1].ChangeStatus(ForklitStatus.Passive);
                    ForkliftController[2].ChangeStatus(ForklitStatus.Passive);
                    toolController.ToolEject(1);
                    toolController.ToolEject(2);
                    toolController.ToolEject(3);
                    MotorsGroupA[2].SetActive(false);
                }
                else
                {
                    sceneMessageController.ShowMessage(HelperController.MessageType.Failure, 0, HelperController.ButtonType.ClosePanel, true);
                }
                #endregion
                break;
            case 7:
                #region Manevra B
                if (tabletController.activity2Scene1Controller.ManevraAnswers[1].CheckAnswer() == TabletActivity2AnswerModel.AnswerResponseType.NotNumeric)
                {
                    sceneMessageController.ShowMessage(HelperController.MessageType.Failure, 1, HelperController.ButtonType.ClosePanel, true);
                }
                else if (tabletController.activity2Scene1Controller.ManevraAnswers[1].CheckAnswer() == TabletActivity2AnswerModel.AnswerResponseType.True)
                {
                    TaskComleted();
                    tabletController.activity2Scene1Controller.CompleteAnswer(3, 1, true);
                    //     tabletController.activity1scene1Controller.MenuButtonComplete(0);
                    ForkliftController[0].ChangeStatus(ForklitStatus.Done);
                    ForkliftController[1].ChangeStatus(ForklitStatus.Done);
                    ForkliftController[2].ChangeStatus(ForklitStatus.Passive);
                    toolController.ToolEject(1);
                    toolController.ToolEject(2);
                    toolController.ToolEject(3);
                    MotorsGroupB[2].SetActive(false);
                }
                else
                {
                    sceneMessageController.ShowMessage(HelperController.MessageType.Failure, 0, HelperController.ButtonType.ClosePanel, true);
                }
                #endregion
                break;
            case 8:
                #region Manevra C
                if (tabletController.activity2Scene1Controller.ManevraAnswers[2].CheckAnswer() == TabletActivity2AnswerModel.AnswerResponseType.NotNumeric)
                {
                    sceneMessageController.ShowMessage(HelperController.MessageType.Failure, 1, HelperController.ButtonType.ClosePanel, true);
                }
                else if (tabletController.activity2Scene1Controller.ManevraAnswers[2].CheckAnswer() == TabletActivity2AnswerModel.AnswerResponseType.True)
                {
                    TaskComleted();
                    tabletController.activity2Scene1Controller.CompleteAnswer(3, 2, true);
                    tabletController.activity1scene1Controller.MenuButtonComplete(0);
                    ForkliftController[0].ChangeStatus(ForklitStatus.Done);
                    ForkliftController[1].ChangeStatus(ForklitStatus.Done);
                    ForkliftController[2].ChangeStatus(ForklitStatus.Done);
                    toolController.ToolEject(1);
                    toolController.ToolEject(2);
                    toolController.ToolEject(3);
                    MotorsGroupC[2].SetActive(false);
                }
                else
                {
                    sceneMessageController.ShowMessage(HelperController.MessageType.Failure, 0, HelperController.ButtonType.ClosePanel, true);
                }
                #endregion
                break;
            default:
                break;
        }
    }
    public void RestartMission()
    {
        switch (activeTaskID)
        {
            case 0:
                tabletController.activity2Scene1Controller.FırlatmaAnswers[0].ClearWrongAnswer();
                break;
            case 1:
                #region Fırlatma B
                tabletController.activity2Scene1Controller.FırlatmaAnswers[1].ClearWrongAnswer();
                #endregion
                break;
            case 2:
                #region Fırlatma C
                tabletController.activity2Scene1Controller.FırlatmaAnswers[2].ClearWrongAnswer();
                #endregion
                break;
            case 3:
                #region Mekik A
                tabletController.activity2Scene1Controller.MekikAnswers[0].ClearWrongAnswer();
                #endregion
                break;
            case 4:
                #region Mekik B
                tabletController.activity2Scene1Controller.MekikAnswers[1].ClearWrongAnswer();
                #endregion
                break;
            case 5:
                #region Mekik C
                tabletController.activity2Scene1Controller.MekikAnswers[2].ClearWrongAnswer();
                #endregion
                break;
            case 6:
                #region Manevra A
                tabletController.activity2Scene1Controller.ManevraAnswers[0].ClearWrongAnswer();
                #endregion
                break;
            case 7:
                #region Manevra B
                tabletController.activity2Scene1Controller.ManevraAnswers[1].ClearWrongAnswer();
                #endregion
                break;
            case 8:
                #region Manevra C
                tabletController.activity2Scene1Controller.ManevraAnswers[2].ClearWrongAnswer();
                #endregion
                break;
            default:
                break;
        }
    }
    #endregion

    public void RestartTask()
    {
        if (activeTaskID > -1)
        {
            StopAllCoroutines();
            activeTaskID--;
            NextTask();
        }

    }
    #region Section
    public void NextSection()
    {
        StopAllCoroutines();
        StartCoroutine(NextSectionProcess());
    }

    private IEnumerator NextSectionProcess()
    {
        agentAnim.gameObject.SetActive(true);
        speechBubble.SetActive(true);
        agentAnim.SetBool(agentAnimParamStartId, true);
        HelperController.GameController.isPlayable = false;
        speechBubbleText.text = "Elimizde ki tüm motorlarla alakalı detaylı hız testlerini başarı ile tamamladın. Şimdi bu test raporlarına göre mekiğe takılacak motora karar vermen gerekiyor.";
       yield return new WaitWhile(() => speechBubble.activeSelf); // yield return new WaitForSeconds(HelperController.SpeechbublpeWaitTime(speechBubbleText.text, textChangeSpeed));
        speechBubble.SetActive(false);
        yield return new WaitForSeconds(0.1f);
        speechBubble.SetActive(true);
        speechBubbleText.text = "Şimdi hangara gidip hangi motorların takılacağını seçip mekiğe monte et.";
       yield return new WaitWhile(() => speechBubble.activeSelf); // yield return new WaitForSeconds(HelperController.SpeechbublpeWaitTime(speechBubbleText.text, textChangeSpeed));
        speechBubble.SetActive(false);
        yield return new WaitForSeconds(0.1f);
        speechBubble.SetActive(true);
        agentAnim.SetBool(agentAnimParamStartId, false);
        speechBubble.SetActive(false);
        agentAnim.transform.parent.gameObject.SetActive(false);
        HelperController.GameController.isPlayable = true;
        sceneMessageController.ShowMessage(HelperController.MessageType.Section, 1f, HelperController.ButtonType.ClosePanel, false);
        yield return new WaitForEndOfFrame();
    }
    public void NextSectionGo()
    {
        StopAllCoroutines();
        HelperController.GameController.sceneController.LoadNextScene();
    }
    #endregion

    #region List
    public List<TaskController.TaskStruct> taskList = new List<TaskController.TaskStruct>();
    private void TaskGenerate()
    {
        taskList.Add(new TaskController.TaskStruct("Fırlatma Motoru A-3'ün testlerini yap ve değerleri tablete gir."));
        taskList.Add(new TaskController.TaskStruct("Fırlatma Motoru B-2'nin testlerini yap ve değerleri tablete gir."));
        taskList.Add(new TaskController.TaskStruct("Fırlatma Motoru C-2'nin testlerini yap ve değerleri tablete gir."));

        taskList.Add(new TaskController.TaskStruct("Mekik Ana Motoru A-1'in testlerini yap ve değerleri tablete gir."));
        taskList.Add(new TaskController.TaskStruct("Mekik Ana Motoru B-1'in testlerini yap ve değerleri tablete gir."));
        taskList.Add(new TaskController.TaskStruct("Mekik Ana Motoru C-1'in testlerini yap ve değerleri tablete gir."));

        taskList.Add(new TaskController.TaskStruct("Manevra Motoru A-2'nin testlerini yap ve değerleri tablete gir."));
        taskList.Add(new TaskController.TaskStruct("Manevra Motoru B-3'ün testlerini yap ve değerleri tablete gir."));
        taskList.Add(new TaskController.TaskStruct("Manevra Motoru C-3'ün testlerini yap ve değerleri tablete gir."));
        HelperController.TaskController.TaskListFillOnSceneStart(taskList);

    }



    public Dictionary<string, float> engineSpeed = new Dictionary<string, float>()
    {
        {"FA",1.3f },
        {"FB",1.1f },
        {"FC",1.5f },
        {"AA",0.9f },
        {"AB",0.7f },
        {"AC",0.6f },
        {"MA",0.3f },
        {"MB",0.4f },
        {"MC",0.2f },

        {"A1",0.9f },
        {"A2",0.3f },
        {"A3",1.3f },

        {"B1",0.7f },
        {"B2",1.1f },
        {"B3",0.4f },

        {"C1",0.2f },
        {"C2",1.5f },
        {"C3",0.6f },
    };

    #endregion

    #region Helper
    public void SetActiveMotor(char group, int id)
    {
        foreach (GameObject item in MotorsGroupA)
        {
            item.SetActive(false);
        }
        foreach (GameObject item in MotorsGroupB)
        {
            item.SetActive(false);
        }
        foreach (GameObject item in MotorsGroupC)
        {
            item.SetActive(false);
        }
        if (group == 'A' || group == 'a')
        {
            MotorsGroupA[id].SetActive(true);
        }
        if (group == 'B' || group == 'b')
        {
            MotorsGroupB[id].SetActive(true);
        }
        if (group == 'C' || group == 'c')
        {
            MotorsGroupC[id].SetActive(true);
        }
        trafficLightController.BlinkRedToGreen(false, 0);
        trafficLightController.SwitchRed();
    }

    public void SetForkliftActive(int id, bool active)
    {
        if (id == -1)
        {
            foreach (Activity2ForkliftController item in ForkliftController)
            {
                item.gameObject.SetActive(active);
                item.ChangeStatus(ForklitStatus.Passive);
            }
        }
        else
        {
            ForkliftController[id].gameObject.SetActive(active);
            ForkliftController[id].ChangeStatus(ForklitStatus.Passive);

        }

    }

    public float SpeechbublpeWaitTimeO()
    {
        return (speechBubbleText.text.Length * Time.deltaTime / textChangeSpeed) + 1.5f;
    }
    #endregion

}
