﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Activity2ToolController : MonoBehaviour
{
    public GameObject[] Slot1Tools;
    public GameObject[] Slot2Tools;
    public GameObject[] Slot3Tools;
    public Activity2ToolItemController[] Slot1ToolsItem;
    public Activity2ToolItemController[] Slot2ToolsItem;
    public Activity2ToolItemController[] Slot3ToolsItem;
    public SpriteRenderer firedSlotBG;
    public Sprite outofOrderSprite;

    public int insertedSlotNumber = 0;
    public int MaxWorkSlotNumber = 3;
    public GameObject FireParticleObject;

    public float MeasuredSpeed = 0;
    public float MeasuredDistance = 0;
    public float MeasuredTime = 0;


    private List<Activity2ToolItemController> ActiveToolList = new List<Activity2ToolItemController>();
    private bool slot1Filled;
    private bool slot2Filled;
    private bool slot3Filled;
    // Use this for initialization
    void Start()
    {
        Fire(false);
    }

    // Update is called once per frame
    void Update()
    {

    }
    
    public void SetPanelTxt(string text)
    {
        foreach (Activity2ToolItemController item in ActiveToolList)
        {

            item.panelTxt.text = text;
        }
    }
    public void MeasureSpeed(float value)
    {
        foreach (Activity2ToolItemController item in ActiveToolList)
        {
            if (item.Type == MeasurementToolType.Speed)
            {
                item.measuredValue = value;
            }
        }
    }
    public void MeasureDistance(float value)
    {
        foreach (Activity2ToolItemController item in ActiveToolList)
        {
            if (item.Type == MeasurementToolType.Distance)
            {
                item.measuredValue = value;
            }
        }
    }
    public void MeasureTime(float value)
    {
        foreach (Activity2ToolItemController item in ActiveToolList)
        {
            if (item.Type == MeasurementToolType.Time)
            {
                item.measuredValue = value;
            }
        }
    }
    public void SetSlotNumber(int value)
    {
        MaxWorkSlotNumber = value;
    }
    public void ToolInsert(int DeviceID)
    {

        //bool inserted = false;
        if (insertedSlotNumber < MaxWorkSlotNumber && HelperController.GameController.isPlayable)
        {


            if (!slot1Filled)
            {
                Slot1ToolsItem[DeviceID].gameObject.SetActive(true);
                Slot1ToolsItem[DeviceID].measuredValue = 0;
                Slot1ToolsItem[DeviceID].panelTxt.text = "---";
                slot1Filled = true;
                insertedSlotNumber++;
                ActiveToolList.Add(Slot1ToolsItem[DeviceID]);
               
                return;
            }
            if (!slot2Filled)
            {
                Slot2ToolsItem[DeviceID].gameObject.SetActive(true);
                Slot2ToolsItem[DeviceID].measuredValue = 0;
                Slot2ToolsItem[DeviceID].panelTxt.text = "---";
                insertedSlotNumber++;
                slot2Filled = true;
                ActiveToolList.Add(Slot2ToolsItem[DeviceID]);
                
                return;
            }
            if (!slot3Filled && !FireParticleObject.activeInHierarchy)
            {
                Slot3ToolsItem[DeviceID].gameObject.SetActive(true);
                Slot3ToolsItem[DeviceID].measuredValue = 0;
                Slot3ToolsItem[DeviceID].panelTxt.text = "---";
                insertedSlotNumber++;
                slot3Filled = true;
                ActiveToolList.Add(Slot3ToolsItem[DeviceID]);
                
                return;
            }

        }
        else
        {
            Debug.Log("Warning Working Slot is full");
        }



    }
    public void ToolEject(int slotID)
    {

        if (insertedSlotNumber > 0 && HelperController.GameController.isPlayable)
        {
            if (slotID == 1)
            {
                Slot1Tools[0].SetActive(false);
                Slot1Tools[1].SetActive(false);
                Slot1Tools[2].SetActive(false);
                
                slot1Filled = false;
                ActiveToolList.Remove(Slot1Tools[0].GetComponent<Activity2ToolItemController>());
                ActiveToolList.Remove(Slot1Tools[1].GetComponent<Activity2ToolItemController>());
                ActiveToolList.Remove(Slot1Tools[2].GetComponent<Activity2ToolItemController>());


            }
            if (slotID == 2)
            {
                Slot2Tools[0].SetActive(false);
                Slot2Tools[1].SetActive(false);
                Slot2Tools[2].SetActive(false);
                slot2Filled = false;
                ActiveToolList.Remove(Slot2Tools[0].GetComponent<Activity2ToolItemController>());
                ActiveToolList.Remove(Slot2Tools[1].GetComponent<Activity2ToolItemController>());
                ActiveToolList.Remove(Slot2Tools[2].GetComponent<Activity2ToolItemController>());
            }
            if (slotID == 3 && !FireParticleObject.activeInHierarchy)
            {
                Slot3Tools[0].SetActive(false);
                Slot3Tools[1].SetActive(false);
                Slot3Tools[2].SetActive(false);
                slot3Filled = false;
                ActiveToolList.Remove(Slot3Tools[0].GetComponent<Activity2ToolItemController>());
                ActiveToolList.Remove(Slot3Tools[1].GetComponent<Activity2ToolItemController>());
                ActiveToolList.Remove(Slot3Tools[2].GetComponent<Activity2ToolItemController>());
            }
            insertedSlotNumber--;
        }



    }

    public void Fire(bool enable)
    {
        FireParticleObject.SetActive(enable);
        if (enable)
        {
            firedSlotBG.sprite = outofOrderSprite;

        }
        Slot3Tools[0].SetActive(false);
        Slot3Tools[1].SetActive(false);
        Slot3Tools[2].SetActive(false);
       


    }
}
