﻿using UnityEngine;
using System.Collections;

public class Activity2ToolEjectButton : MonoBehaviour {
    public GameObject Text;
    public int slotID;
    public Activity2ToolController tc;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
    void OnMouseEnter()
    {
        if (HelperController.GameController.isPlayable)
        {
         //   Text.SetActive(true);
        }
    }
    void OnMouseExit()
    {
        if (HelperController.GameController.isPlayable)
        {
          //  Text.SetActive(false);
        }
    }
    public void OnMouseDown()
    {
     
        tc.ToolEject(slotID);
    }
}
