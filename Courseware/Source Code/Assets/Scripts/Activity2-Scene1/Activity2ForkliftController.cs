﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Activity2ForkliftController : MonoBehaviour
{
    public Vector3 ScaleNormal;
    public Vector3 ScaleHover;
    public char group;
    [HideInInspector]
    public int motorItemID = 0;
    public Texture2D mouseHandCursor;
    public ForklitStatus initialstatus;

    public GameObject[] motorLabels;

    public Activity2Scene1Contoller ac;
    [HideInInspector]
    public ForklitStatus status;
    // Use this for initialization
    void Awake()
    {
        ChangeStatus(initialstatus);
        if (ac != null)
        {
            ac = GameObject.FindGameObjectWithTag(HelperController.Tags.SceneController.ToString()).GetComponent<Activity2Scene1Contoller>();
        }
    }
    public void SetMotor(int id, char _group)
    {
        foreach (GameObject item in motorLabels)
        {
            item.SetActive(false);
        }
        motorLabels[id].SetActive(true);
        motorItemID = id;
        group = _group;
    }
    void OnMouseEnter()
    {
        if (status == ForklitStatus.Active && HelperController.GameController.isPlayable)
        {
            Vector2 hotspot = new Vector2(Input.mousePosition.x, Input.mousePosition.y);
            gameObject.transform.localScale = ScaleHover;
            //   Cursor.SetCursor(mouseHandCursor, hotspot, CursorMode.Auto);
        }
    }
    void OnMouseExit()
    {
        if (status == ForklitStatus.Active && HelperController.GameController.isPlayable)
        {
            Vector2 hotspot = new Vector2(Input.mousePosition.x, Input.mousePosition.y);
            gameObject.transform.localScale = ScaleNormal;
            //  Cursor.SetCursor(null, Vector2.zero, CursorMode.Auto);
        }
    }
    void OnMouseDown()
    {
        if (status == ForklitStatus.Active && HelperController.GameController.isPlayable)
        {
            ac.SetActiveMotor(group,motorItemID); 

        }
    }
    public void ChangeStatus(ForklitStatus s)
    {
        //Debug.Log(ForklitColors[status]);
        gameObject.GetComponent<SpriteRenderer>().color = ForklitColors[s];
        status = s;


    }


    // Helpers

    public Dictionary<ForklitStatus, Color> ForklitColors = new Dictionary<ForklitStatus, Color>()
    {
        {ForklitStatus.Active,new Color(255.0f, 255.0f, 255.0f)},
        {ForklitStatus.Passive,new Color(208, 208, 208, 0.5f)},
        {ForklitStatus.Done,new Color(0, 255.0f, 76.0f)}

    };
}
