﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System;

public class HelpController : MonoBehaviour
{
    public Button CloseHelpButton;
    PanelManager pm;


    private int m_OpenParameterId;
    const string k_OpenTransitionName = "Open";
    const string k_ClosedStateName = "Closed";

    void Start()
    {
        m_OpenParameterId = Animator.StringToHash(k_OpenTransitionName);
        pm = HelperController.PanelManager;
    }


    void OnEnable()
    {
        CloseHelpButton.onClick.RemoveAllListeners();
        pm = HelperController.PanelManager;

        if (SceneManager.GetActiveScene().name == HelperController.SceneNames[HelperController.Scenes.MainMenu])
        {
            GameObject menu = GameObject.Find("MainMenu");
            CloseHelpButton.onClick.AddListener(delegate { pm.OpenPanel(menu.GetComponent<Animator>()); });
        }
        else
        {

            CloseHelpButton.onClick.AddListener(
                delegate
                {
                    HelperController.GameController.PauseGame(false, false);
                    pm.CloseCurrent();
                });
            StartCoroutine(PauseGameDelayed());

        }

    }
    public void ClosePanel()
    {

    }
    private IEnumerator PauseGameDelayed()
    {
        Animator anim = gameObject.GetComponent<Animator>();

        bool closedStateReached = false;
        bool wantToClose = true;
        while (!closedStateReached && wantToClose)
        {
            if (!anim.IsInTransition(0))
                closedStateReached = anim.GetCurrentAnimatorStateInfo(0).IsName(k_ClosedStateName);

            wantToClose = !anim.GetBool("Open");

            yield return new WaitForEndOfFrame();
        }
        yield return new WaitWhile(() => anim.IsInTransition(0));
        HelperController.GameController.PauseGame(true, false);

    }

    // Update is called once per frame
    void Update()
    {

    }
}
