﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class TabletActivity4Scene2Controller : MonoBehaviour {

    public GameObject[] TaskPanels;
    public Button[] MenuButtons;
    public TabletActivity4AnswerModel[] FırlatmaAnswers;
    public TabletActivity4AnswerModel[] MekikAnswers;
    


	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}


public class TabletActivity4AnswerModel
{
    public GameObject Container;
    public Image ContainerBg;
    public Text Speed;
    public Text Distance;
    public Text Time;
    public bool isAnswered;
    public bool isCorrect;

    [SerializeField]
    public float[] AnswerMatrix = new float[3];

    public void SetAnswers(float speed, float distance, float time)
    {
        AnswerMatrix[0] = speed;
        AnswerMatrix[1] = distance;
        AnswerMatrix[2] = time;

    }
    public void ClearWrongAnswer()
    {
        Speed.text = "";
        Distance.text = "";
        Time.text = "";
            
    }
    public AnswerResponseType CheckAnswer()
    {

        string _distanceStr = Distance.text.Replace(",", ".");
        string _timeStr = Time.text.Replace(",", ".");
        string _speedStr = Speed.text.Replace(",", ".");

        AnswerResponseType response = AnswerResponseType.True;
        float _distance;
        float _time;
        float _speed;
        if (!float.TryParse(_speedStr, out _speed) || !float.TryParse(_distanceStr, out _distance) || !float.TryParse(_timeStr, out _time))
        {
            return AnswerResponseType.NotNumeric;
        }
        else
        {
            if (float.Parse(_speedStr) != AnswerMatrix[0])
            {
                response = AnswerResponseType.False;
            }
            if (float.Parse(_distanceStr) != AnswerMatrix[1])
            {
                response = AnswerResponseType.False;
            }
            if (float.Parse(_timeStr) != AnswerMatrix[2])
            {
                response = AnswerResponseType.False;
            }
            return response;
        }


    }
    public enum AnswerResponseType
    {
        True,
        False,
        NotNumeric,

    }
}