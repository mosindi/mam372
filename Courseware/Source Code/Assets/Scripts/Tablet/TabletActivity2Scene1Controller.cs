﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System;

public class TabletActivity2Scene1Controller : MonoBehaviour {
    public GameObject[] TaskPanels;
    public Button[] MenuButtons;
    public TabletActivity2AnswerModel[] FırlatmaAnswers;
    public TabletActivity2AnswerModel[] MekikAnswers;
    public TabletActivity2AnswerModel[] ManevraAnswers;

    void Awake()
    {
        if (HelperController.GameController.sceneController.GetSceneName()== HelperController.Scenes.Activity4_Scene2)
        {
            return;
        }
        DisableAllMenuButtons();
        SwitchOnTaskPanel(0);
        SwitchOnOffAnswerContainer(0, 0, false);
        CompleteAnswer(0, 0, false);
     
    }
    public void SwitchOnTaskPanel(int id)
    {

        for (int i = 0; i < TaskPanels.Length; i++)
        {
            TaskPanels[i].gameObject.SetActive(false);
        }
        TaskPanels[id].gameObject.SetActive(true);

    }

    public void SwitchOnOffAnswerContainer(int PanelId, int id, bool OnOff)
    {
        switch (PanelId)
        {
            case 0:
                for (int i = 0; i < FırlatmaAnswers.Length; i++)
                {
                    SwitchOnOffAnswerContainer(1, i, OnOff);

                }
                for (int i = 0; i < ManevraAnswers.Length; i++)
                {
                    SwitchOnOffAnswerContainer(2, i, OnOff);

                }
                for (int i = 0; i < MekikAnswers.Length; i++)
                {
                    SwitchOnOffAnswerContainer(3, i, OnOff);

                }
                break;
            case 1:
                FırlatmaAnswers[id].Container.SetActive(OnOff);
                break;
            case 2:
                MekikAnswers[id].Container.SetActive(OnOff);
                break;
            case 3:
                ManevraAnswers[id].Container.SetActive(OnOff);
                break;
            default:
                break;
        }
    }

    public void CompleteAnswer(int PanelId, int id, bool complete)
    {
        switch (PanelId)
        {
            case 0:
                for (int i = 0; i < FırlatmaAnswers.Length; i++)
                {
                    CompleteAnswer(1, i, complete);
                }
                for (int i = 0; i < ManevraAnswers.Length; i++)
                {
                    CompleteAnswer(2, i, complete);

                }
                for (int i = 0; i < MekikAnswers.Length; i++)
                {
                    CompleteAnswer(3, i, complete);
                }
                break;
            case 1:
                if (id < FırlatmaAnswers.Length)
                {
                    if (complete)
                    {
                        FırlatmaAnswers[id].ContainerBg.color = new Color(0, 255.0f, 76.0f);
                    }
                    else
                    {
                        FırlatmaAnswers[id].ContainerBg.color = new Color(208, 208, 208, 0.5f);

                    }
                }
                break;
            case 2:
                if (id < MekikAnswers.Length)
                {
                    if (complete)
                    {
                       MekikAnswers[id].ContainerBg.color = new Color(0, 255.0f, 76.0f);
                    }
                    else
                    {
                        MekikAnswers[id].ContainerBg.color = new Color(208, 208, 208, 0.5f);

                    }
                }
                break;
            case 3:
                if (id < ManevraAnswers.Length)
                {
                    if (complete)
                    {
                        ManevraAnswers[id].ContainerBg.color = new Color(0, 255.0f, 76.0f);
                    }
                    else
                    {
                        ManevraAnswers[id].ContainerBg.color = new Color(208, 208, 208, 0.5f);

                    }
                }
                break;
            default:
                break;
        }
    }
    public void EnableMenuButton(int id)
    {
        MenuButtons[id].interactable = true;
    }
    public void DisableAllMenuButtons()
    {
        for (int i = 0; i < MenuButtons.Length; i++)
        {
            MenuButtons[i].interactable = false;
        }
    }
    public void MenuButtonComplete(int id)
    {
        MenuButtons[id].GetComponent<Image>().color = new Color(0, 255, 76);
    }
}
[Serializable]
public class TabletActivity2AnswerModel
{
    public GameObject Container;
    public Image ContainerBg;
    public Text Speed;
    public Text Distance;
    public Text Time;
    public bool isAnswered;
    public bool isCorrect;
    [SerializeField]
    public float[] AnswerMatrix = new float[3];
    public void SetAnswers(float speed, float distance, float time)
    {
        AnswerMatrix[0] = speed;
        AnswerMatrix[1] = distance;
        AnswerMatrix[2] = time;
    }
    public void ClearWrongAnswer()
    {
        Speed.text = "";
        Distance.text = "";
        Time.text = "";
    }
    public AnswerResponseType CheckAnswer()
    {
    
        string _distanceStr = Distance.text.Replace(",", ".");
        string _timeStr =Time.text.Replace(",", ".");
        string _speedStr = Speed.text.Replace(",", ".");

        AnswerResponseType response = AnswerResponseType.True;
        float _distance;
        float _time;
        float _speed;
        if (!float.TryParse(_speedStr, out _speed) || !float.TryParse(_distanceStr, out _distance) || !float.TryParse(_timeStr, out _time))
        {
            return AnswerResponseType.NotNumeric;
        }
        else
        {
            if (float.Parse(_speedStr) != AnswerMatrix[0])
            {
                response = AnswerResponseType.False;
            }
            if (float.Parse(_distanceStr) != AnswerMatrix[1])
            {
                response = AnswerResponseType.False;
            }
            if (float.Parse(_timeStr) != AnswerMatrix[2])
            {
                response = AnswerResponseType.False;
            }
            return response;
        }


    }
    public enum AnswerResponseType
    {
        True,
        False,
        NotNumeric,

    }
}