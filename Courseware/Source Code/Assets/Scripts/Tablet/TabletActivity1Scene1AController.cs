﻿using UnityEngine;
using System.Collections;

public class TabletActivity1Scene1AController : MonoBehaviour {
    public GameObject[] MotorItems;
    public Transform[] InitialParents;
    public Transform[] FırlatmaDropArea;
    public Transform[] MekikAnaDropArea;
    public Transform[] ManevraDropArea;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
    public bool CheckAnswer()
    {
        bool response = true;
        for (int i = 0; i < MotorItems.Length;i=i+3)
        {
            if (MotorItems[i].transform.parent != FırlatmaDropArea[0] && 
                MotorItems[i].transform.parent != FırlatmaDropArea[1] &&
                MotorItems[i].transform.parent != FırlatmaDropArea[2])
            {
                MotorItems[i].transform.parent = InitialParents[i];
                response = false;
            }
            
        }
        for (int i = 1; i < MotorItems.Length; i = i + 3)
        {
            if (MotorItems[i].transform.parent != MekikAnaDropArea[0] &&
                MotorItems[i].transform.parent != MekikAnaDropArea[1] &&
                MotorItems[i].transform.parent != MekikAnaDropArea[2])
            {
                MotorItems[i].transform.parent = InitialParents[i];
                response = false;
            }

        }
        for (int i = 2; i < MotorItems.Length; i = i + 3)
        {
            if (MotorItems[i].transform.parent != ManevraDropArea[0] &&
                MotorItems[i].transform.parent != ManevraDropArea[1] &&
                MotorItems[i].transform.parent != ManevraDropArea[2])
            {
                MotorItems[i].transform.parent = InitialParents[i];
                response = false;
            }

        }
        return response;
    }

    public void Reset()
    {
        for (int i = 0; i < MotorItems.Length; i++)
        {
            MotorItems[i].transform.parent = InitialParents[i];
        }
    }
}
