﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class TabletController : MonoBehaviour {


    public GameObject[] Screens;
    public TabletActivity1Scene1Controller activity1scene1Controller;
    public TabletActivity1Scene1AController activity1Scene1aController;
    public TabletActivity2Scene1Controller activity2Scene1Controller;
    public TabletActivity2Scene1Controller activity4Scene1Controller;
    
    // Use this for initialization
    void Awake () {
        if (activity1scene1Controller != null)
        {
            activity1scene1Controller = GetComponentInChildren<TabletActivity1Scene1Controller>(true);
        }
       
    }
	
	// Update is called once per frame
	void Update () {
	
	}
    public void SwitcOn(TabletScreens screen)
    {
        for (int i = 0; i < Screens.Length; i++)
        {
            Screens[i].SetActive(false);
        }
        Screens[(int)screen].SetActive(true);
    }


   public void SetTabletActive(bool active)
    {
        if (HelperController.GameController.isPlayable)
        {
            gameObject.SetActive(active);
        }
    }
    public void OnClickCheckAnswerButton()
    {
        GameObject.FindGameObjectWithTag(HelperController.Tags.SceneController.ToString()).BroadcastMessage("CheckAnswer", SendMessageOptions.DontRequireReceiver);
    }
}


[Serializable]
public enum TabletScreens
{
    Default,
    Activity1Scene1,
    Activity1Scene1a,
    Activity2Scene1,
    Activity4Scene2

}