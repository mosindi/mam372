﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class TabletActivity1Scene1Controller : MonoBehaviour {

    public GameObject[] taskPanels;
    public Button[] MenuButtons;
    Activity1Scene1Controller ac;
	// Use this for initialization
	void Awake () {
        ac = GameObject.FindGameObjectWithTag(HelperController.Tags.SceneController.ToString()).GetComponent<Activity1Scene1Controller>();
        DisableAllMenuButtons();
        SwitchOnTaskPanel(0);
	}
	
	// Update is called once per frame
	void Update () {
	
	}
    public void SwitchOnTaskPanel(int id)
    {
       
        for (int i = 0; i < taskPanels.Length; i++)
        {
            taskPanels[i].gameObject.SetActive(false);
        }
        taskPanels[id].gameObject.SetActive(true);
        
    }
    public void EnableMenuButton(int id)
    {
        MenuButtons[id].interactable = true;
    }
    public void DisableAllMenuButtons()
    {
        for (int i = 0; i < MenuButtons.Length; i++)
        {
            MenuButtons[i].interactable = false;
        }
    }
    public void MenuButtonComplete(int id)
    {
        MenuButtons[id].GetComponent<Image>().color = new Color(0, 255, 76);
    }

    public void OnClickCheckAnswerButton()
    {
        GameObject.FindGameObjectWithTag(HelperController.Tags.SceneController.ToString()).BroadcastMessage("CheckAnswer", SendMessageOptions.DontRequireReceiver);
    }

}
