﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;

public class Activity5Scene2Controller : MonoBehaviour
{

    public TabletController tabletController;
    public Activity5Scene2ReportController reportController;
    public GameObject ToolContainer;
    public GameObject speechBubble;
    public Text speechBubbleText;
    public Animator agentAnim;
    [Range(0, 10)]
    public float textChangeSpeed;
    [Range(0, 10)]
    public float lunchCountDown;

    public Button IntroPassBtn;

    [HideInInspector]
    public Activity5Scene2MessageController sceneMessageController;

    private int agentAnimParamStartId;
    private int camAnimParamBuildId;
    private int activeTaskID = -1;

    private string agentAnimParamStart = "Interaction";
    private Coroutine IntroInteractionCoroutine;


    // Use this for initialization
    void Start()
    {
        TaskGenerate();
        sceneMessageController = GetComponent<Activity5Scene2MessageController>();
        HelperController.GameController.isPlayable = false;

        agentAnimParamStartId = Animator.StringToHash(agentAnimParamStart);
        speechBubble.SetActive(false);
        IntroPassBtn.gameObject.SetActive(false);
        //IntroInteractionCoroutine = StartCoroutine(IntroSpeakInteractionCoroutine());
        StartCoroutine(SpeakInteractionCoroutine(1));
    }

    // Update is called once per frame
    void Update()
    {

    }
    #region Intro
    private IEnumerator IntroSpeakInteractionCoroutine()
    {
        IntroPassBtn.gameObject.SetActive(true);
        agentAnim.gameObject.SetActive(true);
        yield return new WaitForSeconds(1.2f);
        speechBubble.SetActive(true);
        agentAnim.SetBool(agentAnimParamStartId, true);
        HelperController.GameController.isPlayable = false;
  //      speechBubbleText.text = "Uçuş için artık çok az bir zaman kaldı. Uçuş esnasında motorlardan gelen verileri toplaman ve bunları kayıt altına alman gerekiyor.";
        yield return new WaitWhile(() => speechBubble.activeSelf);
        //yield return new WaitForSeconds(HelperController.SpeechbublpeWaitTime(speechBubbleText.text, textChangeSpeed));


        agentAnim.SetBool(agentAnimParamStartId, false);
        speechBubble.SetActive(false);
        IntroPassBtn.gameObject.SetActive(false);
        HelperController.GameController.isPlayable = true;
        StartCoroutine(SpeakInteractionCoroutine(1));
    }
    public void OnClickPassIntro()
    {

        IntroPassBtn.gameObject.SetActive(false);
        StopCoroutine(IntroInteractionCoroutine);
        StartCoroutine(SpeakInteractionCoroutine(1));
    }
    #endregion
    private IEnumerator SpeakInteractionCoroutine(int interactionID)
    {
        yield return new WaitForSeconds(0.1f);


        InteractionObjectsActive(true);
        if (interactionID == 1)
        {
            speechBubbleText.text = "Tebrikler görevleri başarı ile tamamladın. Eve hoşgeldin. Şimdi son bir görevin var. Uçuş esnasında topladığın veriler ile rapor oluşturman gerekiyor. Bu sonraki araştırma görevleri için faydalı olacak.";
            yield return new WaitWhile(() => speechBubble.activeSelf);
            //yield return new WaitForSeconds(HelperController.SpeechbublpeWaitTime(speechBubbleText.text, textChangeSpeed));

            tabletController.SwitcOn(TabletScreens.Activity4Scene2);
            NextTask();
        }
        InteractionObjectsActive(false);

    }
    private void InteractionObjectsActive(bool status)
    {
        agentAnim.gameObject.SetActive(status);
        speechBubble.SetActive(status);
        agentAnim.SetBool(agentAnimParamStartId, status);
        HelperController.GameController.isPlayable = !status;
    }


    #region TaskOps
    public void NextTask()
    {
        StopAllCoroutines();
        if (activeTaskID + 1 < taskList.Count)
        {
            activeTaskID++;
            //  StartCoroutine(NextTaskInteraction());
            sceneMessageController.ShowMessage(HelperController.MessageType.Task, activeTaskID, HelperController.ButtonType.ClosePanel, true);
        }
        else
        {
            NextSection();
        }
    }

    public void TaskStarted()
    {

        HelperController.GameController.Playable(true);
        StartCoroutine(TaskProcess());
    }
    private IEnumerator TaskProcess()
    {
        switch (activeTaskID)
        {
            case 0:
                InteractionObjectsActive(true);
                speechBubbleText.text = "Raporları hazırlamak için \"Rapor\" aracını ekranın sağında bulunan \"Rapor\" düğmesine tıklayarak aç. Rapor aracının solunda topladığın veriler var bunlara bakarak grafik üzerinde noktaları belirle.";
                yield return new WaitWhile(() => speechBubble.activeSelf);
                //yield return new WaitForSeconds(HelperController.SpeechbublpeWaitTime(speechBubbleText.text, textChangeSpeed));
                speechBubble.SetActive(false);
                yield return new WaitForSeconds(0.1f);
                speechBubble.SetActive(true);


                InteractionObjectsActive(false);

                reportController.PageSwitch(1);
                reportController.ChangePageButtonStatus(0, Activity5Scene2ReportController.ButtonStatus.active);
                reportController.ChangePageButtonStatus(1, Activity5Scene2ReportController.ButtonStatus.passive);
                reportController.ChangePageButtonStatus(2, Activity5Scene2ReportController.ButtonStatus.passive);

                sceneMessageController.ShowMessage(HelperController.MessageType.Task, 0.1f, HelperController.ButtonType.ClosePanel, false);
                yield return new WaitUntil(() => reportController.gameObject.activeSelf);
                sceneMessageController.ShowMessage(HelperController.MessageType.Task, 0.2f, HelperController.ButtonType.ClosePanel, false);
                yield return new WaitUntil(() => reportController.isActivePagePointCorrect);


                sceneMessageController.ShowMessage(HelperController.MessageType.Success, 0, HelperController.ButtonType.Continue, true);
                yield return new WaitWhile(() => sceneMessageController.successMessagePanel.Button.transform.parent.gameObject.activeSelf);
                sceneMessageController.ShowMessage(HelperController.MessageType.Task, 0.3f, HelperController.ButtonType.Continue, true);

                yield return new WaitUntil(() => reportController.isActivePageGraphOk);
                yield return new WaitForSeconds(1);
                TaskComleted();

                break;
            case 1:
                reportController.gameObject.SetActive(false);
                InteractionObjectsActive(true);
                speechBubbleText.text = "Fırlatma motorları için hazırladığın raporun bir benzerinde Mekik motorları için hazırlamalısın. Noktaları yerleştirirken birim ve değerlere dikkat et.";
                yield return new WaitWhile(() => speechBubble.activeSelf);
                //yield return new WaitForSeconds(HelperController.SpeechbublpeWaitTime(speechBubbleText.text, textChangeSpeed));
                InteractionObjectsActive(false);

                reportController.PageSwitch(2);
                reportController.ChangePageButtonStatus(0, Activity5Scene2ReportController.ButtonStatus.done);
                reportController.ChangePageButtonStatus(1, Activity5Scene2ReportController.ButtonStatus.active);
                reportController.ChangePageButtonStatus(2, Activity5Scene2ReportController.ButtonStatus.passive);
                sceneMessageController.ShowMessage(HelperController.MessageType.Task, 0.1f, HelperController.ButtonType.ClosePanel, false);
                yield return new WaitUntil(() => reportController.gameObject.activeSelf);
                sceneMessageController.ShowMessage(HelperController.MessageType.Task, 0.2f, HelperController.ButtonType.ClosePanel, false);
                yield return new WaitUntil(() => reportController.isActivePagePointCorrect);


                sceneMessageController.ShowMessage(HelperController.MessageType.Success, 0, HelperController.ButtonType.Continue, true);
                yield return new WaitWhile(() => sceneMessageController.successMessagePanel.Button.transform.parent.gameObject.activeSelf);
                sceneMessageController.ShowMessage(HelperController.MessageType.Task, 0.3f, HelperController.ButtonType.Continue, true);

                yield return new WaitUntil(() => reportController.isActivePageGraphOk);
                yield return new WaitForSeconds(1);
                TaskComleted();
                break;
            case 2:
                reportController.gameObject.SetActive(false);
                InteractionObjectsActive(true);
                speechBubbleText.text = "Son raporumuz olan Manevra Motorları raporunu da hazırlamalısın. Birimlere ve değerlere dikkat etmeyi unutma.";
                yield return new WaitWhile(() => speechBubble.activeSelf);
                //yield return new WaitForSeconds(HelperController.SpeechbublpeWaitTime(speechBubbleText.text, textChangeSpeed));
                InteractionObjectsActive(false);

                reportController.PageSwitch(3);
                reportController.ChangePageButtonStatus(0, Activity5Scene2ReportController.ButtonStatus.done);
                reportController.ChangePageButtonStatus(1, Activity5Scene2ReportController.ButtonStatus.done);
                reportController.ChangePageButtonStatus(2, Activity5Scene2ReportController.ButtonStatus.active);
                sceneMessageController.ShowMessage(HelperController.MessageType.Task, 0.1f, HelperController.ButtonType.ClosePanel, false);
                yield return new WaitUntil(() => reportController.gameObject.activeSelf);
                sceneMessageController.ShowMessage(HelperController.MessageType.Task, 0.2f, HelperController.ButtonType.ClosePanel, false);
                yield return new WaitUntil(() => reportController.isActivePagePointCorrect);


                sceneMessageController.ShowMessage(HelperController.MessageType.Success, 0, HelperController.ButtonType.Continue, true);
                yield return new WaitWhile(() => sceneMessageController.successMessagePanel.Button.transform.parent.gameObject.activeSelf);
                sceneMessageController.ShowMessage(HelperController.MessageType.Task, 0.3f, HelperController.ButtonType.Continue, true);

                yield return new WaitUntil(() => reportController.isActivePageGraphOk);
                yield return new WaitForSeconds(1);
                reportController.gameObject.SetActive(false);
                TaskComleted();
                break;
            default:
                break;
        }
    }

    public void CheckAnswer()
    {

    }



    public void TaskComleted()
    {
        StopAllCoroutines();
        sceneMessageController.ShowMessage(HelperController.MessageType.Success, 1, HelperController.ButtonType.ClosePanel, true);
    }

    public void RestartTask()
    {
        if (activeTaskID > -1)
        {
            StopAllCoroutines();
            activeTaskID--;
            NextTask();
        }

    }
    #endregion



    #region SectionOps
    public void NextSection()
    {
        StopAllCoroutines();
        StartCoroutine(NextSectionProcess());
    }

    private IEnumerator NextSectionProcess()
    {
        InteractionObjectsActive(true);
        speechBubbleText.text = "Tüm raporlar hazırlandı. Bu projedeki tüm görevler böylece tamamlanmış oldu.";
        yield return new WaitWhile(() => speechBubble.activeSelf);
        //yield return new WaitForSeconds(HelperController.SpeechbublpeWaitTime(speechBubbleText.text, textChangeSpeed));


        InteractionObjectsActive(false);
        agentAnim.gameObject.SetActive(true);
        sceneMessageController.taskMessagePanel.ButtonLabel.text = "Bitir";
        sceneMessageController.ShowMessage(HelperController.MessageType.Section, 1f, HelperController.ButtonType.ClosePanel, false);

    }
    public void NextSectionGo()
    {
        StopAllCoroutines();
        HelperController.GameController.sceneController.LoadNextScene();
    }
    #endregion

    #region List
    public List<TaskController.TaskStruct> taskList = new List<TaskController.TaskStruct>();
    private void TaskGenerate()
    {
        taskList.Add(new TaskController.TaskStruct("Fırlatma Motorları için rapor oluştur."));
        taskList.Add(new TaskController.TaskStruct("Mekik Ana Motorları için rapor oluştur."));
        taskList.Add(new TaskController.TaskStruct("Manevra Motorları için rapor oluştur."));

        HelperController.TaskController.TaskListFillOnSceneStart(taskList);

    }
    #endregion
    #region Helper
    public float SpeechbublpeWaitTime()
    {
        return (speechBubbleText.text.Length * Time.deltaTime / textChangeSpeed) + 1.5f;
    }
    #endregion
}
