﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System;

public class Activity5Scene2ReportController : MonoBehaviour
{

    public GameObject[] Pages;
    public Button[] PageButtons;
    public Sprite PageButtonActiveSprite;
    public Sprite PageButtonInActiveSprite;

    public ReportPageStruct[] PageStruct;

    [HideInInspector]
    public bool isActivePagePointCorrect = false;

    [HideInInspector]
    public bool isActivePageGraphOk = false;

    private int activePageId;
    private Activity5Scene2Controller ac;


    // Use this for initialization
    void Awake()
    {
        ac = GameObject.FindGameObjectWithTag(HelperController.Tags.SceneController.ToString()).GetComponent<Activity5Scene2Controller>();
        // PageSwitch(0);

    }

    // Update is called once per frame
    void Update()
    {

    }

    public void ReportActive(bool active)
    {
        if (HelperController.GameController.isPlayable && !HelperController.GameController.isPaused)
        {
            gameObject.SetActive(active);
        }
    }
    public void ChangePageButtonStatus(int id, ButtonStatus st)
    {
        switch (st)
        {
            case ButtonStatus.active:
                PageButtons[id].interactable = true;
                PageButtons[id].GetComponent<Image>().sprite = PageButtonActiveSprite;
                PageStruct[id].ChekPoints.interactable = true;
                PageStruct[id].DrawGraph.interactable = true;
                isActivePagePointCorrect = false;
                isActivePageGraphOk = false;
                break;
            case ButtonStatus.passive:
                PageButtons[id].interactable = false;
                PageButtons[id].GetComponent<Image>().sprite = PageButtonInActiveSprite;
                PageStruct[id].ChekPoints.interactable = false;
                PageStruct[id].DrawGraph.interactable = false;
                break;
            case ButtonStatus.done:
                PageButtons[id].interactable = true;
                PageStruct[id].ChekPoints.interactable = false;
                PageStruct[id].DrawGraph.interactable = false;
                PageStruct[id].GraphCompleteObject.SetActive(true);
                PageStruct[id].PointButtonContainer.SetActive(false);
                isActivePagePointCorrect = true;
                isActivePageGraphOk = true;

                PageButtons[id].GetComponent<Image>().sprite = PageButtonInActiveSprite;

                break;
            default:
                break;
        }
    }
    public void PageSwitch(int id)
    {
        foreach (GameObject item in Pages)
        {
            item.SetActive(false);
        }
        Pages[id].SetActive(true);
        //PageStruct[id - 1].GraphCompleteObject.SetActive(false);
        //PageStruct[id - 1].DrawGraph.interactable = false;
        //isActivePagePointCorrect = false;
        //isActivePageGraphOk = false;

        foreach (Button item in PageButtons)
        {
            item.GetComponent<Image>().sprite = PageButtonInActiveSprite;
        }
        if (id != 0)
        {
            PageButtons[id - 1].GetComponent<Image>().sprite = PageButtonActiveSprite;
        }
        activePageId = id;
    }

    public void OnClickGraphPoint(int id)
    {
        PageStruct[activePageId - 1].PointButtonOnClicked(id);

    }

    public void CheckPoint()
    {
        if (PageStruct[activePageId - 1].CheckPoint())
        {
            PageStruct[activePageId - 1].DrawGraph.interactable = true;
            Debug.Log("Doğru");
            isActivePagePointCorrect = true;
        }
        else
        {

            ac.sceneMessageController.ShowMessage(HelperController.MessageType.Failure, 0.0f, HelperController.ButtonType.ClosePanel, true);
            Debug.Log("Yanlış");
        }


    }

    public void ShowGraph()
    {
        if (PageStruct[activePageId - 1].isPointsCorrect)
        {
            PageStruct[activePageId - 1].PointButtonContainer.SetActive(false);
            PageStruct[activePageId - 1].GraphCompleteObject.SetActive(true);
            isActivePageGraphOk = true;

        }
        else
        {
            isActivePageGraphOk = false;

            ac.sceneMessageController.ShowMessage(HelperController.MessageType.Failure, 0.0f, HelperController.ButtonType.ClosePanel, true);
            Debug.Log("Noktalarda hata var");
        }

    }

    public enum ButtonStatus
    {
        active,
        passive,
        done
    }
}
[Serializable]
public class ReportPageStruct
{
    public GameObject PageContainer;
    public Button ChekPoints;
    public Button DrawGraph;
    public GameObject GraphCompleteObject;
    public GameObject PointButtonContainer;
    public GraphPointStruct[] y1Points;
    public GraphPointStruct[] y2Points;
    public GraphPointStruct[] y3Points;
    public GraphPointStruct[] y4Points;
    public GraphPointStruct[] y5Points;
    public GraphPointStruct[] y6Points;

    public bool isPointsCorrect = false;


    public void PointButtonOnClicked(int id)
    {
        int y = id / 10;
        int x = id - y * 10;
        // Debug.Log("id:"+id+"--"+"y"+y.ToString()+"x"+x.ToString());

        if (y == 1)
        {
            //  Debug.Log("if:"+x);
            y1Points[x - 1].Clicked();
        }
        if (y == 2)
        {
            y2Points[x - 1].Clicked();
        }
        if (y == 3)
        {
            y3Points[x - 1].Clicked();
        }
        if (y == 4)
        {
            y4Points[x - 1].Clicked();
        }
        if (y == 5)
        {
            y5Points[x - 1].Clicked();
        }
        if (y == 6)
        {
            y6Points[x - 1].Clicked();
        }
    }

    public bool CheckPoint()
    {
        bool response = true;
        foreach (GraphPointStruct item in y1Points)
        {
            if (item.isCorrect)
            {
                if (!item.isClicked)
                {
                    response = false;
                }
            }
            if (item.isClicked)
            {
                if (!item.isCorrect)
                {
                    response = false;
                }
            }
        }
        foreach (GraphPointStruct item in y2Points)
        {
            if (item.isCorrect)
            {
                if (!item.isClicked)
                {
                    response = false;
                }
            }
            if (item.isClicked)
            {
                if (!item.isCorrect)
                {
                    response = false;
                }
            }
        }
        foreach (GraphPointStruct item in y3Points)
        {
            if (item.isCorrect)
            {
                if (!item.isClicked)
                {
                    response = false;
                }
            }
            if (item.isClicked)
            {
                if (!item.isCorrect)
                {
                    response = false;
                }
            }
        }
        foreach (GraphPointStruct item in y4Points)
        {
            if (item.isCorrect)
            {
                if (!item.isClicked)
                {
                    response = false;
                }
            }
            if (item.isClicked)
            {
                if (!item.isCorrect)
                {
                    response = false;
                }
            }
        }
        foreach (GraphPointStruct item in y5Points)
        {
            if (item.isCorrect)
            {
                if (!item.isClicked)
                {
                    response = false;
                }
            }
            if (item.isClicked)
            {
                if (!item.isCorrect)
                {
                    response = false;
                }
            }
        }
        foreach (GraphPointStruct item in y6Points)
        {
            if (item.isCorrect)
            {
                if (!item.isClicked)
                {
                    response = false;
                }
            }
            if (item.isClicked)
            {
                if (!item.isCorrect)
                {
                    response = false;
                }
            }
        }
        isPointsCorrect = response;
        return response;
    }

    #region ClickedMethod Not used
    public void y6ButtonClicked(int id)
    {
        throw new NotImplementedException();
    }



    public void y5ButtonClicked(int id)
    {
        throw new NotImplementedException();
    }

    public void y4ButtonClicked(int id)
    {
        throw new NotImplementedException();
    }

    public void y3ButtonClicked(int id)
    {
        throw new NotImplementedException();
    }

    public void y2ButtonClicked(int id)
    {
        throw new NotImplementedException();
    }

    public void y1ButtonClicked(int id)
    {

    }

    #endregion
}
[Serializable]
public class GraphPointStruct
{
    public Button point;
    public bool isCorrect;
    public bool isClicked = false;

    private Color initialcolor;
    private Color ClickedColor;
    public GraphPointStruct(Button _b, bool _c, bool _cl)
    {
        point = _b;
        isCorrect = _c;
        isClicked = _cl;
    }

    public void Clicked()
    {
        float r = 208.0f / 255.0f;
        initialcolor = new Color(r, r, r, 1);
        isClicked = !isClicked;
        point.gameObject.GetComponent<Image>().color = (isClicked) ? Color.yellow : initialcolor;
    }
}