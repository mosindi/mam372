﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;

public class Activity5Scene2MessageController : MonoBehaviour {

    public Text HudBottomMessageText;
    public HudMessageStruct taskMessagePanel;
    public HudMessageStruct successMessagePanel;
    public HudMessageStruct FailulerMessagePanel;

    // Use this for initialization
    void Start()
    {
        HudBottomMessageText.text = "";
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void ShowMessage(HelperController.MessageType mType, float id = 0, HelperController.ButtonType buttonEvent = HelperController.ButtonType.ClosePanel, bool notification = false)
    {

        switch (mType)
        {
            case HelperController.MessageType.Task:
                if (ActivityMessageListTask.Count > id)
                {
                    //   HelperController.GameController.isPlayable = false;
                    if (notification)
                    {
                        ShowNewTaskNotify(id);
                    }
                    HudBottomMessageText.text = ActivityMessageListTask[id];
                }

                break;
            case HelperController.MessageType.Warning:
                break;
            case HelperController.MessageType.Success:
                ShowSuccessNotify((int)id);
                HudBottomMessageText.text = "";
                break;
            case HelperController.MessageType.Failure:
                ShowFaillureNotify(id);
                break;
            case HelperController.MessageType.Section:
                ShowSectionNotify((int)id);
                HudBottomMessageText.text = ActivityMessageListSection[id];
                break;
            default:
                break;
        }
        AddEventListenerToButton(mType, buttonEvent);

    }
    private void AddEventListenerToButton(HelperController.MessageType mType, HelperController.ButtonType buttonEvent = HelperController.ButtonType.ClosePanel)
    {
        switch (mType)
        {
            #region Task
            case HelperController.MessageType.Task:
                taskMessagePanel.Button.onClick.RemoveAllListeners();
                switch (buttonEvent)
                {

                    case HelperController.ButtonType.ClosePanel:
                        taskMessagePanel.Button.onClick.AddListener(delegate
                        {
                            taskMessagePanel.Button.transform.parent.gameObject.SetActive(false);
                            HelperController.GameController.isPlayable = true;
                            gameObject.BroadcastMessage("TaskStarted");
                        });
                        break;
                    case HelperController.ButtonType.NextSection:
                        taskMessagePanel.Button.onClick.AddListener(delegate
                        {
                            taskMessagePanel.Button.transform.parent.gameObject.SetActive(false);
                            HelperController.GameController.isPlayable = true;
                            //  Debug.Log("Task Message Pane Button");
                            HelperController.GameController.sceneController.LoadNextScene();
                        });

                        break;
                    case HelperController.ButtonType.Continue:
                        taskMessagePanel.Button.onClick.AddListener(delegate
                        {
                            taskMessagePanel.Button.transform.parent.gameObject.SetActive(false);
                            HelperController.GameController.isPlayable = true;
                            //   gameObject.BroadcastMessage("TaskStarted");
                        });
                        break;
                    default:
                        break;
                }
                break;
            #endregion
            case HelperController.MessageType.Warning:
                break;
            case HelperController.MessageType.Success:
                successMessagePanel.Button.onClick.RemoveAllListeners();
                switch (buttonEvent)
                {
                    case HelperController.ButtonType.ClosePanel:
                        successMessagePanel.Button.onClick.AddListener(delegate
                        {
                            successMessagePanel.Button.transform.parent.gameObject.SetActive(false);
                            HelperController.GameController.isPlayable = true;
                            gameObject.BroadcastMessage("NextTask");
                        });
                        break;
                    case HelperController.ButtonType.NextSection:
                        successMessagePanel.Button.onClick.AddListener(delegate
                        {
                            successMessagePanel.Button.transform.parent.gameObject.SetActive(false);
                            HelperController.GameController.isPlayable = true;
                            HelperController.GameController.sceneController.LoadNextScene();
                        });

                        break;
                    case HelperController.ButtonType.Continue:
                        successMessagePanel.Button.onClick.AddListener(delegate
                        {
                            successMessagePanel.Button.transform.parent.gameObject.SetActive(false);
                            HelperController.GameController.isPlayable = true;

                        });
                        break;
                    default:
                        break;
                }
                break;
            case HelperController.MessageType.Failure:
                switch (buttonEvent)
                {
                    case HelperController.ButtonType.ClosePanel:
                        FailulerMessagePanel.Button.onClick.AddListener(delegate
                        {
                            FailulerMessagePanel.Button.transform.parent.gameObject.SetActive(false);
                            HelperController.GameController.isPlayable = true;
                            //     BroadcastMessage("RestartMission");
                        });

                        break;
                    case HelperController.ButtonType.HudToolBox:
                        break;
                    case HelperController.ButtonType.NextSection:
                        break;
                    default:
                        break;
                }

                break;
            case HelperController.MessageType.Section:
                taskMessagePanel.Button.onClick.RemoveAllListeners();
                switch (buttonEvent)
                {

                    case HelperController.ButtonType.ClosePanel:
                        taskMessagePanel.Button.onClick.AddListener(delegate
                        {
                            taskMessagePanel.Button.transform.parent.gameObject.SetActive(false);
                            HelperController.GameController.isPlayable = true;
                            gameObject.BroadcastMessage("NextSectionGo");
                        });
                        break;
                    case HelperController.ButtonType.NextSection:
                        taskMessagePanel.Button.onClick.AddListener(delegate
                        {
                            taskMessagePanel.Button.transform.parent.gameObject.SetActive(false);
                            HelperController.GameController.isPlayable = true;
                            //  Debug.Log("Task Message Pane Button");
                            HelperController.GameController.sceneController.LoadNextScene();
                        });

                        break;
                    default:
                        break;
                }
                break;
            default:
                break;
        }
    }

    private void ShowNewTaskNotify(float id)
    {
        taskMessagePanel.Button.transform.parent.gameObject.SetActive(true);
        taskMessagePanel.Text.text = ActivityMessageListTask[id];
        HelperController.GameController.isPlayable = false;

    }
    private void ShowSuccessNotify(int id)
    {
        successMessagePanel.Button.transform.parent.gameObject.SetActive(true);
        successMessagePanel.Text.text = ActivityMessageListSuccess[id];
        HelperController.GameController.isPlayable = false;

    }
    private void ShowFaillureNotify(float id)
    {
        FailulerMessagePanel.Button.transform.parent.gameObject.SetActive(true);
        FailulerMessagePanel.Text.text = ActivityMessageListFailure[id];
        HelperController.GameController.isPlayable = false;

    }
    private void ShowSectionNotify(int id)
    {
        taskMessagePanel.Button.transform.parent.gameObject.SetActive(true);
        taskMessagePanel.Text.text = ActivityMessageListSection[id];
        HelperController.GameController.isPlayable = false;
    }


    #region Lists
    #region Task
    public static Dictionary<float, string> ActivityMessageListTask = new Dictionary<float, string>()
    {
         {0.0f,"Fırlatma motorları için raporu hazırla." },
         {0.1f,"Ekranın sağında bulunan rapor düğmesine tıkla ve rapor aracını aç." },
         {0.2f,"Raporun sol tarafındaki bilgileri kullanarak grafik üzerindeki noktalrı işaretle. Sonra da Noktaları kontrol et." },
         {0.3f,"Noktaları doğru işaretlediğine göre artık grafiği oluşturmak için Grafiği Tamamla düğmesine tıkla ve raporu tamamla." },
         { 1.0f,"Mekik motorları için raporu hazırla." },

        { 2.0f,"Manevra motorları için raporu hazırla." },


    };
    #endregion
    #region Success
    public static Dictionary<int, string> ActivityMessageListSuccess = new Dictionary<int, string>()
    {
        {0,"Noktaları doğru işaretledin tebrikler!" },
        {1,"Raporu başarı ile tamamladın, diğer rapora geçebiliriz" },
        {2,"Tüm Görevleri başarı ile tamamladın, devam edebiliriz." },
        {3,"Ölçüm cihazlarını doğru ayarladın. Tebrikler!" },
        {4,"Fırlatma anında Fırlatma motorları verileri toplandı. Tebrikler!" },
        {5,"Mars araştırma istasyonuna doğru ilerlerken Mekik motoru verileri toplandı. Tebrikler!" },
        {6,"İstasyona kenetlenmek anında Manevra motorlarının verileri toplandı. Tebrikler!" },

    };
    #endregion
    #region Failure
    public static Dictionary<float, string> ActivityMessageListFailure = new Dictionary<float, string>()
    {
        {0.0f,"Doğru noktaları işaretlemedin daha dikkatli ol ve tekrar dene." },
        {0.1f,"Yalnızca rakamlardan oluşan değer gir" },

        {1.1f,"Ölçüm cihazları için seçtiğin birimler doğru değil. Daha dikkatli ol." },
        {1.2f,"Yol Ölçer için seçtiğin birim doğru değil." },
        {1.3f,"Zaman Ölçer için seçtiğin birim doğru değil." },
        {1.4f,"Hız Ölçer için seçtiğin birim doğru değil." },
        {1.5f,"Yol ve Zaman Ölçerler için seçtiğin birimler doğru değil." },
        {1.6f,"Yol ve Hız Ölçerler için seçtiğin birimler doğru değil." },
        {1.7f,"Zaman ve Hız Ölçerler için seçtiğin birimler doğru değil." },

    };
    #endregion
    #region Speech
    public static Dictionary<int, string> ActivityMessageListSpeech = new Dictionary<int, string>()
    {
        {0,"Merhaba" },
        {1,"" },
        {2,"" },
    };
    #endregion
    #region Section
    public static Dictionary<float, string> ActivityMessageListSection = new Dictionary<float, string>()
    {
        {0,"" },
        {1,"Tebrikler tüm görevleri bitirdin. Artık çalışmayı bitirebilir misin."}
    };
    #endregion
    #endregion
}
