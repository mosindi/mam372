﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using System;

public class LoaderController : MonoBehaviour
{
    //  public RectTransform LoaderCanvas;
    private GameController gc;
    private int currentLevelID;
    private float timeElapesed = 0;
    //public data
    public bool isSplash;
    [Range(0.0f, 10.0f)]
    public float SplashTime;
    public bool isDev;

    //exposed data
    [SerializeField]
    private FadeSprite m_blackScreenCover;
    [SerializeField]
    [Range(0.0f, 5.0f)]
    float m_minDuration = 2.0f;
    // Use this for initialization
    void Start()
    {

        gc = GameObject.FindGameObjectWithTag(HelperController.Tags.GameController.ToString()).GetComponent<GameController>();
        m_blackScreenCover.gameObject.SetActive(false);
        SpashCheck();
    }

    private void SpashCheck()
    {
        if (isSplash)
        {
            StartCoroutine(Splash());
            isSplash = false;
        }
        else
        {
#if UNITY_EDITOR
            if (!isDev && !isSplash)
            {
                DestroyImmediate(gameObject);
            }
#endif
        }


    }
    private IEnumerator Splash()
    {
        yield return new WaitForSeconds(m_minDuration);
        LoadScene(HelperController.Scenes.MainMenu);

    }

    public void LoadSceneImmediately(HelperController.Scenes scene, LoadSceneMode loadSceneMode = LoadSceneMode.Single)
    {
        SceneManager.LoadScene((int)scene, LoadSceneMode.Single);
    }

    public void LoadScene(HelperController.Scenes scene, LoadSceneMode loadSceneMoe = LoadSceneMode.Single)
    {
        StartCoroutine(_LoadSceneAsyc(scene, loadSceneMoe));
    }

    private IEnumerator _LoadSceneAsyc(HelperController.Scenes scene, LoadSceneMode loadSceneMoe = LoadSceneMode.Single)
    {
        m_blackScreenCover.gameObject.SetActive(true);
        gc.SoundMuteAll();

        // Fade to black
        // yield return StartCoroutine(m_blackScreenCover.FadeIn());

        // Load loading screen
        yield return SceneManager.LoadSceneAsync((int)HelperController.Scenes.Loader, LoadSceneMode.Additive);


        // !!! unload old screen (automatic)

        // Fade to loading screen
        // yield return StartCoroutine(m_blackScreenCover.FadeOut());
        float endTime = Time.time + m_minDuration;

        //    yield return StartCoroutine(m_blackScreenCover.FadeIn());

        // Load level async

        AsyncOperation loading = SceneManager.LoadSceneAsync((int)scene, loadSceneMoe);

        yield return loading;

    
        // Fade to black
        gc.SoundUnMuteAll();
        //      yield return new WaitForSeconds(m_minDuration);

        // !!! unload loading screen
        SceneManager.UnloadScene((int)HelperController.Scenes.Loader);

        // Fade to new screen
        // yield return StartCoroutine(m_blackScreenCover.FadeOut());

        //Disctivate black screen cover
        m_blackScreenCover.gameObject.SetActive(false);


    }
    public static void UnloadLoadingScene()
    {
        //  GameObject.Destroy(gameObject);
        SceneManager.UnloadScene((int)HelperController.Scenes.Loader);
    }
}
