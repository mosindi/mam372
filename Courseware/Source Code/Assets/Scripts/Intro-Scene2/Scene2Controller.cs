﻿using UnityEngine;
using System.Collections;
using System;
using UnityEngine.UI;
using System.Collections.Generic;

public class Scene2Controller : MonoBehaviour
{

    public GameObject speechBubble;
    public Text speechBubbleText;
    public Animator agentAnim;
    [Range(0, 10)]
    public float textChangeSpeed;
    Scene2MessageController sceneMessageController;
    public Button IntroPassBtn;

    public GameObject HudToolBox;
    //public Button HudToolBoxClose;
    public GameObject HudTablet;
 //   public Button HudTabletClose;

    private Coroutine IntroInteractionCoroutine;
    private string animParamStart = "Interaction";
    private int animParamStartId;
    private bool toolBoxIsClicked;
    private bool tabletIsClicked;
    private int activeTaskID = -1;
    // Use this for initialization
    void Awake()
    {
        sceneMessageController = GetComponent<Scene2MessageController>();
        HelperController.GameController.isPlayable = false;
        animParamStartId = Animator.StringToHash(animParamStart);
        speechBubble.SetActive(false);
        IntroInteractionCoroutine=StartCoroutine(IntroSpeakInteractionCoroutine());
        TasksGenerate();
    }
    void Update()
    {
     

    }
    private IEnumerator IntroSpeakInteractionCoroutine()
    {
        yield return new WaitForSeconds(1);
        speechBubble.SetActive(true);
        agentAnim.SetBool(animParamStartId, true);
        speechBubbleText.text = "Merhaba, ben bu tesisin müdürüyüm sen de Mars'a gidecek ekibin içerisinde ki makina mühendisimiz olmalısın. Aramıza hoş geldin.";
        //        yield return new WaitForSeconds(HelperController.SpeechbublpeWaitTime(speechBubbleText.text, textChangeSpeed));
        yield return new WaitWhile(()=>speechBubble.activeSelf);
        speechBubble.SetActive(false);
        yield return new WaitForSeconds(0.1f);
        speechBubble.SetActive(true);

        speechBubbleText.text = "Mars'a ilk insanlı uçuş görevi için hazırlıklar yapıyoruz. Bu uçuş görevinde motorlar senin sorumluluğunda. Kullanılacak motorları araştırıp hangilerinin kullanılacağına sen karar vereceksin.";
        //yield return new WaitForSeconds(HelperController.SpeechbublpeWaitTime(speechBubbleText.text, textChangeSpeed));
        yield return new WaitWhile(() => speechBubble.activeSelf);

        speechBubble.SetActive(false);
        yield return new WaitForSeconds(0.1f);
        speechBubble.SetActive(true);

        speechBubbleText.text = "Daha sonrasında görev sırasında motorların durumları ile ilgili sonraki uçuş görevlerinde kullanılmak üzere raporlar hazırlayacaksın.";
      //  yield return new WaitForSeconds(HelperController.SpeechbublpeWaitTime(speechBubbleText.text, textChangeSpeed));
        yield return new WaitWhile(() => speechBubble.activeSelf);

        agentAnim.SetBool(animParamStartId, false);
        speechBubble.SetActive(false);
        IntroPassBtn.gameObject.SetActive(false);
        StartCoroutine(SpeakInteractionCoroutine());
    }
    private IEnumerator SpeakInteractionCoroutine()
    {
        yield return new WaitForSeconds(0.1f);
        speechBubble.SetActive(true);
        agentAnim.SetBool(animParamStartId, true);
        speechBubbleText.text = "İşe başlamadan göstermem gerekenler var. Bu oryantasyon görevlerini yerine getir.";
     //   yield return new WaitForSeconds(HelperController.SpeechbublpeWaitTime(speechBubbleText.text, textChangeSpeed));
        yield return new WaitWhile(() => speechBubble.activeSelf);

        agentAnim.SetBool(animParamStartId, false);
        speechBubble.SetActive(false);
        //yield return new WaitForSeconds(1);
        NextTask();
    }
    public void OnClickPassIntro()
    {
        agentAnim.SetBool(animParamStartId, false);
        speechBubble.SetActive(false);
        IntroPassBtn.gameObject.SetActive(false);
        StopCoroutine(IntroInteractionCoroutine);
        StartCoroutine(SpeakInteractionCoroutine());
    }
    #region TaskOps
    private void NextTask()
    {
        if (activeTaskID + 1 < taskList.Count)
        {
            activeTaskID++;
            sceneMessageController.ShowMessage(HelperController.MessageType.Task, activeTaskID, HelperController.ButtonType.ClosePanel, true);
        }
        else
        {
            NextSection();
        }


    }

    public void TaskStarted()
    {
        StartCoroutine(TaskInteraction(activeTaskID));
    }
    public void TaskCompleted()
    {
        HelperController.TaskController.TaskComplete(activeTaskID);
        sceneMessageController.ShowMessage(HelperController.MessageType.Success, 1);
    }
    private IEnumerator TaskInteraction(int id)
    {

        speechBubble.SetActive(true);
        agentAnim.SetBool(animParamStartId, true);
        if (id == 0)
        {
            HelperController.GameController.isPlayable = true;
            speechBubbleText.text = "Oyunu oynarken oyunu nasıl oynayacağın, verilen görevi nasıl yapacağın konusunda yardım almak istediğin yukarıda bulunan \"?\" Yardım düğmesine tıklayarak yardım alabilirsin. Yardım düğmesine tıkla";
            sceneMessageController.ShowMessage(HelperController.MessageType.Task, 0.1f, HelperController.ButtonType.ClosePanel, false);
            yield return new WaitUntil(() => HelperController.HelpController.gameObject.activeInHierarchy);

            sceneMessageController.ShowMessage(HelperController.MessageType.Task, 0.2f, HelperController.ButtonType.ClosePanel, false);
            yield return new WaitWhile(() => HelperController.HelpController.gameObject.activeInHierarchy);
            yield return new WaitForFixedUpdate();
            TaskCompleted();
        }
        if (id == 1)
        {
            HelperController.GameController.isPlayable = true;

            speechBubbleText.text = "Görevleri yapabilmek için gerekli tüm araçlar \"Alet Çantası\"nın içerisinde. Alet Çantası'nı açmak için Alet Çantası düğmesine tıkla!";
            sceneMessageController.ShowMessage(HelperController.MessageType.Task, 1.1f, HelperController.ButtonType.ClosePanel, false);
            yield return new WaitUntil(() => HudToolBox.activeInHierarchy);

            sceneMessageController.ShowMessage(HelperController.MessageType.Task, 1.2f, HelperController.ButtonType.HudToolBox, false);
            HelperController.GameController.isPlayable = true;

            yield return new WaitWhile(() => HudToolBox.activeInHierarchy);
            TaskCompleted();
        }
        if (id == 2)
        {
            HelperController.GameController.isPlayable = true;

            speechBubbleText.text = "Görevler sırasında topladığın bilgileri saklamak ve rapor hazırlamak için tableti kullanacaksın. Tablet'i açmak için Tablet düğmesine tıkla";
            sceneMessageController.ShowMessage(HelperController.MessageType.Task, 2.1f, HelperController.ButtonType.ClosePanel, false);
            yield return new WaitUntil(() => HudTablet.activeInHierarchy);
            sceneMessageController.ShowMessage(HelperController.MessageType.Task, 2.2f, HelperController.ButtonType.HudToolBox, false);
            yield return new WaitWhile(() => HudTablet.activeInHierarchy);
            TaskCompleted();

        }

        speechBubble.SetActive(false);
        agentAnim.SetBool(animParamStartId, false);
    }
    #endregion
    #region SectionOps
    public void NextSection()
    {
        StartCoroutine(SectionInteraction());
    }

    private IEnumerator SectionInteraction()
    {
        speechBubble.SetActive(true);
        agentAnim.SetBool(animParamStartId, true);
        speechBubbleText.text = "Oryantasyon görevlerini başarıyla tamamladın. Artık uçuştan önce tamamlamamız gereken görevleri yapmak için hazırsın. Haydi işe koyulalım.";
       // yield return new WaitForSeconds(HelperController.SpeechbublpeWaitTime(speechBubbleText.text, textChangeSpeed));
        yield return new WaitWhile(() => speechBubble.activeSelf);

        //yield return new WaitForSeconds(speechBubbleText.text.Length * Time.deltaTime * textChangeSpeed);
        speechBubble.SetActive(false);
        agentAnim.SetBool(animParamStartId, false);
        //yield return new WaitForEndOfFrame();
        sceneMessageController.ShowMessage(HelperController.MessageType.Task, taskList.Count, HelperController.ButtonType.NextSection, true);
    }
    #endregion

    public void SetToolBoxIsClicked(bool param)
    {
        toolBoxIsClicked = param;
    }
    public void SetTabletIsClicked(bool param)
    {
        tabletIsClicked = param;
    }

    public List<TaskController.TaskStruct> taskList = new List<TaskController.TaskStruct>();

    public void TasksGenerate()
    {
        taskList.Add(new TaskController.TaskStruct("\"?\"' Yardım'ı aç ve kapat"));
        taskList.Add(new TaskController.TaskStruct("Alet Çantası'nı aç ve kapat.", false));
        taskList.Add(new TaskController.TaskStruct("Tablet'i aç ve kapat.", false));

        HelperController.TaskController.TaskListFillOnSceneStart(taskList);
    }
}
