﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;

public class Scene2MessageController : MonoBehaviour
{
    public Text HudBottomMessageText;
    public HudMessageStruct taskMessagePanel;
    public HudMessageStruct successMessagePanel;
    public HudMessageStruct FailulerMessagePanel;
    public TabletController tabletController;
    // Use this for initialization
    void Start()
    {

        HudBottomMessageText.text = "";
        tabletController.SwitcOn(TabletScreens.Default);
    }
    public void ShowMessage(HelperController.MessageType mType, float id = 0, HelperController.ButtonType buttonEvent = HelperController.ButtonType.ClosePanel, bool notification = false)
    {

        switch (mType)
        {
            case HelperController.MessageType.Task:
                if (ActivityMessageListTask.Count > id)
                {
                  //  HelperController.GameController.isPlayable = false;
                    if (notification)
                    {
                        ShowNewTaskNotify((int)id);
                    }
                    HudBottomMessageText.text = ActivityMessageListTask[id];
                }

                break;
            case HelperController.MessageType.Warning:
                break;
            case HelperController.MessageType.Success:
             //   HelperController.GameController.isPlayable = false;
                ShowSuccessNotify((int)id);
                HudBottomMessageText.text = "";
                break;
            case HelperController.MessageType.Failure:
                break;
            default:
                break;
        }
        AddEventListenerToButton(mType, buttonEvent);

    }
    private void AddEventListenerToButton(HelperController.MessageType mType, HelperController.ButtonType buttonEvent = HelperController.ButtonType.ClosePanel)
    {
        switch (mType)
        {
            case HelperController.MessageType.Task:
                taskMessagePanel.Button.onClick.RemoveAllListeners();
                switch (buttonEvent)
                {
                    case HelperController.ButtonType.ClosePanel:
                        taskMessagePanel.Button.onClick.RemoveAllListeners();
                        taskMessagePanel.Button.onClick.AddListener(delegate
                        {
                            GameObject.FindGameObjectWithTag(HelperController.Tags.GameController.ToString()).GetComponent<GameController>().isPlayable = true;
                            HelperController.GameController.isPlayable = true;
                            //  Debug.Log("Task Message Pane Button");
                            taskMessagePanel.Button.transform.parent.gameObject.SetActive(false);
                            gameObject.BroadcastMessage("TaskStarted");
                        });
                        break;
                    case HelperController.ButtonType.NextSection:
                        taskMessagePanel.Button.onClick.AddListener(delegate
                        {
                            taskMessagePanel.Button.transform.parent.gameObject.SetActive(false);
                            HelperController.GameController.isPlayable = true;
                            //  Debug.Log("Task Message Pane Button");
                            HelperController.GameController.sceneController.LoadNextScene();
                        });
                       
                        break;
                    default:
                        break;
                }
                break;
            case HelperController.MessageType.Warning:
                break;
            case HelperController.MessageType.Success:
                successMessagePanel.Button.onClick.RemoveAllListeners();
                switch (buttonEvent)
                {
                    case HelperController.ButtonType.ClosePanel:
                        successMessagePanel.Button.onClick.AddListener(delegate
                        {
                            GameObject.FindGameObjectWithTag(HelperController.Tags.GameController.ToString()).GetComponent<GameController>().isPlayable = true;
                            HelperController.GameController.isPlayable = true;
                            successMessagePanel.Button.transform.parent.gameObject.SetActive(false);
                            gameObject.BroadcastMessage("NextTask");

                        });
                        break;
                    case HelperController.ButtonType.NextSection:
                        successMessagePanel.Button.onClick.AddListener(delegate
                        {
                            GameObject.FindGameObjectWithTag(HelperController.Tags.GameController.ToString()).GetComponent<GameController>().isPlayable = true;
                            HelperController.GameController.isPlayable = true;
                            successMessagePanel.Button.transform.parent.gameObject.SetActive(false);
                            HelperController.GameController.sceneController.LoadNextScene();

                        });
                       
                        break;
                    default:
                        break;
                }
                break;
            case HelperController.MessageType.Failure:
                break;
            default:
                break;
        }
    }

    private void ShowNewTaskNotify(int id)
    {
        taskMessagePanel.Button.transform.parent.gameObject.SetActive(true);
        taskMessagePanel.Text.text = ActivityMessageListTask[id];

    }
    private void ShowSuccessNotify(int id)
    {
        successMessagePanel.Button.transform.parent.gameObject.SetActive(true);
        successMessagePanel.Text.text = ActivityMessageListSuccess[id];
    }

    private void ShowFaillureNotify(int id)
    {
        FailulerMessagePanel.Button.transform.parent.gameObject.SetActive(true);
        FailulerMessagePanel.Text.text = ActivityMessageListSuccess[id];
    }
    
    #region Lists
    public static Dictionary<float, string> ActivityMessageListTask = new Dictionary<float, string>()
    {
        {0.0f,"Yeni görev: Sağ üst köşede \"?\" Yardım'ı aç ve kapat" },
        {0.1f,"Devem etmek için \"?\" Yardım'ı aç." },
        {0.2f,"Devam etmek için \"?\" Yardım'ı kapat" },
        {1.0f,"Yeni Görev: Alet Çantası'nı aç ve kapat." },
        {1.1f,"Devam etmek için Alet Çantası'nı Aç." },
        {1.2f,"Devam etmek için Alet Çantası'nı Kapat." },
        {2.0f,"Yeni Görev: Tablet'i aç ve kapat." },
        {2.1f,"Devam Etmek için: Tablet'i aç" },
        {2.2f,"Devam etmek için Tablet'i Kapat." },
        {3.0f,"Yeni Görev : Motor testleri için test sahasına git." }
        
    };
    public static Dictionary<int, string> ActivityMessageListSuccess = new Dictionary<int, string>()
    {
        {0,"Doğru cevap tebrikler!" },
        {1,"Görevi başarı ile tamamladın, devam edebiliriz." },
        {2,"Tüm Görevleri başarı ile tamamladın, devam edebiliriz." },
    };
    public static Dictionary<int, string> ActivityMessageListFailure = new Dictionary<int, string>()
    {
        {0,"Yanlış, daha dikkatli olmalısın!" },
        {1,"" },
        {2,"" },
    };
    public static Dictionary<int, string> ActivityMessageListSpeech = new Dictionary<int, string>()
    {
        {0,"Merhaba" },
        {1,"" },
        {2,"" },
    };
    #endregion
}
