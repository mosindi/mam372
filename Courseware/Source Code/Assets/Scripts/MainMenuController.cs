﻿using System;
using UnityEngine;
using System.Collections;


public class MainMenuController : MonoBehaviour {

    [SerializeField]
    public HelperController.MenuItem[] MenuItems;

    public Animator MenuPanelAnim;

    GameController GC;
    PanelManager pm;
	// Use this for initialization
	void Start () {
        GC = HelperController.GameController;
        pm = HelperController.PanelManager;

        pm.OpenPanel(MenuPanelAnim);

        MenuItems[0].Button.onClick.AddListener(delegate { OnClick(HelperController.ButtonType.Start); });
         MenuItems[1].Button.onClick.AddListener(delegate { OnClick(HelperController.ButtonType.Help); });
        MenuItems[2].Button.onClick.AddListener(delegate { OnClick(HelperController.ButtonType.Quit); });

       


    }
   
    // Update is called once per frame
    void Update () {
        if (Input.GetKeyUp(KeyCode.Tab))
        {
            
        }
	}

    public void OnClick(HelperController.ButtonType _button)
    {
        switch (_button)
        {
            case HelperController.ButtonType.Start:
                GC.sceneController.StartGame();
                break;
            case HelperController.ButtonType.Help:
                GameObject go = GameObject.FindGameObjectWithTag(HelperController.Tags.HelpPanel.ToString());

                Animator anim = go.GetComponentInChildren<Animator>(true);
                pm.OpenPanel(anim);
                break;
            case HelperController.ButtonType.Quit:
                GC.messageController.ShowWarning(909, "QuitGame");
                break;
            default:
                break;
        }
    }
}
