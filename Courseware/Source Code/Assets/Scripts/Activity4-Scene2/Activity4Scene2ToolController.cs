﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;

public class Activity4Scene2ToolController : MonoBehaviour {
    public GameObject[] Slot1Tools;
    public GameObject[] Slot2Tools;
    public GameObject[] Slot3Tools;

    public Activity4Scene2ToolItemController[] Slot1ToolItems;
    public Activity4Scene2ToolItemController[] Slot2ToolItems;
    public Activity4Scene2ToolItemController[] Slot3ToolItems;
    
    public int insertedSlotNumber = 0;
    public int MaxWorkSlotNumber = 3;
  

    public float MeasuredSpeed = 0;
    public float MeasuredDistance = 0;
    public float MeasuredTime = 0;

    [HideInInspector]
    public List<Activity4Scene2ToolItemController> ActiveItems = new List<Activity4Scene2ToolItemController>();
    private bool slot1Filled;
    private bool slot2Filled;
    private bool slot3Filled;

    // Use this for initialization
    void Start () {
        

    }
	
	// Update is called once per frame
	void Update () {
	
	}

    public bool IsAllToolsSetted()
    {
        bool response = true;
        foreach (Activity4Scene2ToolItemController item in ActiveItems)
        {
            if (!item.isSwitchOn)
            {
                response = false;
                break;
            }
            
        }
        return response;

    }
    public void UpdateActivesToolMeasurement()
    {
        foreach (Activity4Scene2ToolItemController item in ActiveItems)
        {
            if (item.Type == MeasurementToolType.Speed)
            {
                item.SetResultText(MeasuredSpeed);
            }
            if (item.Type ==MeasurementToolType.Distance)
            {
                item.SetResultText(MeasuredDistance);
            }
            if (item.Type == MeasurementToolType.Time)
            {
                item.SetResultText(MeasuredTime);
            }
        }
    }
    public void UpdateTimeTool()
    {
        foreach (Activity4Scene2ToolItemController item in ActiveItems)
        {
            if (item.Type == MeasurementToolType.Time)
            {
                item.SetResultText(MeasuredTime);
            }
        }
    }
    public void UpdateDistanceTool()
    {
        foreach (Activity4Scene2ToolItemController item in ActiveItems)
        {
            if (item.Type == MeasurementToolType.Distance)
            {
                item.SetResultText(MeasuredDistance);
            }
        }
    }
    public void UpdateSpeedTool()
    {
        foreach (Activity4Scene2ToolItemController item in ActiveItems)
        {
            if (item.Type ==MeasurementToolType.Speed)
            {
                item.SetResultText(MeasuredSpeed);
            }
        }
    }


    public void SetSlotNumber(int value)
    {
        MaxWorkSlotNumber = value;
    }
    public void ToolInsert(int DeviceID)
    {

        //bool inserted = false;
        if (insertedSlotNumber < MaxWorkSlotNumber && HelperController.GameController.isPlayable)
        {
            if (!slot1Filled)
            {
                Slot1ToolItems[DeviceID].gameObject.SetActive(true);
                Slot1ToolItems[DeviceID].SetResultText("---");
                Slot1ToolItems[DeviceID].SetUnitText("---");
                Slot1ToolItems[DeviceID].isSwitchOn = false;
                slot1Filled = true;
                insertedSlotNumber++;
                ActiveItems.Add(Slot1ToolItems[DeviceID]);

                return;
            }
            if (!slot2Filled)
            {
                Slot2ToolItems[DeviceID].gameObject.SetActive(true);
                Slot2ToolItems[DeviceID].SetResultText("---");
                Slot2ToolItems[DeviceID].SetUnitText("---");
                Slot2ToolItems[DeviceID].isSwitchOn = false;
                insertedSlotNumber++;
                slot2Filled = true;
                ActiveItems.Add(Slot2ToolItems[DeviceID]);

                return;
            }
            if (!slot3Filled)
            {
                Slot3ToolItems[DeviceID].gameObject.SetActive(true);
                Slot3ToolItems[DeviceID].SetResultText("---");
                Slot3ToolItems[DeviceID].SetUnitText("---");
                Slot3ToolItems[DeviceID].isSwitchOn = false;
                insertedSlotNumber++;
                slot3Filled = true;
                ActiveItems.Add(Slot3ToolItems[DeviceID]);

                return;
            }

        }
        else
        {
            Debug.Log("Warning Working Slot is full");
        }



    }
    public void ToolEject(int slotID)
    {

        if (insertedSlotNumber > 0 && HelperController.GameController.isPlayable)
        {
            if (slotID == 1)
            {
                Slot1Tools[0].SetActive(false);
                Slot1Tools[1].SetActive(false);
                Slot1Tools[2].SetActive(false);
                Slot1ToolItems[0].isSwitchOn = false;
                Slot1ToolItems[1].isSwitchOn = false;
                Slot1ToolItems[2].isSwitchOn = false;

                slot1Filled = false;
               
                ActiveItems.Remove(Slot1Tools[0].GetComponent<Activity4Scene2ToolItemController>());
                ActiveItems.Remove(Slot1Tools[1].GetComponent<Activity4Scene2ToolItemController>());
                ActiveItems.Remove(Slot1Tools[2].GetComponent<Activity4Scene2ToolItemController>());


            }
            if (slotID == 2)
            {
                Slot2Tools[0].SetActive(false);
                Slot2Tools[1].SetActive(false);
                Slot2Tools[2].SetActive(false);
                Slot2ToolItems[0].isSwitchOn = false;
                Slot2ToolItems[1].isSwitchOn = false;
                Slot2ToolItems[2].isSwitchOn = false;
                slot2Filled = false;
                ActiveItems.Remove(Slot2Tools[0].GetComponent<Activity4Scene2ToolItemController>());
                ActiveItems.Remove(Slot2Tools[1].GetComponent<Activity4Scene2ToolItemController>());
                ActiveItems.Remove(Slot2Tools[2].GetComponent<Activity4Scene2ToolItemController>());
            }
            if (slotID == 3)
            {
                Slot3Tools[0].SetActive(false);
                Slot3Tools[1].SetActive(false);
                Slot3Tools[2].SetActive(false);
                Slot3ToolItems[0].isSwitchOn = false;
                Slot3ToolItems[1].isSwitchOn = false;
                Slot3ToolItems[2].isSwitchOn = false;
                slot3Filled = false;
                ActiveItems.Remove(Slot3Tools[0].GetComponent<Activity4Scene2ToolItemController>());
                ActiveItems.Remove(Slot3Tools[1].GetComponent<Activity4Scene2ToolItemController>());
                ActiveItems.Remove(Slot3Tools[2].GetComponent<Activity4Scene2ToolItemController>());
            }
            insertedSlotNumber--;
        }



    }

  
}

