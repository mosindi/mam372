﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System;
using System.Collections.Generic;

public class Activity4Scene2Controller : MonoBehaviour
{

    public TabletController tabletController;
    public Activity4Scene2ToolController toolController;
    public Activity4Scene2SettingPanelController settingPanelController;
    public GameObject ToolContainer;
    public GameObject speechBubble;
    public Text speechBubbleText;
    public Animator agentAnim;
    [Range(0, 10)]
    public float textChangeSpeed;
    [Range(0, 10)]
    public float lunchCountDown;

    public Button IntroPassBtn;
    public Animator camAnim;
    public Button CheckandApproveBtn;
    public GameObject Screen2Obj;
    public Text Screen2Txt;
    public Text Screen2Lbl;
    [HideInInspector]
    public bool isSettingDone = false;

    Activity4Scene2MessageController sceneMessageController;

    private int agentAnimParamStartId;
    private int camAnimParamBuildId;
    private int activeTaskID = -1;
    private int activeCheckPoint = 0;
    private string agentAnimParamStart = "Interaction";
    private string camAnimParamBuild = "CurrentState";

    private Coroutine IntroInteractionCoroutine;
    private Coroutine SettingCoroutine;
    private Coroutine ReadSpeedDataCrntn;
    private Coroutine ReadDistanceDataCrntn;
    private Coroutine ReadTimeDataCrntn;


    private bool checkpointTime;
    private bool checkpointDistance;
    private bool checkpointSpeed;
    private bool checkTabletValues;
    // Use this for initialization
    void Start()
    {
        TaskGenerate();
        toolController.ToolInsert(0);
        toolController.ToolInsert(1);
        toolController.ToolInsert(2);
        sceneMessageController = GetComponent<Activity4Scene2MessageController>();


        HelperController.GameController.isPlayable = false;
        camAnimParamBuildId = Animator.StringToHash(camAnimParamBuild);
        agentAnimParamStartId = Animator.StringToHash(agentAnimParamStart);
        speechBubble.SetActive(false);
        IntroPassBtn.gameObject.SetActive(false);

        activeTaskID = 0;
        sceneMessageController.ShowMessage(HelperController.MessageType.Task, 0.0f, HelperController.ButtonType.ClosePanel, false);
    }



    #region Intro
    public void CamAnimate(int stateID)
    {
        camAnim.SetInteger(camAnimParamBuildId, stateID);
        HelperController.TaskController.TaskComplete(activeTaskID);

        IntroInteractionCoroutine = StartCoroutine(IntroSpeakInteractionCoroutine());
    }


    private IEnumerator IntroSpeakInteractionCoroutine()
    {
        IntroPassBtn.gameObject.SetActive(true);
        agentAnim.gameObject.SetActive(true);
        yield return new WaitForSeconds(1.2f);
        speechBubble.SetActive(true);
        agentAnim.SetBool(agentAnimParamStartId, true);
        HelperController.GameController.isPlayable = false;
        speechBubbleText.text = "Uçuş için artık çok az bir zaman kaldı. Uçuş esnasında motorlardan gelen verileri toplaman ve bunları kayıt altına alman gerekiyor.";
        yield return new WaitWhile(() => speechBubble.activeSelf); //yield return new WaitForSeconds(HelperController.SpeechbublpeWaitTime(speechBubbleText.text, textChangeSpeed));

        agentAnim.SetBool(agentAnimParamStartId, false);
        speechBubble.SetActive(false);
        IntroPassBtn.gameObject.SetActive(false);
        HelperController.GameController.isPlayable = true;
        StartCoroutine(SpeakInteractionCoroutine(1));
    }
    public void OnClickPassIntro()
    {

        IntroPassBtn.gameObject.SetActive(false);
        StopCoroutine(IntroInteractionCoroutine);
        StartCoroutine(SpeakInteractionCoroutine(1));
    }

    #endregion
    private IEnumerator SpeakInteractionCoroutine(int interactionID)
    {
        InteractionObjectsActive(true);
        if (interactionID == 1)
        {
            speechBubbleText.text = "Her motor için hız, zaman ve yol değerleniri okuyup tablet üzerinden sisteme girmen gerekiyor.";
            yield return new WaitWhile(() => speechBubble.activeSelf); //yield return new WaitForSeconds(HelperController.SpeechbublpeWaitTime(speechBubbleText.text, textChangeSpeed));

            tabletController.SwitcOn(TabletScreens.Activity4Scene2);
            NextTask();
        }
        InteractionObjectsActive(false);

    }

    private void InteractionObjectsActive(bool status)
    {
        agentAnim.gameObject.SetActive(status);
        speechBubble.SetActive(status);
        agentAnim.SetBool(agentAnimParamStartId, status);
        HelperController.GameController.isPlayable = !status;
    }
    #region TaskOps
    public void NextTask()
    {
        StopAllCoroutines();
        if (activeTaskID + 1 < taskList.Count)
        {
            activeTaskID++;
            //  StartCoroutine(NextTaskInteraction());
            sceneMessageController.ShowMessage(HelperController.MessageType.Task, activeTaskID, HelperController.ButtonType.ClosePanel, true);
        }
        else
        {
            NextSection();
        }
    }

    public void TaskStarted()
    {

        HelperController.GameController.Playable(true);
        StartCoroutine(TaskProcess());
    }
    private IEnumerator TaskProcess()
    {
        CheckandApproveBtn.interactable = false;
        float _sTime;
        float _fTime;
        switch (activeTaskID)
        {
            case 1:
                tabletController.activity4Scene1Controller.FırlatmaAnswers[0].SetAnswers(5000, 5000, 1);
                tabletController.activity4Scene1Controller.FırlatmaAnswers[1].SetAnswers(5000, 10000, 2);
                tabletController.activity4Scene1Controller.FırlatmaAnswers[2].SetAnswers(5000, 15000, 3);
                tabletController.activity4Scene1Controller.FırlatmaAnswers[3].SetAnswers(5000, 20000, 4);
                tabletController.activity4Scene1Controller.FırlatmaAnswers[4].SetAnswers(6000, 30000, 5);


                #region setting
                sceneMessageController.ShowMessage(HelperController.MessageType.Task, 1.1f, HelperController.ButtonType.ClosePanel, false);
                isSettingDone = false;
                settingPanelController.measurementToolType = MeasurementToolType.None;
                SettingCoroutine = StartCoroutine(SettingTools());
                while (!isSettingDone)
                {
                    yield return new WaitForEndOfFrame();
                }
                StopCoroutine(SettingCoroutine);
                CheckandApproveBtn.interactable = false;

                sceneMessageController.ShowMessage(HelperController.MessageType.Success, 3f, HelperController.ButtonType.Continue, true);

                yield return new WaitWhile(() => sceneMessageController.successMessagePanel.Button.transform.parent.gameObject.activeSelf);

                sceneMessageController.ShowMessage(HelperController.MessageType.Task, 1.3f, HelperController.ButtonType.Continue, true);

                yield return new WaitWhile(() => sceneMessageController.taskMessagePanel.Button.transform.parent.gameObject.activeSelf);

                #endregion
                #region Lunch
                InteractionObjectsActive(true);
                speechBubbleText.text = "Fırlatma için geri sayım başlıyor. Fırlatma başladıktan sonra ölçüm cihazlarını izle. Birinci kontrol noktasında tekrar bağlantı kuracağız.";
                yield return new WaitWhile(() => speechBubble.activeSelf); //yield return new WaitForSeconds(HelperController.SpeechbublpeWaitTime(speechBubbleText.text, textChangeSpeed));

                InteractionObjectsActive(false);


                Screen2Obj.SetActive(true);
                _sTime = 0;
                _fTime = _sTime + lunchCountDown;
                while (_fTime - _sTime > 0)
                {
                    Screen2Txt.text = (_fTime - _sTime).ToString();
                    yield return new WaitForSeconds(.5f);
                    Screen2Txt.text = "";
                    yield return new WaitForSeconds(.5f);
                    _sTime = _sTime + 1;

                }
                Screen2Lbl.text = "";
                Screen2Txt.text = "Kalkış";

                yield return new WaitForSeconds(.5f);
                #endregion
                #region CheckPoint 1
                activeCheckPoint = 0;
                checkTabletValues = false;
                sceneMessageController.ShowMessage(HelperController.MessageType.Task, 1.31f, HelperController.ButtonType.Continue, true);
                yield return new WaitWhile(() => sceneMessageController.taskMessagePanel.Button.transform.parent.gameObject.activeSelf);

                Screen2Obj.SetActive(false);
                CheckPointFlagReset();

                ReadSpeedDataCrntn = StartCoroutine(ReadDataSpeed(0, 5000, 2, 0.2f));
                ReadDistanceDataCrntn = StartCoroutine(ReadDataDistance(0, 5000, 5, 0.05f));
                ReadTimeDataCrntn = StartCoroutine(ReadDataTime(0, 1, 5, 0.1f));

                yield return new WaitForSeconds(5.7f);

                StopCoroutine(ReadSpeedDataCrntn);
                StopCoroutine(ReadDistanceDataCrntn);
                StopCoroutine(ReadTimeDataCrntn);

                Screen2Lbl.text = "Kontrol Noktası:";
                Screen2Txt.text = "1";
                Screen2Obj.SetActive(true);

                tabletController.activity4Scene1Controller.SwitchOnTaskPanel(1);
                tabletController.activity4Scene1Controller.SwitchOnOffAnswerContainer(1, 0, true);
                InteractionObjectsActive(true);
                speechBubbleText.text = "Birinci kontrol noktasına ulaştınız. Ölçüm cihazından okuduğun değerleri tablete gir.";
                yield return new WaitWhile(() => speechBubble.activeSelf); //yield return new WaitForSeconds(HelperController.SpeechbublpeWaitTime(speechBubbleText.text, textChangeSpeed));

                InteractionObjectsActive(false);
                yield return new WaitUntil(() => checkTabletValues);
                yield return new WaitWhile(() => sceneMessageController.successMessagePanel.Button.transform.parent.gameObject.activeSelf);
                #endregion
                #region CheckPoint 2
                tabletController.SetTabletActive(false);
                activeCheckPoint = 1;
                checkTabletValues = false;
                sceneMessageController.ShowMessage(HelperController.MessageType.Task, 1.32f, HelperController.ButtonType.Continue, true);
                yield return new WaitWhile(() => sceneMessageController.taskMessagePanel.Button.transform.parent.gameObject.activeSelf);

                Screen2Obj.SetActive(false);
                CheckPointFlagReset();

                ReadSpeedDataCrntn = StartCoroutine(ReadDataSpeed(5000, 5000, 2, 0.2f));
                ReadDistanceDataCrntn = StartCoroutine(ReadDataDistance(5000, 10000, 5, 0.05f));
                ReadTimeDataCrntn = StartCoroutine(ReadDataTime(1, 2, 5, 0.1f));
                yield return new WaitForSeconds(5.7f);

                StopCoroutine(ReadSpeedDataCrntn);
                StopCoroutine(ReadDistanceDataCrntn);
                StopCoroutine(ReadTimeDataCrntn);

                Screen2Lbl.text = "Kontrol Noktası:";
                Screen2Txt.text = "2";
                Screen2Obj.SetActive(true);

                tabletController.activity4Scene1Controller.SwitchOnTaskPanel(1);
                tabletController.activity4Scene1Controller.SwitchOnOffAnswerContainer(1, 1, true);
                InteractionObjectsActive(true);
                speechBubbleText.text = "İkinci kontrol noktasına ulaştınız. Ölçüm cihazından okuduğun değerleri tablete gir.";
                yield return new WaitWhile(() => speechBubble.activeSelf); //yield return new WaitForSeconds(HelperController.SpeechbublpeWaitTime(speechBubbleText.text, textChangeSpeed));

                InteractionObjectsActive(false);
                yield return new WaitUntil(() => checkTabletValues);
                yield return new WaitWhile(() => sceneMessageController.successMessagePanel.Button.transform.parent.gameObject.activeSelf);
                #endregion
                #region CheckPoint 3
                tabletController.SetTabletActive(false);
                activeCheckPoint = 2;
                checkTabletValues = false;
                sceneMessageController.ShowMessage(HelperController.MessageType.Task, 1.33f, HelperController.ButtonType.Continue, true);
                yield return new WaitWhile(() => sceneMessageController.taskMessagePanel.Button.transform.parent.gameObject.activeSelf);

                Screen2Obj.SetActive(false);
                CheckPointFlagReset();

                ReadSpeedDataCrntn = StartCoroutine(ReadDataSpeed(5000, 5000, 2, 0.2f));
                ReadDistanceDataCrntn = StartCoroutine(ReadDataDistance(10000, 15000, 5, 0.05f));
                ReadTimeDataCrntn = StartCoroutine(ReadDataTime(2, 3, 5, 0.1f));
                yield return new WaitForSeconds(5.5f);

                StopCoroutine(ReadSpeedDataCrntn);
                StopCoroutine(ReadDistanceDataCrntn);
                StopCoroutine(ReadTimeDataCrntn);
                toolController.ActiveItems[2].SetResultText(3);

                Screen2Lbl.text = "Kontrol Noktası:";
                Screen2Txt.text = "3";
                Screen2Obj.SetActive(true);

                tabletController.activity4Scene1Controller.SwitchOnTaskPanel(1);
                tabletController.activity4Scene1Controller.SwitchOnOffAnswerContainer(1, 2, true);
                InteractionObjectsActive(true);
                speechBubbleText.text = "Üçüncü kontrol noktasına ulaştınız. Ölçüm cihazından okuduğun değerleri tablete gir.";
                yield return new WaitWhile(() => speechBubble.activeSelf); //yield return new WaitForSeconds(HelperController.SpeechbublpeWaitTime(speechBubbleText.text, textChangeSpeed));

                InteractionObjectsActive(false);
                yield return new WaitUntil(() => checkTabletValues);
                yield return new WaitWhile(() => sceneMessageController.successMessagePanel.Button.transform.parent.gameObject.activeSelf);
                #endregion
                #region CheckPoint 4
                tabletController.SetTabletActive(false);
                activeCheckPoint = 3;
                checkTabletValues = false;
                sceneMessageController.ShowMessage(HelperController.MessageType.Task, 1.34f, HelperController.ButtonType.Continue, true);
                yield return new WaitWhile(() => sceneMessageController.taskMessagePanel.Button.transform.parent.gameObject.activeSelf);

                Screen2Obj.SetActive(false);
                CheckPointFlagReset();

                ReadSpeedDataCrntn = StartCoroutine(ReadDataSpeed(5000, 5000, 2, 0.2f));
                ReadDistanceDataCrntn = StartCoroutine(ReadDataDistance(15000, 20000, 5, 0.05f));
                ReadTimeDataCrntn = StartCoroutine(ReadDataTime(3, 4, 5, 0.1f));
                yield return new WaitForSeconds(5.5f);

                StopCoroutine(ReadSpeedDataCrntn);
                StopCoroutine(ReadDistanceDataCrntn);
                StopCoroutine(ReadTimeDataCrntn);
                toolController.ActiveItems[2].SetResultText(4);

                Screen2Lbl.text = "Kontrol Noktası:";
                Screen2Txt.text = "4";
                Screen2Obj.SetActive(true);

                tabletController.activity4Scene1Controller.SwitchOnTaskPanel(1);
                tabletController.activity4Scene1Controller.SwitchOnOffAnswerContainer(1, 3, true);
                InteractionObjectsActive(true);
                speechBubbleText.text = "Dördüncü kontrol noktasına ulaştınız. Ölçüm cihazından okuduğun değerleri tablete gir.";
                yield return new WaitWhile(() => speechBubble.activeSelf); //yield return new WaitForSeconds(HelperController.SpeechbublpeWaitTime(speechBubbleText.text, textChangeSpeed));

                InteractionObjectsActive(false);
                yield return new WaitUntil(() => checkTabletValues);
                yield return new WaitWhile(() => sceneMessageController.successMessagePanel.Button.transform.parent.gameObject.activeSelf);
                #endregion
                #region CheckPoint 5
                tabletController.SetTabletActive(false);
                activeCheckPoint = 4;
                checkTabletValues = false;
                sceneMessageController.ShowMessage(HelperController.MessageType.Task, 1.35f, HelperController.ButtonType.Continue, true);
                yield return new WaitWhile(() => sceneMessageController.taskMessagePanel.Button.transform.parent.gameObject.activeSelf);

                Screen2Obj.SetActive(false);
                CheckPointFlagReset();

                ReadSpeedDataCrntn = StartCoroutine(ReadDataSpeed(5000, 6000, 4, 0.2f));
                ReadDistanceDataCrntn = StartCoroutine(ReadDataDistance(20000, 30000, 5, 0.05f));
                ReadTimeDataCrntn = StartCoroutine(ReadDataTime(4, 5, 5, 0.1f));
                yield return new WaitForSeconds(5.5f);

                StopCoroutine(ReadSpeedDataCrntn);
                StopCoroutine(ReadDistanceDataCrntn);
                StopCoroutine(ReadTimeDataCrntn);
                toolController.ActiveItems[2].SetResultText(5);

                Screen2Lbl.text = "Kontrol Noktası:";
                Screen2Txt.text = "5";
                Screen2Obj.SetActive(true);

                tabletController.activity4Scene1Controller.SwitchOnTaskPanel(1);
                tabletController.activity4Scene1Controller.SwitchOnOffAnswerContainer(1, 4, true);
                InteractionObjectsActive(true);
                speechBubbleText.text = "Beşinci kontrol noktasına ulaştınız. Ölçüm cihazından okuduğun değerleri tablete gir.";
                yield return new WaitWhile(() => speechBubble.activeSelf); //yield return new WaitForSeconds(HelperController.SpeechbublpeWaitTime(speechBubbleText.text, textChangeSpeed));

                InteractionObjectsActive(false);
                yield return new WaitUntil(() => checkTabletValues);
                yield return new WaitWhile(() => sceneMessageController.successMessagePanel.Button.transform.parent.gameObject.activeSelf);
                #endregion
                TaskComleted();
                break;
            case 2:
                #region initialize
                tabletController.activity4Scene1Controller.MekikAnswers[0].SetAnswers(500, 500, 1);
                tabletController.activity4Scene1Controller.MekikAnswers[1].SetAnswers(500, 2500, 5);
                tabletController.activity4Scene1Controller.MekikAnswers[2].SetAnswers(500, 5000, 10);
                tabletController.activity4Scene1Controller.MekikAnswers[3].SetAnswers(500, 10000, 20);
                tabletController.activity4Scene1Controller.MekikAnswers[4].SetAnswers(500, 15000, 30);
                toolController.ActiveItems[0].isSwitchOn = false;
                toolController.ActiveItems[0].SetUnitText("---");
                toolController.ActiveItems[0].SetResultText("---");
                toolController.ActiveItems[1].isSwitchOn = false;
                toolController.ActiveItems[1].SetUnitText("---");
                toolController.ActiveItems[1].SetResultText("---");

                toolController.ActiveItems[2].isSwitchOn = false;
                toolController.ActiveItems[2].SetUnitText("---");
                toolController.ActiveItems[2].SetResultText("---");
                Screen2Obj.SetActive(false);
                #endregion

                #region setting
                sceneMessageController.ShowMessage(HelperController.MessageType.Task, 2.1f, HelperController.ButtonType.ClosePanel, false);
                isSettingDone = false;
                settingPanelController.measurementToolType = MeasurementToolType.None;
                SettingCoroutine = StartCoroutine(SettingTools());
                while (!isSettingDone)
                {
                    yield return new WaitForEndOfFrame();
                }
                StopCoroutine(SettingCoroutine);
                CheckandApproveBtn.interactable = false;

                sceneMessageController.ShowMessage(HelperController.MessageType.Success, 3f, HelperController.ButtonType.Continue, true);

                yield return new WaitWhile(() => sceneMessageController.successMessagePanel.Button.transform.parent.gameObject.activeSelf);

                sceneMessageController.ShowMessage(HelperController.MessageType.Task, 1.3f, HelperController.ButtonType.Continue, true);

                yield return new WaitWhile(() => sceneMessageController.taskMessagePanel.Button.transform.parent.gameObject.activeSelf);

                #endregion

                #region Lunch
                InteractionObjectsActive(true);
                speechBubbleText.text = "Fırlatma modülü ayrılıp Mekik motorları çalıştırılıyor.";
                yield return new WaitWhile(() => speechBubble.activeSelf); //yield return new WaitForSeconds(HelperController.SpeechbublpeWaitTime(speechBubbleText.text, textChangeSpeed));

                InteractionObjectsActive(false);

                Screen2Lbl.text = "Çalışmaya";

                Screen2Obj.SetActive(true);
                _sTime = 0;
                _fTime = _sTime + lunchCountDown;
                while (_fTime - _sTime > 0)
                {
                    Screen2Txt.text = (_fTime - _sTime).ToString();
                    yield return new WaitForSeconds(.5f);
                    Screen2Txt.text = "";
                    yield return new WaitForSeconds(.5f);
                    _sTime = _sTime + 1;

                }
                Screen2Lbl.text = "";
                Screen2Txt.text = "Çalıştı.";

                yield return new WaitForSeconds(.5f);
                #endregion
                #region CheckPoint 1
                activeCheckPoint = 0;
                checkTabletValues = false;
                sceneMessageController.ShowMessage(HelperController.MessageType.Task, 1.31f, HelperController.ButtonType.Continue, true);
                yield return new WaitWhile(() => sceneMessageController.taskMessagePanel.Button.transform.parent.gameObject.activeSelf);

                Screen2Obj.SetActive(false);
                CheckPointFlagReset();

                ReadSpeedDataCrntn = StartCoroutine(ReadDataSpeed(0, 500, 2, 0.2f));
                ReadDistanceDataCrntn = StartCoroutine(ReadDataDistance(0, 500, 5, 0.05f));
                ReadTimeDataCrntn = StartCoroutine(ReadDataTime(0, 1, 5, 0.1f));

                yield return new WaitForSeconds(5.7f);

                StopCoroutine(ReadSpeedDataCrntn);
                StopCoroutine(ReadDistanceDataCrntn);
                StopCoroutine(ReadTimeDataCrntn);

                Screen2Lbl.text = "Kontrol Noktası:";
                Screen2Txt.text = "1";
                Screen2Obj.SetActive(true);

                tabletController.activity4Scene1Controller.SwitchOnTaskPanel(2);
                tabletController.activity4Scene1Controller.SwitchOnOffAnswerContainer(2, 0, true);
                InteractionObjectsActive(true);
                speechBubbleText.text = "Birinci kontrol noktasına ulaştınız. Ölçüm cihazından okuduğun değerleri tablete gir.";
                yield return new WaitWhile(() => speechBubble.activeSelf); //yield return new WaitForSeconds(HelperController.SpeechbublpeWaitTime(speechBubbleText.text, textChangeSpeed));

                InteractionObjectsActive(false);
                yield return new WaitUntil(() => checkTabletValues);
                yield return new WaitWhile(() => sceneMessageController.successMessagePanel.Button.transform.parent.gameObject.activeSelf);
                #endregion
                #region CheckPoint 2
                tabletController.SetTabletActive(false);
                activeCheckPoint = 1;
                checkTabletValues = false;
                sceneMessageController.ShowMessage(HelperController.MessageType.Task, 1.32f, HelperController.ButtonType.Continue, true);
                yield return new WaitWhile(() => sceneMessageController.taskMessagePanel.Button.transform.parent.gameObject.activeSelf);

                Screen2Obj.SetActive(false);
                CheckPointFlagReset();

                ReadSpeedDataCrntn = StartCoroutine(ReadDataSpeed(500, 500, 2, 0.2f));
                ReadDistanceDataCrntn = StartCoroutine(ReadDataDistance(500, 2500, 5, 0.05f));
                ReadTimeDataCrntn = StartCoroutine(ReadDataTime(1, 5, 5, 0.1f));
                yield return new WaitForSeconds(5.7f);

                StopCoroutine(ReadSpeedDataCrntn);
                StopCoroutine(ReadDistanceDataCrntn);
                StopCoroutine(ReadTimeDataCrntn);

                Screen2Lbl.text = "Kontrol Noktası:";
                Screen2Txt.text = "2";
                Screen2Obj.SetActive(true);

                tabletController.activity4Scene1Controller.SwitchOnTaskPanel(2);
                tabletController.activity4Scene1Controller.SwitchOnOffAnswerContainer(2, 1, true);
                InteractionObjectsActive(true);
                speechBubbleText.text = "İkinci kontrol noktasına ulaştınız. Ölçüm cihazından okuduğun değerleri tablete gir.";
                yield return new WaitWhile(() => speechBubble.activeSelf); //yield return new WaitForSeconds(HelperController.SpeechbublpeWaitTime(speechBubbleText.text, textChangeSpeed));
                InteractionObjectsActive(false);
                yield return new WaitUntil(() => checkTabletValues);
                yield return new WaitWhile(() => sceneMessageController.successMessagePanel.Button.transform.parent.gameObject.activeSelf);
                #endregion
                #region CheckPoint 3
                tabletController.SetTabletActive(false);
                activeCheckPoint = 2;
                checkTabletValues = false;
                sceneMessageController.ShowMessage(HelperController.MessageType.Task, 1.33f, HelperController.ButtonType.Continue, true);
                yield return new WaitWhile(() => sceneMessageController.taskMessagePanel.Button.transform.parent.gameObject.activeSelf);

                Screen2Obj.SetActive(false);
                CheckPointFlagReset();

                ReadSpeedDataCrntn = StartCoroutine(ReadDataSpeed(500, 500, 2, 0.2f));
                ReadDistanceDataCrntn = StartCoroutine(ReadDataDistance(2500, 5000, 5, 0.05f));
                ReadTimeDataCrntn = StartCoroutine(ReadDataTime(5, 10, 5, 0.1f));
                yield return new WaitForSeconds(5.7f);

                StopCoroutine(ReadSpeedDataCrntn);
                StopCoroutine(ReadDistanceDataCrntn);
                StopCoroutine(ReadTimeDataCrntn);

                Screen2Lbl.text = "Kontrol Noktası:";
                Screen2Txt.text = "3";
                Screen2Obj.SetActive(true);

                tabletController.activity4Scene1Controller.SwitchOnTaskPanel(2);
                tabletController.activity4Scene1Controller.SwitchOnOffAnswerContainer(2, 2, true);
                InteractionObjectsActive(true);
                speechBubbleText.text = "Üçüncü kontrol noktasına ulaştınız. Ölçüm cihazından okuduğun değerleri tablete gir.";
                yield return new WaitWhile(() => speechBubble.activeSelf); //yield return new WaitForSeconds(HelperController.SpeechbublpeWaitTime(speechBubbleText.text, textChangeSpeed));
                InteractionObjectsActive(false);
                yield return new WaitUntil(() => checkTabletValues);
                yield return new WaitWhile(() => sceneMessageController.successMessagePanel.Button.transform.parent.gameObject.activeSelf);
                #endregion
                #region CheckPoint 4
                tabletController.SetTabletActive(false);
                activeCheckPoint = 3;
                checkTabletValues = false;
                sceneMessageController.ShowMessage(HelperController.MessageType.Task, 1.34f, HelperController.ButtonType.Continue, true);
                yield return new WaitWhile(() => sceneMessageController.taskMessagePanel.Button.transform.parent.gameObject.activeSelf);

                Screen2Obj.SetActive(false);
                CheckPointFlagReset();

                ReadSpeedDataCrntn = StartCoroutine(ReadDataSpeed(500, 500, 2, 0.2f));
                ReadDistanceDataCrntn = StartCoroutine(ReadDataDistance(5000, 10000, 5, 0.05f));
                ReadTimeDataCrntn = StartCoroutine(ReadDataTime(10, 20, 5, 0.1f));
                yield return new WaitForSeconds(5.7f);

                StopCoroutine(ReadSpeedDataCrntn);
                StopCoroutine(ReadDistanceDataCrntn);
                StopCoroutine(ReadTimeDataCrntn);

                Screen2Lbl.text = "Kontrol Noktası:";
                Screen2Txt.text = "4";
                Screen2Obj.SetActive(true);

                tabletController.activity4Scene1Controller.SwitchOnTaskPanel(2);
                tabletController.activity4Scene1Controller.SwitchOnOffAnswerContainer(2, 3, true);
                InteractionObjectsActive(true);
                speechBubbleText.text = "Dördüncü kontrol noktasına ulaştınız. Ölçüm cihazından okuduğun değerleri tablete gir.";
                yield return new WaitWhile(() => speechBubble.activeSelf); //yield return new WaitForSeconds(HelperController.SpeechbublpeWaitTime(speechBubbleText.text, textChangeSpeed));
                InteractionObjectsActive(false);
                yield return new WaitUntil(() => checkTabletValues);
                yield return new WaitWhile(() => sceneMessageController.successMessagePanel.Button.transform.parent.gameObject.activeSelf);
                #endregion
                #region CheckPoint 5
                tabletController.SetTabletActive(false);
                activeCheckPoint = 4;
                checkTabletValues = false;
                sceneMessageController.ShowMessage(HelperController.MessageType.Task, 1.35f, HelperController.ButtonType.Continue, true);
                yield return new WaitWhile(() => sceneMessageController.taskMessagePanel.Button.transform.parent.gameObject.activeSelf);

                Screen2Obj.SetActive(false);
                CheckPointFlagReset();

                ReadSpeedDataCrntn = StartCoroutine(ReadDataSpeed(500, 500, 4, 0.2f));
                ReadDistanceDataCrntn = StartCoroutine(ReadDataDistance(10000, 15000, 5, 0.05f));
                ReadTimeDataCrntn = StartCoroutine(ReadDataTime(20, 30, 5, 0.1f));
                yield return new WaitForSeconds(5.7f);

                StopCoroutine(ReadSpeedDataCrntn);
                StopCoroutine(ReadDistanceDataCrntn);
                StopCoroutine(ReadTimeDataCrntn);

                Screen2Lbl.text = "Kontrol Noktası:";
                Screen2Txt.text = "5";
                Screen2Obj.SetActive(true);

                tabletController.activity4Scene1Controller.SwitchOnTaskPanel(2);
                tabletController.activity4Scene1Controller.SwitchOnOffAnswerContainer(2, 4, true);
                InteractionObjectsActive(true);
                speechBubbleText.text = "Beşinci kontrol noktasına ulaştınız. Ölçüm cihazından okuduğun değerleri tablete gir.";
                yield return new WaitWhile(() => speechBubble.activeSelf); //yield return new WaitForSeconds(HelperController.SpeechbublpeWaitTime(speechBubbleText.text, textChangeSpeed));
                InteractionObjectsActive(false);
                yield return new WaitUntil(() => checkTabletValues);
                yield return new WaitWhile(() => sceneMessageController.successMessagePanel.Button.transform.parent.gameObject.activeSelf);
                #endregion
                TaskComleted();
                break;
            case 3:
                #region initialize
                tabletController.activity4Scene1Controller.ManevraAnswers[0].SetAnswers(5, 10, 2);
                tabletController.activity4Scene1Controller.ManevraAnswers[1].SetAnswers(5, 20, 4);
                tabletController.activity4Scene1Controller.ManevraAnswers[2].SetAnswers(5, 30, 6);
                tabletController.activity4Scene1Controller.ManevraAnswers[3].SetAnswers(5, 40, 8);
                tabletController.activity4Scene1Controller.ManevraAnswers[4].SetAnswers(5, 50, 10);
                toolController.ActiveItems[0].isSwitchOn = false;
                toolController.ActiveItems[0].SetUnitText("---");
                toolController.ActiveItems[0].SetResultText("---");
                toolController.ActiveItems[1].isSwitchOn = false;
                toolController.ActiveItems[1].SetUnitText("---");
                toolController.ActiveItems[1].SetResultText("---");
                toolController.ActiveItems[2].isSwitchOn = false;
                toolController.ActiveItems[2].SetUnitText("---");
                toolController.ActiveItems[2].SetResultText("---");
                Screen2Obj.SetActive(false);



                #endregion

                #region setting
                sceneMessageController.ShowMessage(HelperController.MessageType.Task, 3.1f, HelperController.ButtonType.ClosePanel, false);
                isSettingDone = false;
                settingPanelController.measurementToolType = MeasurementToolType.None;
                SettingCoroutine = StartCoroutine(SettingTools());
                while (!isSettingDone)
                {
                    yield return new WaitForEndOfFrame();
                }
                StopCoroutine(SettingCoroutine);
                CheckandApproveBtn.interactable = false;

                sceneMessageController.ShowMessage(HelperController.MessageType.Success, 3f, HelperController.ButtonType.Continue, true);

                yield return new WaitWhile(() => sceneMessageController.successMessagePanel.Button.transform.parent.gameObject.activeSelf);

                sceneMessageController.ShowMessage(HelperController.MessageType.Task, 1.3f, HelperController.ButtonType.Continue, true);

                yield return new WaitWhile(() => sceneMessageController.taskMessagePanel.Button.transform.parent.gameObject.activeSelf);

                #endregion

                #region Lunch
                InteractionObjectsActive(true);
                speechBubbleText.text = "Kenetlenme işlemi için manevra motorları çalıştırılıyor.";
                yield return new WaitWhile(() => speechBubble.activeSelf); //yield return new WaitForSeconds(HelperController.SpeechbublpeWaitTime(speechBubbleText.text, textChangeSpeed));
                InteractionObjectsActive(false);

                Screen2Lbl.text = "Çalışmaya";

                Screen2Obj.SetActive(true);
                _sTime = 0;
                _fTime = _sTime + lunchCountDown;
                while (_fTime - _sTime > 0)
                {
                    Screen2Txt.text = (_fTime - _sTime).ToString();
                    yield return new WaitForSeconds(.5f);
                    Screen2Txt.text = "";
                    yield return new WaitForSeconds(.5f);
                    _sTime = _sTime + 1;

                }
                Screen2Lbl.text = "";
                Screen2Txt.text = "Çalıştı.";

                yield return new WaitForSeconds(.5f);
                #endregion
                #region CheckPoint 1
                activeCheckPoint = 0;
                checkTabletValues = false;
                sceneMessageController.ShowMessage(HelperController.MessageType.Task, 1.31f, HelperController.ButtonType.Continue, true);
                yield return new WaitWhile(() => sceneMessageController.taskMessagePanel.Button.transform.parent.gameObject.activeSelf);

                Screen2Obj.SetActive(false);
                CheckPointFlagReset();

                ReadSpeedDataCrntn = StartCoroutine(ReadDataSpeed(0, 5, 2, 0.2f));
                ReadDistanceDataCrntn = StartCoroutine(ReadDataDistance(0, 10, 5, 0.05f));
                ReadTimeDataCrntn = StartCoroutine(ReadDataTime(0, 2, 5, 0.1f));

                yield return new WaitForSeconds(5.7f);

                StopCoroutine(ReadSpeedDataCrntn);
                StopCoroutine(ReadDistanceDataCrntn);
                StopCoroutine(ReadTimeDataCrntn);

                Screen2Lbl.text = "Kontrol Noktası:";
                Screen2Txt.text = "1";
                Screen2Obj.SetActive(true);

                tabletController.activity4Scene1Controller.SwitchOnTaskPanel(3);
                tabletController.activity4Scene1Controller.SwitchOnOffAnswerContainer(3, 0, true);
                InteractionObjectsActive(true);
                speechBubbleText.text = "Birinci kontrol noktasına ulaştınız. Ölçüm cihazından okuduğun değerleri tablete gir.";
                yield return new WaitWhile(() => speechBubble.activeSelf); //yield return new WaitForSeconds(HelperController.SpeechbublpeWaitTime(speechBubbleText.text, textChangeSpeed));
                InteractionObjectsActive(false);
                yield return new WaitUntil(() => checkTabletValues);
                yield return new WaitWhile(() => sceneMessageController.successMessagePanel.Button.transform.parent.gameObject.activeSelf);
                #endregion
                #region CheckPoint 2
                tabletController.SetTabletActive(false);
                activeCheckPoint = 1;
                checkTabletValues = false;
                sceneMessageController.ShowMessage(HelperController.MessageType.Task, 1.32f, HelperController.ButtonType.Continue, true);
                yield return new WaitWhile(() => sceneMessageController.taskMessagePanel.Button.transform.parent.gameObject.activeSelf);

                Screen2Obj.SetActive(false);
                CheckPointFlagReset();

                ReadSpeedDataCrntn = StartCoroutine(ReadDataSpeed(5, 5, 2, 0.2f));
                ReadDistanceDataCrntn = StartCoroutine(ReadDataDistance(10, 20, 5, 0.05f));
                ReadTimeDataCrntn = StartCoroutine(ReadDataTime(2, 4, 5, 0.1f));
                yield return new WaitForSeconds(5.7f);

                StopCoroutine(ReadSpeedDataCrntn);
                StopCoroutine(ReadDistanceDataCrntn);
                StopCoroutine(ReadTimeDataCrntn);

                Screen2Lbl.text = "Kontrol Noktası:";
                Screen2Txt.text = "2";
                Screen2Obj.SetActive(true);

                tabletController.activity4Scene1Controller.SwitchOnTaskPanel(3);
                tabletController.activity4Scene1Controller.SwitchOnOffAnswerContainer(3, 1, true);
                InteractionObjectsActive(true);
                speechBubbleText.text = "İkinci kontrol noktasına ulaştınız. Ölçüm cihazından okuduğun değerleri tablete gir.";
                yield return new WaitWhile(() => speechBubble.activeSelf); //yield return new WaitForSeconds(HelperController.SpeechbublpeWaitTime(speechBubbleText.text, textChangeSpeed));
                InteractionObjectsActive(false);
                yield return new WaitUntil(() => checkTabletValues);
                yield return new WaitWhile(() => sceneMessageController.successMessagePanel.Button.transform.parent.gameObject.activeSelf);
                #endregion
                #region CheckPoint 3
                tabletController.SetTabletActive(false);
                activeCheckPoint = 2;
                checkTabletValues = false;
                sceneMessageController.ShowMessage(HelperController.MessageType.Task, 1.33f, HelperController.ButtonType.Continue, true);
                yield return new WaitWhile(() => sceneMessageController.taskMessagePanel.Button.transform.parent.gameObject.activeSelf);

                Screen2Obj.SetActive(false);
                CheckPointFlagReset();

                ReadSpeedDataCrntn = StartCoroutine(ReadDataSpeed(5, 5, 2, 0.2f));
                ReadDistanceDataCrntn = StartCoroutine(ReadDataDistance(20, 30, 5, 0.05f));
                ReadTimeDataCrntn = StartCoroutine(ReadDataTime(4, 6, 5, 0.1f));
                yield return new WaitForSeconds(5.7f);

                StopCoroutine(ReadSpeedDataCrntn);
                StopCoroutine(ReadDistanceDataCrntn);
                StopCoroutine(ReadTimeDataCrntn);

                Screen2Lbl.text = "Kontrol Noktası:";
                Screen2Txt.text = "3";
                Screen2Obj.SetActive(true);

                tabletController.activity4Scene1Controller.SwitchOnTaskPanel(3);
                tabletController.activity4Scene1Controller.SwitchOnOffAnswerContainer(3, 2, true);
                InteractionObjectsActive(true);
                speechBubbleText.text = "Üçüncü kontrol noktasına ulaştınız. Ölçüm cihazından okuduğun değerleri tablete gir.";
                yield return new WaitWhile(() => speechBubble.activeSelf); //yield return new WaitForSeconds(HelperController.SpeechbublpeWaitTime(speechBubbleText.text, textChangeSpeed));
                InteractionObjectsActive(false);
                yield return new WaitUntil(() => checkTabletValues);
                yield return new WaitWhile(() => sceneMessageController.successMessagePanel.Button.transform.parent.gameObject.activeSelf);
                #endregion
                #region CheckPoint 4
                tabletController.SetTabletActive(false);
                activeCheckPoint = 3;
                checkTabletValues = false;
                sceneMessageController.ShowMessage(HelperController.MessageType.Task, 1.34f, HelperController.ButtonType.Continue, true);
                yield return new WaitWhile(() => sceneMessageController.taskMessagePanel.Button.transform.parent.gameObject.activeSelf);

                Screen2Obj.SetActive(false);
                CheckPointFlagReset();

                ReadSpeedDataCrntn = StartCoroutine(ReadDataSpeed(5, 5, 2, 0.2f));
                ReadDistanceDataCrntn = StartCoroutine(ReadDataDistance(30, 40, 5, 0.05f));
                ReadTimeDataCrntn = StartCoroutine(ReadDataTime(6, 8, 5, 0.1f));
                yield return new WaitForSeconds(5.7f);

                StopCoroutine(ReadSpeedDataCrntn);
                StopCoroutine(ReadDistanceDataCrntn);
                StopCoroutine(ReadTimeDataCrntn);

                Screen2Lbl.text = "Kontrol Noktası:";
                Screen2Txt.text = "4";
                Screen2Obj.SetActive(true);

                tabletController.activity4Scene1Controller.SwitchOnTaskPanel(3);
                tabletController.activity4Scene1Controller.SwitchOnOffAnswerContainer(3, 3, true);
                InteractionObjectsActive(true);
                speechBubbleText.text = "Dördüncü kontrol noktasına ulaştınız. Ölçüm cihazından okuduğun değerleri tablete gir.";
                yield return new WaitWhile(() => speechBubble.activeSelf); //yield return new WaitForSeconds(HelperController.SpeechbublpeWaitTime(speechBubbleText.text, textChangeSpeed));
                InteractionObjectsActive(false);
                yield return new WaitUntil(() => checkTabletValues);
                yield return new WaitWhile(() => sceneMessageController.successMessagePanel.Button.transform.parent.gameObject.activeSelf);
                #endregion
                #region CheckPoint 5
                tabletController.SetTabletActive(false);
                activeCheckPoint = 4;
                checkTabletValues = false;
                sceneMessageController.ShowMessage(HelperController.MessageType.Task, 1.35f, HelperController.ButtonType.Continue, true);
                yield return new WaitWhile(() => sceneMessageController.taskMessagePanel.Button.transform.parent.gameObject.activeSelf);

                Screen2Obj.SetActive(false);
                CheckPointFlagReset();

                ReadSpeedDataCrntn = StartCoroutine(ReadDataSpeed(5, 5, 4, 0.2f));
                ReadDistanceDataCrntn = StartCoroutine(ReadDataDistance(40, 50, 5, 0.05f));
                ReadTimeDataCrntn = StartCoroutine(ReadDataTime(8, 10, 5, 0.1f));
                yield return new WaitForSeconds(5.7f);

                StopCoroutine(ReadSpeedDataCrntn);
                StopCoroutine(ReadDistanceDataCrntn);
                StopCoroutine(ReadTimeDataCrntn);

                Screen2Lbl.text = "Kontrol Noktası:";
                Screen2Txt.text = "5";
                Screen2Obj.SetActive(true);

                tabletController.activity4Scene1Controller.SwitchOnTaskPanel(3);
                tabletController.activity4Scene1Controller.SwitchOnOffAnswerContainer(3, 4, true);
                InteractionObjectsActive(true);
                speechBubbleText.text = "Beşinci kontrol noktasına ulaştınız. Ölçüm cihazından okuduğun değerleri tablete gir.";
                yield return new WaitWhile(() => speechBubble.activeSelf); //yield return new WaitForSeconds(HelperController.SpeechbublpeWaitTime(speechBubbleText.text, textChangeSpeed));
                InteractionObjectsActive(false);
                yield return new WaitUntil(() => checkTabletValues);
                yield return new WaitWhile(() => sceneMessageController.successMessagePanel.Button.transform.parent.gameObject.activeSelf);
                #endregion
                TaskComleted();
                break;
            default:
                break;
        }
        // Debug.Log("Switch Broken");
    }

    public void CheckAnswer()
    {
        switch (activeTaskID)
        {
            case 1:
                if (tabletController.activity4Scene1Controller.FırlatmaAnswers[activeCheckPoint].CheckAnswer() == TabletActivity2AnswerModel.AnswerResponseType.NotNumeric)
                {
                    sceneMessageController.ShowMessage(HelperController.MessageType.Failure, 0.1f, HelperController.ButtonType.ClosePanel, true);

                }
                else if (tabletController.activity4Scene1Controller.FırlatmaAnswers[activeCheckPoint].CheckAnswer() == TabletActivity2AnswerModel.AnswerResponseType.True)
                {
                    checkTabletValues = true;
                    tabletController.activity4Scene1Controller.CompleteAnswer(1, activeCheckPoint, true);
                    sceneMessageController.ShowMessage(HelperController.MessageType.Success, 0, HelperController.ButtonType.Continue, true);

                }
                else
                {
                    sceneMessageController.ShowMessage(HelperController.MessageType.Failure, 0.0f, HelperController.ButtonType.ClosePanel, true);
                }
                break;
            case 2:
                if (tabletController.activity4Scene1Controller.MekikAnswers[activeCheckPoint].CheckAnswer() == TabletActivity2AnswerModel.AnswerResponseType.NotNumeric)
                {
                    sceneMessageController.ShowMessage(HelperController.MessageType.Failure, 0.1f, HelperController.ButtonType.ClosePanel, true);

                }
                else if (tabletController.activity4Scene1Controller.MekikAnswers[activeCheckPoint].CheckAnswer() == TabletActivity2AnswerModel.AnswerResponseType.True)
                {
                    checkTabletValues = true;
                    tabletController.activity4Scene1Controller.CompleteAnswer(1, activeCheckPoint, true);
                    sceneMessageController.ShowMessage(HelperController.MessageType.Success, 0, HelperController.ButtonType.Continue, true);

                }
                else
                {
                    sceneMessageController.ShowMessage(HelperController.MessageType.Failure, 0.0f, HelperController.ButtonType.ClosePanel, true);
                }
                break;
            case 3:
                if (tabletController.activity4Scene1Controller.ManevraAnswers[activeCheckPoint].CheckAnswer() == TabletActivity2AnswerModel.AnswerResponseType.NotNumeric)
                {
                    sceneMessageController.ShowMessage(HelperController.MessageType.Failure, 0.1f, HelperController.ButtonType.ClosePanel, true);

                }
                else if (tabletController.activity4Scene1Controller.ManevraAnswers[activeCheckPoint].CheckAnswer() == TabletActivity2AnswerModel.AnswerResponseType.True)
                {
                    checkTabletValues = true;
                    tabletController.activity4Scene1Controller.CompleteAnswer(1, activeCheckPoint, true);
                    sceneMessageController.ShowMessage(HelperController.MessageType.Success, 0, HelperController.ButtonType.Continue, true);

                }
                else
                {
                    sceneMessageController.ShowMessage(HelperController.MessageType.Failure, 0.0f, HelperController.ButtonType.ClosePanel, true);
                }
                break;
            default:
                break;
        }
    }


    private void CheckPointFlagReset()
    {
        checkpointTime = false;
        checkpointSpeed = false;
        checkpointDistance = false;
    }

    private IEnumerator ReadDataSpeed(float beginSpeed, float endSpeed, float timePeriod, float timeFactor)
    {
        //  checkpointSpeed = false;
        toolController.ActiveItems[0].ToolResultText.text = beginSpeed.ToString();
        float t = (endSpeed - beginSpeed) * timeFactor;
        while ((endSpeed - beginSpeed) >= 0)
        {
            if (beginSpeed <= endSpeed)
            {
                toolController.ActiveItems[0].SetResultText(beginSpeed);
            }
            beginSpeed += t;

            yield return new WaitForSeconds(timePeriod * timeFactor);
        }
        checkpointSpeed = true;
    }

    private IEnumerator ReadDataDistance(float beginDistance, float endDistance, float timePeriod, float timeFactor)
    {
        //  checkpointDistance = false;
        toolController.ActiveItems[1].ToolResultText.text = beginDistance.ToString();
        float t = (endDistance - beginDistance) * timeFactor;
        while ((endDistance - beginDistance) >= 0)
        {

            if (beginDistance <= endDistance)
            {
                toolController.ActiveItems[1].SetResultText(beginDistance);
            }
            beginDistance += t;

            yield return new WaitForSeconds(timePeriod * timeFactor);
        }
        checkpointDistance = true;
    }

    private IEnumerator ReadDataTime(float beginTime, float endTime, float timePeriod, float timeFactor)
    {
        //   Debug.Log("EndTime:" + endTime);
        //  checkpointTime = false;
        toolController.ActiveItems[2].ToolResultText.text = beginTime.ToString();
        float t = (endTime - beginTime) * timeFactor;
        while ((endTime - beginTime) >= 0)
        {
            if (beginTime <= endTime)
            {
                toolController.ActiveItems[2].SetResultText(beginTime);
            }
            beginTime += t;

            yield return new WaitForSeconds(timePeriod * timeFactor);
        }
        toolController.ActiveItems[2].SetResultText(endTime);

        checkpointTime = true;

    }

    private IEnumerator SettingTools()
    {
        sceneMessageController.ShowMessage(HelperController.MessageType.Task, activeTaskID + 0.11f, HelperController.ButtonType.ClosePanel, false);

        while (true)
        {

            if (toolController.IsAllToolsSetted())
            {
                sceneMessageController.ShowMessage(HelperController.MessageType.Task, activeTaskID + 0.2f, HelperController.ButtonType.ClosePanel, false);
                CheckandApproveBtn.interactable = true;
            }
            else
            {
                CheckandApproveBtn.interactable = false;

                if (settingPanelController.measurementToolType == MeasurementToolType.None)
                {
                    sceneMessageController.ShowMessage(HelperController.MessageType.Task, activeTaskID + 0.11f, HelperController.ButtonType.ClosePanel, false);
                }
                if (settingPanelController.measurementToolType == MeasurementToolType.Distance)
                {
                    sceneMessageController.ShowMessage(HelperController.MessageType.Task, activeTaskID + 0.12f, HelperController.ButtonType.ClosePanel, false);
                }
                if (settingPanelController.measurementToolType == MeasurementToolType.Time)
                {
                    sceneMessageController.ShowMessage(HelperController.MessageType.Task, activeTaskID + 0.14f, HelperController.ButtonType.ClosePanel, false);
                }
                if (settingPanelController.measurementToolType == MeasurementToolType.Speed)
                {
                    sceneMessageController.ShowMessage(HelperController.MessageType.Task, activeTaskID + 0.16f, HelperController.ButtonType.ClosePanel, false);
                }
            }
            yield return new WaitForEndOfFrame();
        }
    }
    public void CheckSettings()
    {
        if (!toolController.IsAllToolsSetted())
        {
            return;
        }
        bool _distance = false;
        bool _time = false;
        bool _speed = false;

        switch (activeTaskID)
        {
            case 1:
                foreach (Activity4Scene2ToolItemController item in toolController.ActiveItems)
                {
                    if (item.Type == MeasurementToolType.Distance && item.ToolUnitText.text == Activity4Scene2ToolItemController.DistanceUnits.km.ToString())
                    {
                        _distance = true;

                    }
                    if (item.Type == MeasurementToolType.Time && item.ToolUnitText.text == Activity4Scene2ToolItemController.TimeUnits.sa.ToString())
                    {
                        _time = true;
                    }
                    if (item.Type == MeasurementToolType.Speed && item.ToolUnitText.text == Activity4Scene2ToolItemController.SpeedUnits.km_sa.ToString().Replace("_", "/"))
                    {
                        _speed = true;
                    }
                }

                break;
            case 2:
                foreach (Activity4Scene2ToolItemController item in toolController.ActiveItems)
                {
                    if (item.Type == MeasurementToolType.Distance && item.ToolUnitText.text == Activity4Scene2ToolItemController.DistanceUnits.m.ToString())
                    {
                        _distance = true;
                    }
                    if (item.Type == MeasurementToolType.Time && item.ToolUnitText.text == Activity4Scene2ToolItemController.TimeUnits.dk.ToString())
                    {
                        _time = true;
                    }
                    if (item.Type == MeasurementToolType.Speed && item.ToolUnitText.text == Activity4Scene2ToolItemController.SpeedUnits.m_dk.ToString().Replace("_", "/"))
                    {
                        _speed = true;
                    }
                }
                break;
            case 3:
                foreach (Activity4Scene2ToolItemController item in toolController.ActiveItems)
                {
                    if (item.Type == MeasurementToolType.Distance && item.ToolUnitText.text == Activity4Scene2ToolItemController.DistanceUnits.cm.ToString())
                    {
                        _distance = true;
                    }
                    if (item.Type == MeasurementToolType.Time && item.ToolUnitText.text == Activity4Scene2ToolItemController.TimeUnits.sn.ToString())
                    {
                        _time = true;
                    }
                    if (item.Type == MeasurementToolType.Speed && item.ToolUnitText.text == Activity4Scene2ToolItemController.SpeedUnits.cm_sn.ToString().Replace("_", "/"))
                    {
                        _speed = true;
                    }
                }
                break;
            default:
                break;
        }
        isSettingDone = (_distance && _speed && _time);

        #region ShowFailMessage
        if (!_distance && !_speed && !_time)
        {
            sceneMessageController.ShowMessage(HelperController.MessageType.Failure, activeTaskID + 0.1f, HelperController.ButtonType.ClosePanel, true);

        }
        else if (!_distance && _time && _speed)     // Yol hatalı
        {
            sceneMessageController.ShowMessage(HelperController.MessageType.Failure, 1.2f, HelperController.ButtonType.ClosePanel, true);
        }
        else if (_distance && !_time && _speed)     // zaman hatalı
        {
            sceneMessageController.ShowMessage(HelperController.MessageType.Failure, 1.3f, HelperController.ButtonType.ClosePanel, true);
        }
        else if (_distance && _time && !_speed)     // hız hatalı
        {
            sceneMessageController.ShowMessage(HelperController.MessageType.Failure, 1.4f, HelperController.ButtonType.ClosePanel, true);
        }
        else if (!_distance && !_time && _speed)   // yol ve zaman hatalı
        {
            sceneMessageController.ShowMessage(HelperController.MessageType.Failure, 1.5f, HelperController.ButtonType.ClosePanel, true);
        }
        else if (!_distance && _time && !_speed)   // yol ve hız hatalı
        {
            sceneMessageController.ShowMessage(HelperController.MessageType.Failure, 1.6f, HelperController.ButtonType.ClosePanel, true);
        }
        else if (_distance && !_time && !_speed)  // zaman ve hız hatalı
        {
            sceneMessageController.ShowMessage(HelperController.MessageType.Failure, 1.7f, HelperController.ButtonType.ClosePanel, true);
        }
        #endregion


    }
    public void TaskComleted()
    {
        tabletController.SetTabletActive(false);
        HelperController.TaskController.TaskComplete(activeTaskID);
        tabletController.activity1scene1Controller.MenuButtonComplete(activeTaskID - 1);
        StopAllCoroutines();
        sceneMessageController.ShowMessage(HelperController.MessageType.Success, activeTaskID + 3, HelperController.ButtonType.ClosePanel, true);
    }

    public void RestartTask()
    {
        if (activeTaskID > -1)
        {
            StopAllCoroutines();
            activeTaskID--;
            NextTask();
        }

    }
    #endregion

    #region SectionOps
    public void NextSection()
    {
        StopAllCoroutines();
        StartCoroutine(NextSectionProcess());
    }

    private IEnumerator NextSectionProcess()
    {
        InteractionObjectsActive(true);
        speechBubbleText.text = "Kenetlenme tamamlandı. Artık Mars araştırmaları yapılabilir. Araştırmalar tamamlandıktan sonra Dünya'ya döneceksiniz.";
        yield return new WaitWhile(() => speechBubble.activeSelf); //yield return new WaitForSeconds(HelperController.SpeechbublpeWaitTime(speechBubbleText.text, textChangeSpeed));
        InteractionObjectsActive(false);
        sceneMessageController.ShowMessage(HelperController.MessageType.Section, 1f, HelperController.ButtonType.ClosePanel, false);

    }
    public void NextSectionGo()
    {
        StopAllCoroutines();
        HelperController.GameController.sceneController.LoadNextScene();
    }
    #endregion

    #region List
    public List<TaskController.TaskStruct> taskList = new List<TaskController.TaskStruct>();
    private void TaskGenerate()
    {
        taskList.Add(new TaskController.TaskStruct("Mekike bin ve koltuğuna otur"));
        taskList.Add(new TaskController.TaskStruct("Fırlatma anında Fırlatma motorları verilerini toplamak için ölçüm cihazlarını ayarla."));
        taskList.Add(new TaskController.TaskStruct("Mars araştırma istasyonuna doğru ilerlerken Mekik Motoru verilerini toplamak için cihazları ayarla"));
        taskList.Add(new TaskController.TaskStruct("İstasyona kenetlenme anında Manevra motorları verilerini toplamak için cihazları ayarla."));

        HelperController.TaskController.TaskListFillOnSceneStart(taskList);

    }
    #endregion
    #region Helper
    public float SpeechbublpeWaitTime()
    {
        return (speechBubbleText.text.Length * Time.deltaTime / textChangeSpeed) + 1.5f;
    }
    #endregion
}
