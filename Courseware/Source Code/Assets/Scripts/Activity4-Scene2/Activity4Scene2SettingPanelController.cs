﻿using UnityEngine;
using System.Collections;
using System;
using UnityEngine.UI;
using System.Collections.Generic;

public class Activity4Scene2SettingPanelController : MonoBehaviour
{

    //  public Activity4Scene2ToolController tc;
    [HideInInspector]
    public MeasurementToolType measurementToolType;
    public Text[] buttonTexts;
    public GameObject SwitcButton;

    private int SelectedSlot;
    private Activity4Scene2ToolItemController selectedTool;

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }
    void OnEnable()
    {
        SwitcButton.GetComponent<Transform>().eulerAngles = new Vector3(0f, 0f, SwitchRotationZValue[-1]);
    }
    #region PanelMethods
    public void SwitchOn(int id)
    {
        measurementToolType = (MeasurementToolType)id;
        gameObject.SetActive(true);
    }
    internal void SwitchOn(MeasurementToolType toolType, int slot)
    {
        measurementToolType = toolType;
        SelectedSlot = slot;
        ChangeButtonLabels();
        gameObject.SetActive(true);

    }
    internal void SwitchOn(Activity4Scene2ToolItemController tool)
    {
        selectedTool = tool;
        measurementToolType = tool.Type;
       

        ChangeButtonLabels();
        gameObject.SetActive(true);

    }
    private void ChangeButtonLabels()
    {
        switch (measurementToolType)
        {
            case MeasurementToolType.Speed:
                buttonTexts[1].text = "cm/sn";
                buttonTexts[2].text = "m/dk";
                buttonTexts[3].text = "km/s";

                break;
            case MeasurementToolType.Distance:
                buttonTexts[1].text = "santimetre";
                buttonTexts[2].text = "metre";
                buttonTexts[3].text = "kilometre";
                break;
            case MeasurementToolType.Time:
                buttonTexts[1].text = "saniye";
                buttonTexts[2].text = "dakika";
                buttonTexts[3].text = "saat";
                break;
            default:
                break;
        }
    }
    public void SwitchOff()
    {
        measurementToolType = MeasurementToolType.None;
        gameObject.SetActive(false);
    }

    #endregion

    #region settingMethods

    public void SetUnit(int unitId)
    {
        selectedTool.SetUnitTextGeneric(unitId);


        // SwitcButton.GetComponent<Transform>().eulerAngles = new Vector3(0f, 0f, SwitchRotationZValue[unitId]);
        StartCoroutine(RotateSwitch(unitId));
       

    }

    private IEnumerator RotateSwitch(int unitId)
    {
        float _stime = Time.time;
        float _ftime = _stime + 1.3f;
        while (_stime<_ftime)
        {
            SwitcButton.GetComponent<Transform>().eulerAngles = Vector3.Lerp(SwitcButton.GetComponent<Transform>().eulerAngles, new Vector3(0f, 0f, SwitchRotationZValue[unitId]), 5 * Time.deltaTime);
            _stime += Time.deltaTime;
            yield return new WaitForEndOfFrame();
        }
    }

    #endregion

    public Dictionary<int, float> SwitchRotationZValue = new Dictionary<int, float>()
    {
        { -1,120.0f },
        {0,20.0f },
        {1,300f },
        {2,220.0f },


    };

}
