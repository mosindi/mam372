﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class Activity4Scene2MessageController : MonoBehaviour
{

    public Text HudBottomMessageText;
    public HudMessageStruct taskMessagePanel;
    public HudMessageStruct successMessagePanel;
    public HudMessageStruct FailulerMessagePanel;

    // Use this for initialization
    void Start()
    {
        HudBottomMessageText.text = "";
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void ShowMessage(HelperController.MessageType mType, float id = 0, HelperController.ButtonType buttonEvent = HelperController.ButtonType.ClosePanel, bool notification = false)
    {

        switch (mType)
        {
            case HelperController.MessageType.Task:
                if (ActivityMessageListTask.Count > id)
                {
                    //   HelperController.GameController.isPlayable = false;
                    if (notification)
                    {
                        ShowNewTaskNotify(id);
                    }
                    HudBottomMessageText.text = ActivityMessageListTask[id];
                }

                break;
            case HelperController.MessageType.Warning:
                break;
            case HelperController.MessageType.Success:
                ShowSuccessNotify((int)id);
                HudBottomMessageText.text = "";
                break;
            case HelperController.MessageType.Failure:
                ShowFaillureNotify(id);
                break;
            case HelperController.MessageType.Section:
                ShowSectionNotify((int)id);
                HudBottomMessageText.text = ActivityMessageListSection[id];
                break;
            default:
                break;
        }
        AddEventListenerToButton(mType, buttonEvent);

    }
    private void AddEventListenerToButton(HelperController.MessageType mType, HelperController.ButtonType buttonEvent = HelperController.ButtonType.ClosePanel)
    {
        switch (mType)
        {
            #region Task
            case HelperController.MessageType.Task:
                taskMessagePanel.Button.onClick.RemoveAllListeners();
                switch (buttonEvent)
                {

                    case HelperController.ButtonType.ClosePanel:
                        taskMessagePanel.Button.onClick.AddListener(delegate
                        {
                            taskMessagePanel.Button.transform.parent.gameObject.SetActive(false);
                            HelperController.GameController.isPlayable = true;
                            gameObject.BroadcastMessage("TaskStarted");
                        });
                        break;
                    case HelperController.ButtonType.NextSection:
                        taskMessagePanel.Button.onClick.AddListener(delegate
                        {
                            taskMessagePanel.Button.transform.parent.gameObject.SetActive(false);
                            HelperController.GameController.isPlayable = true;
                            //  Debug.Log("Task Message Pane Button");
                            HelperController.GameController.sceneController.LoadNextScene();
                        });

                        break;
                    case HelperController.ButtonType.Continue:
                        taskMessagePanel.Button.onClick.AddListener(delegate
                        {
                            taskMessagePanel.Button.transform.parent.gameObject.SetActive(false);
                            HelperController.GameController.isPlayable = true;
                            //   gameObject.BroadcastMessage("TaskStarted");
                        });
                        break;
                    default:
                        break;
                }
                break;
            #endregion
            case HelperController.MessageType.Warning:
                break;
            case HelperController.MessageType.Success:
                successMessagePanel.Button.onClick.RemoveAllListeners();
                switch (buttonEvent)
                {
                    case HelperController.ButtonType.ClosePanel:
                        successMessagePanel.Button.onClick.AddListener(delegate
                        {
                            successMessagePanel.Button.transform.parent.gameObject.SetActive(false);
                            HelperController.GameController.isPlayable = true;
                            gameObject.BroadcastMessage("NextTask");
                        });
                        break;
                    case HelperController.ButtonType.NextSection:
                        successMessagePanel.Button.onClick.AddListener(delegate
                        {
                            successMessagePanel.Button.transform.parent.gameObject.SetActive(false);
                            HelperController.GameController.isPlayable = true;
                            HelperController.GameController.sceneController.LoadNextScene();
                        });

                        break;
                    case HelperController.ButtonType.Continue:
                        successMessagePanel.Button.onClick.AddListener(delegate
                        {
                            successMessagePanel.Button.transform.parent.gameObject.SetActive(false);
                            HelperController.GameController.isPlayable = true;

                        });
                        break;
                    default:
                        break;
                }
                break;
            case HelperController.MessageType.Failure:
                switch (buttonEvent)
                {
                    case HelperController.ButtonType.ClosePanel:
                        FailulerMessagePanel.Button.onClick.AddListener(delegate
                        {
                            FailulerMessagePanel.Button.transform.parent.gameObject.SetActive(false);
                            HelperController.GameController.isPlayable = true;
                            //     BroadcastMessage("RestartMission");
                        });

                        break;
                    case HelperController.ButtonType.HudToolBox:
                        break;
                    case HelperController.ButtonType.NextSection:
                        break;
                    default:
                        break;
                }

                break;
            case HelperController.MessageType.Section:
                taskMessagePanel.Button.onClick.RemoveAllListeners();
                switch (buttonEvent)
                {

                    case HelperController.ButtonType.ClosePanel:
                        taskMessagePanel.Button.onClick.AddListener(delegate
                        {
                            taskMessagePanel.Button.transform.parent.gameObject.SetActive(false);
                            HelperController.GameController.isPlayable = true;
                            gameObject.BroadcastMessage("NextSectionGo");
                        });
                        break;
                    case HelperController.ButtonType.NextSection:
                        taskMessagePanel.Button.onClick.AddListener(delegate
                        {
                            taskMessagePanel.Button.transform.parent.gameObject.SetActive(false);
                            HelperController.GameController.isPlayable = true;
                            //  Debug.Log("Task Message Pane Button");
                            HelperController.GameController.sceneController.LoadNextScene();
                        });

                        break;
                    default:
                        break;
                }
                break;
            default:
                break;
        }
    }

    private void ShowNewTaskNotify(float id)
    {
        taskMessagePanel.Button.transform.parent.gameObject.SetActive(true);
        taskMessagePanel.Text.text = ActivityMessageListTask[id];
        HelperController.GameController.isPlayable = false;

    }
    private void ShowSuccessNotify(int id)
    {
        successMessagePanel.Button.transform.parent.gameObject.SetActive(true);
        successMessagePanel.Text.text = ActivityMessageListSuccess[id];
        HelperController.GameController.isPlayable = false;

    }
    private void ShowFaillureNotify(float id)
    {
        FailulerMessagePanel.Button.transform.parent.gameObject.SetActive(true);
        FailulerMessagePanel.Text.text = ActivityMessageListFailure[id];
        HelperController.GameController.isPlayable = false;

    }
    private void ShowSectionNotify(int id)
    {
        taskMessagePanel.Button.transform.parent.gameObject.SetActive(true);
        taskMessagePanel.Text.text = ActivityMessageListSection[id];
        HelperController.GameController.isPlayable = false;
    }


    #region Lists
    #region Task
    public static Dictionary<float, string> ActivityMessageListTask = new Dictionary<float, string>()
    {
         {0.0f,"Mekiğe bin ve koltuğa otur." },

         {1.0f,"Fırlatma için ölçüm cihazlarını, cihazların üzerindeki ayarla düğmesine tıklayarak ayarla. Fırlatma için onay ver." },
         {1.1f,"Ölçüm cihazları üzerindeki \"Ayarla\" düğmesine tıkla ve uygun birimi seç. Fırlatma motorları için yol ve zaman ölçüm cihazından gelen verilerle hız ölçerin çalıştığını hatırlamalısın."},

         {1.11f,"Fırlatmaya başlamak için ölçüm cihazlarnı ayarlamalısın." },
         {1.12f,"Fırlatma aşamasında uzun bir yol gidilecek bu yüzden Kilometreye ayarlamasın." },
         {1.14f,"Fırlatma aşamasında yol uzun olduğu için çok zaman alacak bu yüzden saati seçmelisin." },
         {1.16f,"Fırlatma aşamasında yol ölçer ve zaman ölçer için seçtiğin birimlere göre hız ölçerin birimini seçmelisin." },
         {1.2f,"Ölçüm cihazlarını ayarlamasını tamamladıysan kontrol et ve onay ver düğmesines tıklayabilirsin." },
         {1.3f,"Kontrol noktalarında ölçüm cihazlarındaki verileri tablete gir." },
         {1.31f,"1. Kontrol noktasında ölçüm cihazlarındaki verileri tablete gir." },
         {1.32f,"2. Kontrol noktasında ölçüm cihazlarındaki verileri tablete gir." },
         {1.33f,"3. Kontrol noktasında ölçüm cihazlarındaki verileri tablete gir." },
         {1.34f,"4. Kontrol noktasında ölçüm cihazlarındaki verileri tablete gir." },
         {1.35f,"5. Kontrol noktasında ölçüm cihazlarındaki verileri tablete gir." },


         {2.0f,"Mekik ile ilerlemek için ölçüm cihazlarını, cihazların üzerindeki ayarla düğmesine tıklayarak ayarla. Fırlatma motorlarını ayırmak ve mekik motorlarını çalıştırmak için onay ver." },
         {2.1f,"Ölçüm cihazları üzerindeki \"Ayarla\" düğmesine tıkla ve uygun birimi seç. Mekik motorları için yol ve zaman ölçüm cihazından gelen verilerle hız ölçerin çalıştığını hatırlamalısın."},
         {2.11f,"Mekik ile ilermek için ölçüm cihazlarnı ayarlamalısın." },
         {2.12f,"Mekik ile ilerken fırlatma motoruna göre daha yavaş gidilecek bu yüzden Metre'ye ayarlamalısın." },
         {2.14f,"Mekik ile ilerken fırlatma motoruna göre daha yavaş gidilecek bu yüzden Dakika'yı seçmelisin." },
         {2.16f,"Mekik ile ilerken yol ölçer ve zaman ölçer için seçtiğin birimlere göre hız ölçerin birimini seçmelisin." },

         {2.2f,"Ölçüm cihazlarını ayarlamasını tamamladıysan kontrol et ve onay ver düğmesines tıklayabilirsin." },

         {3.0f,"İstasyona kenetlenmek için ölçüm cihazlarını, cihazların üzerindeki ayarla düğmesine tıklayarak ayarla. Manevra motorlarını çalıştırmak için onay ver." },
         {3.1f,"Ölçüm cihazları üzerindeki \"Ayarla\" düğmesine tıkla ve uygun birimi seç. Manevra motorları için yol ve zaman ölçüm cihazından gelen verilerle hız ölçerin çalıştığını hatırlamalısın."},
         {3.11f,"Manevra motorları ile istayona kenetlenmek için ölçüm cihazlarnı ayarlamalısın." },
         {3.12f,"Kenetlenme esnasında çok yavaş olunması gerekiyor bu yüzden Santimetre'ye ayarlamalısın." },
         {3.14f,"Kenetlenme esnasında çok yavaş olunması gerekiyor bu yüzden Saniye'yi seçmelisin." },
         {3.16f,"Kenetlenme esnasında yol ölçer ve zaman ölçer için seçtiğin birimlere göre hız ölçerin birimini seçmelisin." },

         {3.2f,"Ölçüm cihazlarını ayarlamasını tamamladıysan kontrol et ve onay ver düğmesines tıklayabilirsin." },
    };
    #endregion
    #region Success
    public static Dictionary<int, string> ActivityMessageListSuccess = new Dictionary<int, string>()
    {
        {0,"Doğru cevap tebrikler!" },
        {1,"Görevi başarı ile tamamladın, devam edebiliriz." },
        {2,"Tüm Görevleri başarı ile tamamladın, devam edebiliriz." },
        {3,"Ölçüm cihazlarını doğru ayarladın. Tebrikler!" },
        {4,"Fırlatma anında Fırlatma motorları verileri toplandı. Tebrikler!" },
        {5,"Mars araştırma istasyonuna doğru ilerlerken Mekik motoru verileri toplandı. Tebrikler!" },
        {6,"İstasyona kenetlenmek anında Manevra motorlarının verileri toplandı. Tebrikler!" },

    };
    #endregion
    #region Failure
    public static Dictionary<float, string> ActivityMessageListFailure = new Dictionary<float, string>()
    {
        {0.0f,"Girdiğin değerler doğru değil. Daha dikkatli ol." },
        {0.1f,"Yalnızca rakamlardan oluşan değer gir" },

        {1.1f,"Ölçüm cihazları için seçtiğin birimler fırlatma motoru için uygun değil." },
        {1.2f,"Yol Ölçer için seçtiğin birim doğru değil." },
        {1.3f,"Zaman Ölçer için seçtiğin birim doğru değil." },
        {1.4f,"Hız Ölçer için seçtiğin birim doğru değil." },
        {1.5f,"Yol ve Zaman Ölçerler için seçtiğin birimler doğru değil." },
        {1.6f,"Yol ve Hız Ölçerler için seçtiğin birimler doğru değil." },
        {1.7f,"Zaman ve Hız Ölçerler için seçtiğin birimler doğru değil." },

        {2.1f,"Ölçüm cihazları için seçtiğin birimler mekik motoru için uygun değil." },
        {3.1f,"Ölçüm cihazları için seçtiğin birimler manevra motoru için uygun değil." },

    };
    #endregion
    #region Speech
    public static Dictionary<int, string> ActivityMessageListSpeech = new Dictionary<int, string>()
    {
        {0,"Merhaba" },
        {1,"" },
        {2,"" },
    };
    #endregion
    #region Section
    public static Dictionary<float, string> ActivityMessageListSection = new Dictionary<float, string>()
    {
        {0,"" },
        {1,"Araştırmalar başlasın."}
    };
    #endregion
    #endregion
}
