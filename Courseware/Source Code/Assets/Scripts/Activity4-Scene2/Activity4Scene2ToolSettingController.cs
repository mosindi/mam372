﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System;

public class Activity4Scene2ToolSettingController : MonoBehaviour {

    public SpriteRenderer text;
    public MeasurementToolType ToolType;
    public Activity4Scene2SettingPanelController settingPanel;
    public int slotID;

    public Activity4Scene2Controller ac;
    
    Activity4Scene2ToolItemController toolItemController;
    SpriteRenderer sprite;


	// Use this for initialization
	void Start () {
        sprite = gameObject.GetComponent<SpriteRenderer>();
        toolItemController = gameObject.transform.parent.GetComponent<Activity4Scene2ToolItemController>();
        ac = GameObject.FindGameObjectWithTag(HelperController.Tags.SceneController.ToString()).GetComponent<Activity4Scene2Controller>();
	}
	
	// Update is called once per frame
	void Update () {
	    
	}

    void OnMouseDown()
    {
        if (HelperController.GameController.isPlayable && !ac.isSettingDone)
        {
            ShowSettingPanel();
        }
    }

    private void ShowSettingPanel()
    {
        //settingPanel.SwitchOn(ToolType,slotID);
      //  Debug.Log(toolItemController.gameObject.name);
        settingPanel.SwitchOn(toolItemController);
    }

    void OnMouseEnter()
    {
        sprite.color = Color.green;
        text.color = Color.green;

    }

    void OnMouseExit()
    {
        sprite.color = Color.white;
        text.color = Color.white;




    }
}
