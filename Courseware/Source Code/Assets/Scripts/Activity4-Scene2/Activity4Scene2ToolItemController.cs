﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Activity4Scene2ToolItemController : MonoBehaviour {

    public Text ToolResultText;
    public Text ToolUnitText;
    public GameObject ToolSettingsButton;
    public MeasurementToolType Type;
    

    public bool isSwitchOn;

    // Use this for initialization
    void Start()
    {

    }
    void OnEnable()
    {
        isSwitchOn = false;

    }
    // Update is called once per frame
    void Update()
    {

    }

    public void SetResultText(float value)
    {
        ToolResultText.text = (isSwitchOn) ? value.ToString() : "---";

    }
    public void SetResultText(string value)
    {
        ToolResultText.text = (isSwitchOn) ? value : "---";
    }


    public void SetUnitTextGeneric(int unitId)
    {
        isSwitchOn = (unitId < 0) ? false : true;
        
        switch (Type)
        {
            case MeasurementToolType.Speed:
                SetUnitText((SpeedUnits)unitId);
                break;
            case MeasurementToolType.Distance:
                SetUnitText((DistanceUnits)unitId);
                break;
            case MeasurementToolType.Time:
                SetUnitText((TimeUnits)unitId);
                break;
            default:
                break;
        }
    }
    public void SetUnitText(string unit)
    {
        ToolUnitText.text = (isSwitchOn) ? unit.ToString() : "---";
    }
    public void SetUnitText(DistanceUnits unit)
    {
        ToolUnitText.text = (isSwitchOn) ? unit.ToString() : "---";
    }
    public void SetUnitText(TimeUnits unit)
    {
        ToolUnitText.text = (isSwitchOn) ? unit.ToString() : "---";

    }
    public void SetUnitText(SpeedUnits unit)
    {
        string s = unit.ToString();
        s = s.Replace("_", "/");
        ToolUnitText.text = (isSwitchOn) ? s.ToString() : "---";
    }


    #region Enums
    public enum DistanceUnits
    {
        cm,
        m,
        km,
    }
    public enum SpeedUnits
    {
        cm_sn,
        m_dk,
        km_sa,
    }
    public enum TimeUnits
    {
        sn,
        dk,
        sa
    }

    #endregion


   
}
