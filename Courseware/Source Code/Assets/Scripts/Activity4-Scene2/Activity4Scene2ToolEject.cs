﻿using UnityEngine;
using System.Collections;

public class Activity4Scene2ToolEject : MonoBehaviour {

    public GameObject Text;
    public int slotID;
    public Activity4Scene2ToolController tc;
   
    void OnMouseEnter()
    {
        if (HelperController.GameController.isPlayable)
        {
            Text.SetActive(true);
        }
    }
    void OnMouseExit()
    {
        if (HelperController.GameController.isPlayable)
        {
            Text.SetActive(false);
        }
    }
    public void OnMouseDown()
    {

        tc.ToolEject(slotID);
    }
}
