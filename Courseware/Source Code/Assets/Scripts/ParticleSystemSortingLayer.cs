﻿using UnityEngine;
using System.Collections;
[ExecuteInEditMode]
public class ParticleSystemSortingLayer : MonoBehaviour {
    [SerializeField]
    public SortingLayer sortingLayer;
    public string sortingLayerName;
    public int sortingOrder;
	// Use this for initialization
	void Start () {

        foreach (Renderer item in GetComponentsInChildren<Renderer>())
        {
            item.sortingLayerName = sortingLayerName;
            item.sortingOrder = sortingOrder;
        }

       
    }

    // Update is called once per frame
    void Update () {
	
	}
}
