﻿using UnityEngine;
using System.Collections;
[RequireComponent(typeof(Rigidbody2D))]
public class CrawlerController : MonoBehaviour {
    Rigidbody2D rb2d;
    Vector3 initialPos;
    
	// Use this for initialization
	void Start () {
        rb2d = GetComponent<Rigidbody2D>();
        initialPos = gameObject.transform.position;
	}
	
	// Update is called once per frame
	void Update () {

     //   Debug.Log("Velocity:" + rb2d.velocity);
        if (Input.GetKeyUp(KeyCode.Space) && rb2d.velocity.x<=0)
        {
           
        }
	}

    public void AddForce(float force)
    {
        rb2d.WakeUp();
        rb2d.AddForce(new Vector2(1, 0) * force*100, ForceMode2D.Force); ;
    }

    void OnTriggerExit2D(Collider2D coll)
    {
        if (coll.gameObject.name =="Finish")
        {
            rb2d.velocity = Vector2.zero;
            rb2d.Sleep();
            GameObject.FindGameObjectWithTag(HelperController.Tags.SceneController.ToString()).BroadcastMessage("TestFinish",SendMessageOptions.DontRequireReceiver);
        }
    }
    public void ReturnStartPosition()
    {
       // Debug.Log("return:" + gameObject.name+":"+"initialPos"+initialPos);
        rb2d.Sleep();
        gameObject.transform.position = initialPos;
    }
    public void SetCurrentPosAsInitialPos()
    {
        initialPos = gameObject.transform.position;

    }

}
