﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System;
using System.Collections.Generic;

public class Activity1Scene1Controller : MonoBehaviour
{
    public TabletController tabletController;
    public GameObject speechBubble;
    public Text speechBubbleText;
    public Animator agentAnim;
    [Range(0, 10)]
    public float textChangeSpeed;
    public Button IntroPassBtn;

    public GameObject testControlDevice;
    public List<ForkliftController> forklifts;
    public List<GameObject> MotorsGroupA;
    public List<GameObject> MotorsGroupB;
    public List<GameObject> MotorsGroupC;
    public float Motor1InitialPosX;
    public float Motor2InitialPosX;
    public float Motor3InitialPosX;

    public TrafficLightController trafficLightController;
    public Button TestStartBtn;
    public Activity1TabletGroupReportDragDrop TabletGroupA1;
    public Activity1TabletGroupReportDragDrop TabletGroupA2;
    public Activity1TabletGroupReportDragDrop TabletGroupB1;
    public Activity1TabletGroupReportDragDrop TabletGroupB2;
    public Activity1TabletGroupReportDragDrop TabletGroupC1;
    public Activity1TabletGroupReportDragDrop TabletGroupC2;




    public List<TaskController.TaskStruct> taskList = new List<TaskController.TaskStruct>();

    Activity1Scene1MessageController sceneMessageController;
    private string agentAnimParamStart = "Interaction";
    private int agentAnimParamStartId;
    private int activeTaskID = -1;
    private Coroutine IntroInteractionCoroutine;
    private bool[] testlist = { false };
    private bool isTesting = false;

    // Use this for initialization
    void Start()
    {

        sceneMessageController = GetComponent<Activity1Scene1MessageController>();
        tabletController.SwitcOn(TabletScreens.Default);
        HelperController.GameController.isPlayable = false;
        agentAnimParamStartId = Animator.StringToHash(agentAnimParamStart);
        speechBubble.SetActive(false);
        IntroInteractionCoroutine = StartCoroutine(StartSpeakInteractionCoroutine());
        TaskGenerate();
        forklifts[0].gameObject.transform.parent.gameObject.SetActive(false);
        testControlDevice.SetActive(false);

    }

    #region intro
    private IEnumerator StartSpeakInteractionCoroutine()
    {
        agentAnim.gameObject.SetActive(true);
        speechBubble.SetActive(true);
        agentAnim.SetBool(agentAnimParamStartId, true);
        HelperController.GameController.isPlayable = false;

        speechBubbleText.text = "3 tip motora ihtiyacımız var. Bunlar \"FIRLATMA MOTORU\", \"MEKİK MOTORU\",\"MANEVRA MOTORU\". Elimizdeki motorları inceleyip hangi motorun hangi amaçla kullanılacağına karar vermen gerekiyor. ";
        yield return new WaitWhile(() => speechBubble.activeSelf); //yield return new WaitForSeconds(HelperController.SpeechbublpeWaitTime(speechBubbleText.text, textChangeSpeed));
        //yield return new WaitForSeconds(speechBubbleText.text.Length * Time.deltaTime / textChangeSpeed);

        speechBubble.SetActive(false);
        yield return new WaitForSeconds(0.1f);
        speechBubble.SetActive(true);

        speechBubbleText.text = "Bunun için çeşitli testler yapıp sonuçlarını tabletini kullanarak rapora işlemelisin. Bu testler sana görev olarak bildirilecek. Kolay gelsin.";
        yield return new WaitWhile(() => speechBubble.activeSelf); //yield return new WaitForSeconds(HelperController.SpeechbublpeWaitTime(speechBubbleText.text, textChangeSpeed));
        //yield return new WaitForSeconds(speechBubbleText.text.Length * Time.deltaTime / textChangeSpeed);
        agentAnim.SetBool(agentAnimParamStartId, false);
        speechBubble.SetActive(false);
        IntroPassBtn.gameObject.SetActive(false);
        HelperController.GameController.isPlayable = true;
        StartCoroutine(SpeakInteractionCoroutine());

    }
    private IEnumerator SpeakInteractionCoroutine()
    {
        yield return new WaitForSeconds(0.1f);
        speechBubble.SetActive(true);
        agentAnim.SetBool(agentAnimParamStartId, true);
        HelperController.GameController.isPlayable = false;
        speechBubbleText.text = "Testleri gerçekleştirmek için önce motorları test araçlarına yüklemelisin. Bunun için motor gruplarını taşıyan taşıyıcılara tıklaman gerekiyor. Test esnasında ne yapman gerektiği ekranın aşağısında sürekli yazacak oraya dikkat et.";
        yield return new WaitWhile(() => speechBubble.activeSelf); //yield return new WaitForSeconds(HelperController.SpeechbublpeWaitTime(speechBubbleText.text, textChangeSpeed));

        // yield return new WaitForSeconds(speechBubbleText.text.Length * Time.deltaTime / textChangeSpeed);
        agentAnim.SetBool(agentAnimParamStartId, false);
        speechBubble.SetActive(false);
        agentAnim.gameObject.SetActive(false);
        HelperController.GameController.isPlayable = true;
        yield return new WaitForSeconds(0.5f);
        testControlDevice.SetActive(true);
        forklifts[0].gameObject.transform.parent.gameObject.SetActive(true);
        forklifts[0].ChangeStatus(ForklitStatus.Passive);
        forklifts[1].ChangeStatus(ForklitStatus.Passive);
        forklifts[2].ChangeStatus(ForklitStatus.Passive);
        tabletController.SwitcOn(TabletScreens.Activity1Scene1);
        NextTask();
    }
    public void OnClickPassIntro()
    {

        agentAnim.SetBool(agentAnimParamStartId, false);
        speechBubble.SetActive(false);
        IntroPassBtn.gameObject.SetActive(false);
        HelperController.GameController.isPlayable = true;
        IntroPassBtn.gameObject.SetActive(false);
        StopCoroutine(IntroInteractionCoroutine);
        StartCoroutine(SpeakInteractionCoroutine());
    }
    #endregion

    #region TaskOps
    private void NextTask()
    {
        tabletController.gameObject.SetActive(false);
        StopAllCoroutines();
        if (activeTaskID + 1 < taskList.Count)
        {
            activeTaskID++;
            //  StartCoroutine(NextTaskInteraction());
            sceneMessageController.ShowMessage(HelperController.MessageType.Task, activeTaskID, HelperController.ButtonType.ClosePanel, true);
        }
        else
        {
            NextSection();
        }
    }

    private IEnumerator NextTaskInteraction()
    {
        switch (activeTaskID)
        {
            case 0:
                break;
            case 1:
                break;
            case 2:
                break;
            case 3:
                break;
            case 4:
                break;
            case 5:
                break;
            default:
                break;
        }
        yield return new WaitForEndOfFrame();
    }


    public void TaskStarted()
    {

        HelperController.GameController.Playable(true);
        StartCoroutine(TaskProcess());
    }

    private IEnumerator TaskProcess()
    {
        forklifts[0].gameObject.SetActive(true);
        forklifts[1].gameObject.SetActive(true);
        forklifts[2].gameObject.SetActive(true);
        isTesting = false;

        switch (activeTaskID)
        {
            case 0:
                #region A grubu Gözlem 1
                TestStartBtn.onClick.RemoveAllListeners();
                forklifts[0].ChangeStatus(ForklitStatus.Active);
                forklifts[1].ChangeStatus(ForklitStatus.Passive);
                forklifts[2].ChangeStatus(ForklitStatus.Passive);
                sceneMessageController.ShowMessage(HelperController.MessageType.Task, 0.1f, HelperController.ButtonType.ClosePanel, false);
                yield return new WaitUntil(() => MotorsGroupA[0].activeInHierarchy);
                SetActiveMotors('a');
                TestStartBtn.onClick.AddListener(delegate
                {
                    if (HelperController.GameController.isPlayable)
                    {
                        isTesting = false;
                        StartCoroutine(TestStart('a'));
                        TestStartBtn.interactable = false;
                    }

                });
                sceneMessageController.ShowMessage(HelperController.MessageType.Task, 0.2f, HelperController.ButtonType.ClosePanel, false);

                yield return new WaitUntil(() => isTesting);

                while (isTesting)
                {
                    //Debug.Log("0:"+MotorsGroupA[0].transform.parent.GetComponent<Rigidbody2D>().IsSleeping());
                    //Debug.Log("1:" + MotorsGroupA[1].transform.parent.GetComponent<Rigidbody2D>().IsSleeping());
                    //Debug.Log("1:" + MotorsGroupA[2].transform.parent.GetComponent<Rigidbody2D>().IsSleeping());

                    if (MotorsGroupA[0].transform.parent.GetComponent<Rigidbody2D>().IsSleeping() && MotorsGroupA[1].transform.parent.GetComponent<Rigidbody2D>().IsSleeping() && MotorsGroupA[2].transform.parent.GetComponent<Rigidbody2D>().IsSleeping())
                    {
                        isTesting = false;
                        HelperController.GameController.isPlayable = true;
                    }
                    yield return new WaitForEndOfFrame();
                }
                yield return new WaitForSeconds(1);
                tabletController.gameObject.SetActive(true);
                tabletController.activity1scene1Controller.EnableMenuButton(0);
                tabletController.activity1scene1Controller.SwitchOnTaskPanel(1);

                sceneMessageController.ShowMessage(HelperController.MessageType.Task, 0.3f, HelperController.ButtonType.ClosePanel, false);
                #endregion
                break;
            case 1:
                #region A grubu Gözlem 2

                TestStartBtn.onClick.RemoveAllListeners();
                TestStartBtn.interactable = true;
                Vector3 tempPos = MotorsGroupA[0].transform.parent.localPosition;
                tempPos.x = -2;
                MotorsGroupA[0].transform.parent.localPosition = tempPos;
                tempPos = MotorsGroupA[1].transform.parent.localPosition;
                tempPos.x = 1f;
                MotorsGroupA[1].transform.parent.localPosition = tempPos;
                tempPos = MotorsGroupA[2].transform.parent.localPosition;
                tempPos.x = -5.6f;
                MotorsGroupA[2].transform.parent.localPosition = tempPos;
                MotorsGroupA[0].transform.parent.GetComponent<CrawlerController>().SetCurrentPosAsInitialPos();
                MotorsGroupA[1].transform.parent.GetComponent<CrawlerController>().SetCurrentPosAsInitialPos();
                MotorsGroupA[2].transform.parent.GetComponent<CrawlerController>().SetCurrentPosAsInitialPos();

                forklifts[0].ChangeStatus(ForklitStatus.Active);
                forklifts[1].ChangeStatus(ForklitStatus.Passive);
                forklifts[2].ChangeStatus(ForklitStatus.Passive);
                sceneMessageController.ShowMessage(HelperController.MessageType.Task, 1.1f, HelperController.ButtonType.ClosePanel, false);
                yield return new WaitUntil(() => MotorsGroupA[0].activeInHierarchy);
                SetActiveMotors('a');
                TestStartBtn.onClick.AddListener(delegate
                {
                    if (HelperController.GameController.isPlayable)
                    {
                        isTesting = false;
                        StartCoroutine(TestStart('a'));
                        TestStartBtn.interactable = false;

                    }

                });
                sceneMessageController.ShowMessage(HelperController.MessageType.Task, 1.2f, HelperController.ButtonType.ClosePanel, false);

                yield return new WaitUntil(() => isTesting);

                while (isTesting)
                {
                    //Debug.Log("0:"+MotorsGroupA[0].transform.parent.GetComponent<Rigidbody2D>().IsSleeping());
                    //Debug.Log("1:" + MotorsGroupA[1].transform.parent.GetComponent<Rigidbody2D>().IsSleeping());
                    //Debug.Log("1:" + MotorsGroupA[2].transform.parent.GetComponent<Rigidbody2D>().IsSleeping());

                    if (MotorsGroupA[0].transform.parent.GetComponent<Rigidbody2D>().IsSleeping() && MotorsGroupA[1].transform.parent.GetComponent<Rigidbody2D>().IsSleeping() && MotorsGroupA[2].transform.parent.GetComponent<Rigidbody2D>().IsSleeping())
                    {
                        isTesting = false;
                        HelperController.GameController.isPlayable = true;
                    }
                    yield return new WaitForEndOfFrame();
                }
                yield return new WaitForSeconds(1);
                tabletController.gameObject.SetActive(true);
                tabletController.activity1scene1Controller.EnableMenuButton(1);
                tabletController.activity1scene1Controller.SwitchOnTaskPanel(2);

                sceneMessageController.ShowMessage(HelperController.MessageType.Task, 1.3f, HelperController.ButtonType.ClosePanel, false);
                #endregion
                break;
            case 2:
                #region B grubu Gözlem 1
                TestStartBtn.interactable = true;

                TestStartBtn.onClick.RemoveAllListeners();
                tempPos = MotorsGroupB[0].transform.parent.localPosition;
                tempPos.x = Motor1InitialPosX;
                MotorsGroupB[0].transform.parent.localPosition = tempPos;
                tempPos = MotorsGroupB[1].transform.parent.localPosition;
                tempPos.x = Motor2InitialPosX;
                MotorsGroupB[1].transform.parent.localPosition = tempPos;
                tempPos = MotorsGroupB[2].transform.parent.localPosition;
                tempPos.x = Motor3InitialPosX;
                MotorsGroupB[2].transform.parent.localPosition = tempPos;
                MotorsGroupB[0].transform.parent.GetComponent<CrawlerController>().SetCurrentPosAsInitialPos();
                MotorsGroupB[1].transform.parent.GetComponent<CrawlerController>().SetCurrentPosAsInitialPos();
                MotorsGroupB[2].transform.parent.GetComponent<CrawlerController>().SetCurrentPosAsInitialPos();

                forklifts[0].ChangeStatus(ForklitStatus.Done);
                forklifts[1].ChangeStatus(ForklitStatus.Active);
                forklifts[2].ChangeStatus(ForklitStatus.Passive);
                sceneMessageController.ShowMessage(HelperController.MessageType.Task, 2.1f, HelperController.ButtonType.ClosePanel, false);
                yield return new WaitUntil(() => MotorsGroupB[0].activeInHierarchy);
                SetActiveMotors('b');
                TestStartBtn.onClick.AddListener(delegate
                {
                    if (HelperController.GameController.isPlayable)
                    {
                        isTesting = false;
                        StartCoroutine(TestStart('b'));
                        TestStartBtn.interactable = false;

                    }

                });
                sceneMessageController.ShowMessage(HelperController.MessageType.Task, 2.2f, HelperController.ButtonType.ClosePanel, false);

                yield return new WaitUntil(() => isTesting);

                while (isTesting)
                {
                    //Debug.Log("0:"+MotorsGroupB[0].transform.parent.GetComponent<Rigidbody2D>().IsSleeping());
                    //Debug.Log("1:" + MotorsGroupB[1].transform.parent.GetComponent<Rigidbody2D>().IsSleeping());
                    //Debug.Log("1:" + MotorsGroupB[2].transform.parent.GetComponent<Rigidbody2D>().IsSleeping());

                    if (MotorsGroupB[0].transform.parent.GetComponent<Rigidbody2D>().IsSleeping() && MotorsGroupB[1].transform.parent.GetComponent<Rigidbody2D>().IsSleeping() && MotorsGroupB[2].transform.parent.GetComponent<Rigidbody2D>().IsSleeping())
                    {
                        isTesting = false;
                        HelperController.GameController.isPlayable = true;
                    }
                    yield return new WaitForEndOfFrame();
                }
                yield return new WaitForSeconds(1);
                tabletController.gameObject.SetActive(true);
                tabletController.activity1scene1Controller.EnableMenuButton(2);
                tabletController.activity1scene1Controller.SwitchOnTaskPanel(3);

                sceneMessageController.ShowMessage(HelperController.MessageType.Task, 2.3f, HelperController.ButtonType.ClosePanel, false);
                #endregion
                break;
            case 3:
                #region B grubu Gözlem 2

                TestStartBtn.interactable = true;

                TestStartBtn.onClick.RemoveAllListeners();
                tempPos = MotorsGroupB[0].transform.parent.localPosition;
                tempPos.x = -2;
                MotorsGroupB[0].transform.parent.localPosition = tempPos;
                tempPos = MotorsGroupB[1].transform.parent.localPosition;
                tempPos.x = -5.6f;
                MotorsGroupB[1].transform.parent.localPosition = tempPos;
                tempPos = MotorsGroupB[2].transform.parent.localPosition;
                tempPos.x = 1f;
                MotorsGroupB[2].transform.parent.localPosition = tempPos;
                MotorsGroupB[0].transform.parent.GetComponent<CrawlerController>().SetCurrentPosAsInitialPos();
                MotorsGroupB[1].transform.parent.GetComponent<CrawlerController>().SetCurrentPosAsInitialPos();
                MotorsGroupB[2].transform.parent.GetComponent<CrawlerController>().SetCurrentPosAsInitialPos();

                forklifts[0].ChangeStatus(ForklitStatus.Done);
                forklifts[1].ChangeStatus(ForklitStatus.Active);
                forklifts[2].ChangeStatus(ForklitStatus.Passive);
                sceneMessageController.ShowMessage(HelperController.MessageType.Task, 3.1f, HelperController.ButtonType.ClosePanel, false);
                yield return new WaitUntil(() => MotorsGroupB[0].activeInHierarchy);
                SetActiveMotors('b');
                TestStartBtn.onClick.AddListener(delegate
                {
                    if (HelperController.GameController.isPlayable)
                    {
                        isTesting = false;
                        StartCoroutine(TestStart('b'));
                        TestStartBtn.interactable = false;

                    }

                });
                sceneMessageController.ShowMessage(HelperController.MessageType.Task, 3.2f, HelperController.ButtonType.ClosePanel, false);

                yield return new WaitUntil(() => isTesting);

                while (isTesting)
                {
                    //Debug.Log("0:"+MotorsGroupB[0].transform.parent.GetComponent<Rigidbody2D>().IsSleeping());
                    //Debug.Log("1:" + MotorsGroupB[1].transform.parent.GetComponent<Rigidbody2D>().IsSleeping());
                    //Debug.Log("1:" + MotorsGroupB[2].transform.parent.GetComponent<Rigidbody2D>().IsSleeping());

                    if (MotorsGroupB[0].transform.parent.GetComponent<Rigidbody2D>().IsSleeping() && MotorsGroupB[1].transform.parent.GetComponent<Rigidbody2D>().IsSleeping() && MotorsGroupB[2].transform.parent.GetComponent<Rigidbody2D>().IsSleeping())
                    {
                        isTesting = false;
                        HelperController.GameController.isPlayable = true;
                    }
                    yield return new WaitForEndOfFrame();
                }
                yield return new WaitForSeconds(1);
                tabletController.gameObject.SetActive(true);
                tabletController.activity1scene1Controller.EnableMenuButton(3);
                tabletController.activity1scene1Controller.SwitchOnTaskPanel(4);

                sceneMessageController.ShowMessage(HelperController.MessageType.Task, 3.3f, HelperController.ButtonType.ClosePanel, false);
                #endregion
                break;
            case 4:
                #region C grubu Gözlem 1
                TestStartBtn.interactable = true;

                TestStartBtn.onClick.RemoveAllListeners();
                tempPos = MotorsGroupC[0].transform.parent.localPosition;
                tempPos.x = Motor1InitialPosX;
                MotorsGroupC[0].transform.parent.localPosition = tempPos;
                tempPos = MotorsGroupC[1].transform.parent.localPosition;
                tempPos.x = Motor2InitialPosX;
                MotorsGroupC[1].transform.parent.localPosition = tempPos;
                tempPos = MotorsGroupC[2].transform.parent.localPosition;
                tempPos.x = Motor3InitialPosX;
                MotorsGroupC[2].transform.parent.localPosition = tempPos;

                MotorsGroupC[0].transform.parent.GetComponent<CrawlerController>().SetCurrentPosAsInitialPos();
                MotorsGroupC[1].transform.parent.GetComponent<CrawlerController>().SetCurrentPosAsInitialPos();
                MotorsGroupC[2].transform.parent.GetComponent<CrawlerController>().SetCurrentPosAsInitialPos();

                forklifts[0].ChangeStatus(ForklitStatus.Done);
                forklifts[1].ChangeStatus(ForklitStatus.Done);
                forklifts[2].ChangeStatus(ForklitStatus.Active);
                sceneMessageController.ShowMessage(HelperController.MessageType.Task, 4.1f, HelperController.ButtonType.ClosePanel, false);
                yield return new WaitUntil(() => MotorsGroupC[0].activeInHierarchy);
                SetActiveMotors('c');
                TestStartBtn.onClick.AddListener(delegate
                {
                    if (HelperController.GameController.isPlayable)
                    {
                        isTesting = false;
                        StartCoroutine(TestStart('c'));
                        TestStartBtn.interactable = false;

                    }

                });
                sceneMessageController.ShowMessage(HelperController.MessageType.Task, 4.2f, HelperController.ButtonType.ClosePanel, false);

                yield return new WaitUntil(() => isTesting);

                while (isTesting)
                {
                    //Debug.Log("0:"+MotorsGroupC[0].transform.parent.GetComponent<Rigidbody2D>().IsSleeping());
                    //Debug.Log("1:" + MotorsGroupC[1].transform.parent.GetComponent<Rigidbody2D>().IsSleeping());
                    //Debug.Log("1:" + MotorsGroupC[2].transform.parent.GetComponent<Rigidbody2D>().IsSleeping());

                    if (MotorsGroupC[0].transform.parent.GetComponent<Rigidbody2D>().IsSleeping() && MotorsGroupC[1].transform.parent.GetComponent<Rigidbody2D>().IsSleeping() && MotorsGroupC[2].transform.parent.GetComponent<Rigidbody2D>().IsSleeping())
                    {
                        isTesting = false;
                        HelperController.GameController.isPlayable = true;
                    }
                    yield return new WaitForEndOfFrame();
                }
                yield return new WaitForSeconds(1);
                tabletController.gameObject.SetActive(true);
                tabletController.activity1scene1Controller.EnableMenuButton(4);
                tabletController.activity1scene1Controller.SwitchOnTaskPanel(5);

                sceneMessageController.ShowMessage(HelperController.MessageType.Task, 4.3f, HelperController.ButtonType.ClosePanel, false);
                #endregion
                break;
            case 5:
                #region C grubu Gözlem 2
                TestStartBtn.interactable = true;

                TestStartBtn.onClick.RemoveAllListeners();
                tempPos = MotorsGroupC[0].transform.parent.localPosition;
                tempPos.x = 1;
                MotorsGroupC[0].transform.parent.localPosition = tempPos;
                tempPos = MotorsGroupC[1].transform.parent.localPosition;
                tempPos.x = -5.6f;
                MotorsGroupC[1].transform.parent.localPosition = tempPos;
                tempPos = MotorsGroupC[2].transform.parent.localPosition;
                tempPos.x = -2f;
                MotorsGroupC[2].transform.parent.localPosition = tempPos;
                MotorsGroupC[0].transform.parent.GetComponent<CrawlerController>().SetCurrentPosAsInitialPos();
                MotorsGroupC[1].transform.parent.GetComponent<CrawlerController>().SetCurrentPosAsInitialPos();
                MotorsGroupC[2].transform.parent.GetComponent<CrawlerController>().SetCurrentPosAsInitialPos();

                forklifts[0].ChangeStatus(ForklitStatus.Done);
                forklifts[1].ChangeStatus(ForklitStatus.Done);
                forklifts[2].ChangeStatus(ForklitStatus.Active);
                sceneMessageController.ShowMessage(HelperController.MessageType.Task, 5.1f, HelperController.ButtonType.ClosePanel, false);
                yield return new WaitUntil(() => MotorsGroupC[0].activeInHierarchy);
                SetActiveMotors('c');
                TestStartBtn.onClick.AddListener(delegate
                {
                    if (HelperController.GameController.isPlayable)
                    {
                        isTesting = false;
                        StartCoroutine(TestStart('c'));
                        TestStartBtn.interactable = false;

                    }

                });
                sceneMessageController.ShowMessage(HelperController.MessageType.Task, 5.2f, HelperController.ButtonType.ClosePanel, false);

                yield return new WaitUntil(() => isTesting);

                while (isTesting)
                {
                    //Debug.Log("0:"+MotorsGroupC[0].transform.parent.GetComponent<Rigidbody2D>().IsSleeping());
                    //Debug.Log("1:" + MotorsGroupC[1].transform.parent.GetComponent<Rigidbody2D>().IsSleeping());
                    //Debug.Log("1:" + MotorsGroupC[2].transform.parent.GetComponent<Rigidbody2D>().IsSleeping());

                    if (MotorsGroupC[0].transform.parent.GetComponent<Rigidbody2D>().IsSleeping() && MotorsGroupC[1].transform.parent.GetComponent<Rigidbody2D>().IsSleeping() && MotorsGroupC[2].transform.parent.GetComponent<Rigidbody2D>().IsSleeping())
                    {
                        isTesting = false;
                        HelperController.GameController.isPlayable = true;
                    }
                    yield return new WaitForEndOfFrame();
                }
                yield return new WaitForSeconds(1);
                tabletController.gameObject.SetActive(true);
                tabletController.activity1scene1Controller.EnableMenuButton(5);
                tabletController.activity1scene1Controller.SwitchOnTaskPanel(6);

                sceneMessageController.ShowMessage(HelperController.MessageType.Task, 5.3f, HelperController.ButtonType.ClosePanel, false);
                #endregion
                break;
            case 6:
                #region Değerlendirme
                agentAnim.gameObject.SetActive(true);
                speechBubble.SetActive(true);
                agentAnim.SetBool(agentAnimParamStartId, true);
                HelperController.GameController.isPlayable = false;

                forklifts[0].transform.parent.gameObject.SetActive(false);
                MotorsGroupA[0].transform.gameObject.SetActive(false);
                tabletController.SwitcOn(TabletScreens.Activity1Scene1a);


                sceneMessageController.ShowMessage(HelperController.MessageType.Task, 6f, HelperController.ButtonType.ClosePanel, false);
                speechBubbleText.text = "Tüm gözlemleri tamamladın şimdi elimizdeki 9 motorun ne motoru olacağına karar vermemiz gerekiyor. Unutma En hızlı motorlarlar fırlatma motoru en yavaş motorlar Manevra motoru olacak. Orta hızlı olanlarda Mekik ana motoru olacak.";
                yield return new WaitWhile(() => speechBubble.activeSelf);

                sceneMessageController.ShowMessage(HelperController.MessageType.Task, 6.1f, HelperController.ButtonType.ClosePanel, false);
                speechBubbleText.text = "Tableti aç ve değerlendirmeyi yap.";
                yield return new WaitWhile(() => speechBubble.activeSelf);

                agentAnim.gameObject.SetActive(false);
                speechBubble.SetActive(false);
                agentAnim.SetBool(agentAnimParamStartId, false);
                HelperController.GameController.isPlayable = true;

                yield return new WaitUntil(() => tabletController.gameObject.activeInHierarchy);

                sceneMessageController.ShowMessage(HelperController.MessageType.Task, 6.2f, HelperController.ButtonType.ClosePanel, false);
                #endregion
                break;
            default:
                break;
        }


        yield return new WaitForEndOfFrame();
    }

    private void TestSetup(char group)
    {

    }

    private IEnumerator TestStart(char gr)
    {
        forklifts[0].gameObject.SetActive(false);
        forklifts[1].gameObject.SetActive(false);
        forklifts[2].gameObject.SetActive(false);

        trafficLightController.CountDownForTest();
        yield return new WaitUntil(() => trafficLightController.countDownComplete);

        if (gr == 'A' || gr == 'a')
        {
            MotorsGroupA[0].transform.parent.GetComponent<CrawlerController>().AddForce(engineSpeed["A1"]);
            MotorsGroupA[1].transform.parent.GetComponent<CrawlerController>().AddForce(engineSpeed["A2"]);
            MotorsGroupA[2].transform.parent.GetComponent<CrawlerController>().AddForce(engineSpeed["A3"]);
        }
        if (gr == 'B' || gr == 'b')
        {
            MotorsGroupB[0].transform.parent.GetComponent<CrawlerController>().AddForce(engineSpeed["B1"]);
            MotorsGroupB[1].transform.parent.GetComponent<CrawlerController>().AddForce(engineSpeed["B2"]);
            MotorsGroupB[2].transform.parent.GetComponent<CrawlerController>().AddForce(engineSpeed["B3"]);
        }
        if (gr == 'C' || gr == 'c')
        {
            MotorsGroupC[0].transform.parent.GetComponent<CrawlerController>().AddForce(engineSpeed["C1"]);
            MotorsGroupC[1].transform.parent.GetComponent<CrawlerController>().AddForce(engineSpeed["C2"]);
            MotorsGroupC[2].transform.parent.GetComponent<CrawlerController>().AddForce(engineSpeed["C3"]);
        }

        isTesting = true;
        StartCoroutine(TestFinishWait());
    }

    private IEnumerator TestFinishWait()
    {
        while (isTesting)
        {
            //Debug.Log("0:"+MotorsGroupA[0].transform.parent.GetComponent<Rigidbody2D>().IsSleeping());
            //Debug.Log("1:" + MotorsGroupA[1].transform.parent.GetComponent<Rigidbody2D>().IsSleeping());
            //Debug.Log("1:" + MotorsGroupA[2].transform.parent.GetComponent<Rigidbody2D>().IsSleeping());

            if (MotorsGroupA[0].transform.parent.GetComponent<Rigidbody2D>().IsSleeping() && MotorsGroupA[1].transform.parent.GetComponent<Rigidbody2D>().IsSleeping() && MotorsGroupA[2].transform.parent.GetComponent<Rigidbody2D>().IsSleeping())
            {
                isTesting = false;
                HelperController.GameController.isPlayable = true;
            }
            yield return new WaitForEndOfFrame();
        }
    }

    public void TaskComleted()
    {
        HelperController.TaskController.TaskComplete(activeTaskID);
        //  tabletController.activity1scene1Controller.MenuButtonComplete(activeTaskID);
        sceneMessageController.ShowMessage(HelperController.MessageType.Success, 1, HelperController.ButtonType.ClosePanel, true);
    }
    public void RestartTask()
    {
        if (activeTaskID > -1)
        {
            StopAllCoroutines();
            activeTaskID--;
            NextTask();
        }

    }
    private void TaskGenerate()
    {
        taskList.Add(new TaskController.TaskStruct("A Grubunda ki motorlar için  1. gözlemi yap ve sonucu tablete gir."));
        taskList.Add(new TaskController.TaskStruct("A Grubunda ki motorlar için  2. gözlemi yap ve sonucu tablete gir."));
        taskList.Add(new TaskController.TaskStruct("B Grubunda ki motorlar için  1. gözlemi yap ve sonucu tablete gir."));
        taskList.Add(new TaskController.TaskStruct("B Grubunda ki motorlar için  2. gözlemi yap ve sonucu tablete gir."));
        taskList.Add(new TaskController.TaskStruct("C Grubunda ki motorlar için  1. gözlemi yap ve sonucu tablete gir."));
        taskList.Add(new TaskController.TaskStruct("C Grubunda ki motorlar için  2. gözlemi yap ve sonucu tablete gir."));
        taskList.Add(new TaskController.TaskStruct("Gözlem sonuçlarını değerlendir."));

        HelperController.TaskController.TaskListFillOnSceneStart(taskList);

    }
    #endregion
    #region SectionOps
    private void NextSection()
    {
        StartCoroutine(NextSectionProcess());

    }
    private IEnumerator NextSectionProcess()
    {
        agentAnim.gameObject.SetActive(true);
        speechBubble.SetActive(true);
        agentAnim.SetBool(agentAnimParamStartId, true);
        HelperController.GameController.isPlayable = false;
        speechBubbleText.text = "Elimizdeki 9 motoru sınıflandırdık. Artık elimizde 3 Fırlatma motorumuz 3 Mekik Ana motorumuz ve 3 tane de Manevra motorumuz var.Bunlardan birer tanesi mekiğe takılacak diğerleri yedek motor olacak.";

        yield return new WaitWhile(() => speechBubble.activeSelf);
        //yield return new WaitForSeconds(HelperController.SpeechbublpeWaitTime(speechBubbleText.text, textChangeSpeed));

        speechBubbleText.text = " Hangilerinin mekike takılacağına karar vermek için daha detaylı testler yapmamız gerkiyor.";
        yield return new WaitWhile(() => speechBubble.activeSelf);

        agentAnim.SetBool(agentAnimParamStartId, false);
        speechBubble.SetActive(false);
        agentAnim.transform.parent.gameObject.SetActive(false);
        HelperController.GameController.isPlayable = true;
        sceneMessageController.ShowMessage(HelperController.MessageType.Section, 1f, HelperController.ButtonType.ClosePanel, false);
        yield return new WaitForEndOfFrame();
    }
    public void NextSectionGo()
    {
        StopAllCoroutines();
        HelperController.GameController.sceneController.LoadNextScene();
    }
    #endregion

    #region AnswerOps
    public void CheckAnswer()
    {
        switch (activeTaskID)
        {
            #region Gözlemler
            case 0:
                if (TabletGroupA1.CheckAnswer())
                {
                    TaskComleted();
                    tabletController.activity1scene1Controller.MenuButtonComplete(0);
                    MotorsGroupA[0].SetActive(false);
                    MotorsGroupA[1].SetActive(false);
                    MotorsGroupA[2].SetActive(false);
                }
                else
                {
                    sceneMessageController.ShowMessage(HelperController.MessageType.Failure, 0, HelperController.ButtonType.ClosePanel, true);


                }
                break;
            case 1:
                if (TabletGroupA2.CheckAnswer())
                {
                    TaskComleted();
                    tabletController.activity1scene1Controller.MenuButtonComplete(1);
                    MotorsGroupA[0].SetActive(false);
                    MotorsGroupA[1].SetActive(false);
                    MotorsGroupA[2].SetActive(false);
                }
                else
                {
                    sceneMessageController.ShowMessage(HelperController.MessageType.Failure, 0, HelperController.ButtonType.ClosePanel, true);

                }
                break;
            case 2:
                if (TabletGroupB1.CheckAnswer())
                {
                    TaskComleted();
                    tabletController.activity1scene1Controller.MenuButtonComplete(2);

                    MotorsGroupB[0].SetActive(false);
                    MotorsGroupB[1].SetActive(false);
                    MotorsGroupB[2].SetActive(false);
                }
                else
                {
                    sceneMessageController.ShowMessage(HelperController.MessageType.Failure, 0, HelperController.ButtonType.ClosePanel, true);

                }
                break;
            case 3:
                if (TabletGroupB2.CheckAnswer())
                {
                    TaskComleted();
                    tabletController.activity1scene1Controller.MenuButtonComplete(3);

                    MotorsGroupB[0].SetActive(false);
                    MotorsGroupB[1].SetActive(false);
                    MotorsGroupB[2].SetActive(false);
                }
                else
                {
                    sceneMessageController.ShowMessage(HelperController.MessageType.Failure, 0, HelperController.ButtonType.ClosePanel, true);

                }
                break;
            case 4:
                if (TabletGroupC1.CheckAnswer())
                {
                    TaskComleted();
                    tabletController.activity1scene1Controller.MenuButtonComplete(4);

                    MotorsGroupC[0].SetActive(false);
                    MotorsGroupC[1].SetActive(false);
                    MotorsGroupC[2].SetActive(false);
                }
                else
                {
                    sceneMessageController.ShowMessage(HelperController.MessageType.Failure, 0, HelperController.ButtonType.ClosePanel, true);

                }
                break;
            case 5:
                if (TabletGroupC2.CheckAnswer())
                {
                    TaskComleted();
                    tabletController.activity1scene1Controller.MenuButtonComplete(5);

                    MotorsGroupC[0].SetActive(false);
                    MotorsGroupC[1].SetActive(false);
                    MotorsGroupC[2].SetActive(false);
                }
                else
                {
                    sceneMessageController.ShowMessage(HelperController.MessageType.Failure, 0, HelperController.ButtonType.ClosePanel, true);
                }
                break;
            #endregion
            case 6:
                if (tabletController.activity1Scene1aController.CheckAnswer())
                {
                    TaskComleted();
                }
                else
                {
                    sceneMessageController.ShowMessage(HelperController.MessageType.Failure, 0, HelperController.ButtonType.ClosePanel, true);
                }

                break;
            default:
                break;
        }
    }
    public void RestartMission()
    {
        switch (activeTaskID)
        {
            case 0:
                TabletGroupA1.ClearWrongAnswer();
                break;
            case 1:
                TabletGroupA2.ClearWrongAnswer();
                break;
            case 2:
                TabletGroupB1.ClearWrongAnswer();
                break;
            case 3:
                TabletGroupB2.ClearWrongAnswer();
                break;
            case 4:
                TabletGroupC1.ClearWrongAnswer();
                break;
            case 5:
                TabletGroupC2.ClearWrongAnswer();
                break;
            default:
                break;
        }
    }
    #endregion





    public void SetActiveMotors(char group)
    {
        for (int i = 0; i < MotorsGroupA.Count; i++)
        {
            MotorsGroupA[i].SetActive(false);
            MotorsGroupB[i].SetActive(false);
            MotorsGroupC[i].SetActive(false);
        }
        if (group == 'A' || group == 'a')
        {
            foreach (GameObject item in MotorsGroupA)
            {
                item.SetActive(true);
                item.transform.parent.GetComponent<CrawlerController>().SetCurrentPosAsInitialPos();
            }
        }
        if (group == 'B' || group == 'b')
        {
            foreach (GameObject item in MotorsGroupB)
            {
                item.SetActive(true);
                item.transform.parent.GetComponent<CrawlerController>().SetCurrentPosAsInitialPos();

            }
        }
        if (group == 'C' || group == 'c')
        {
            foreach (GameObject item in MotorsGroupC)
            {
                item.SetActive(true);
                item.transform.parent.GetComponent<CrawlerController>().SetCurrentPosAsInitialPos();

            }
        }
        trafficLightController.BlinkRedToGreen(false, 0);
        trafficLightController.SwitchRed();
    }



    #region list

    public Dictionary<string, float> engineSpeed = new Dictionary<string, float>()
    {
        {"A1",0.9f },
        {"A2",0.3f },
        {"A3",1.3f },
        {"B1",0.7f },
        {"B2",1.1f },
        {"B3",0.4f },
        {"C1",0.2f },
        {"C2",1.5f },
        {"C3",0.6f },

    };
    #endregion

}
[Serializable]
public class Activity1TabletGroupReportDragDrop
{
    public GameObject[] DropSlot;

    public GameObject[] item;
    public int[] TrueAnswerMatrix;
    public GameObject[] initialDropSlot;

    public bool CheckAnswer()
    {
        bool response = true;

        for (int i = 0; i < DropSlot.Length; i++)
        {
            if (DropSlot[i].transform != item[TrueAnswerMatrix[i]].transform.parent)
            {
                response = false;
            }
        }
        return response;
    }
    public void ClearWrongAnswer()
    {
        for (int i = 0; i < DropSlot.Length; i++)
        {
            if (DropSlot[i].transform != item[TrueAnswerMatrix[i]].transform.parent)
            {
                item[TrueAnswerMatrix[i]].transform.parent = initialDropSlot[i].transform;
            }
        }
    }
    public void ResetSlots()
    {
        for (int i = 0; i < item.Length; i++)
        {
            item[i].transform.parent = initialDropSlot[i].transform;
        }
    }

}