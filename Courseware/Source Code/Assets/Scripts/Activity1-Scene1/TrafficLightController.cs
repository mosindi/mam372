﻿using UnityEngine;
using System.Collections;
using System;

public class TrafficLightController : MonoBehaviour
{

    public TrafficLightObject trafficLight;
    [Range(0.0f,3.0f)]
    public float defaultBlinkDelayTime = 0.4f;
    [Range(0.0f, 3.0f)]
    public float countdownDelayTime = 0.7f;
    [HideInInspector]
    public bool countDownComplete = false;
    
    private Coroutine activeBlinkRedToGreen;

    // Use this for initialization
    void Start()
    {
        BlinkRedToGreen(true,defaultBlinkDelayTime);
    }

    // Update is called once per frame
    void Update()
    {
       
    }
    public void SwitchOffLight()
    {
        trafficLight.LightRed.SetActive(false);
        trafficLight.LightYellow.SetActive(false);
        trafficLight.LightGreen.SetActive(false);
        trafficLight.LightDefault.SetActive(true);
    }
    public void SwitchRed()
    {
        trafficLight.LightRed.SetActive(true);
        trafficLight.LightYellow.SetActive(false);
        trafficLight.LightGreen.SetActive(false);
        trafficLight.LightDefault.SetActive(false);
    }
    public void SwitchYellow()
    {
        trafficLight.LightRed.SetActive(false);
        trafficLight.LightYellow.SetActive(true);
        trafficLight.LightGreen.SetActive(false);
        trafficLight.LightDefault.SetActive(false);
    }
    public void SwitchGreen()
    {
        trafficLight.LightRed.SetActive(false);
        trafficLight.LightYellow.SetActive(false);
        trafficLight.LightGreen.SetActive(true);
        trafficLight.LightDefault.SetActive(false);
    }
    public void BlinkRedToGreen(bool active,float delayTime)
    {
        if (active)
            activeBlinkRedToGreen = StartCoroutine(BlinkRedToGreenCoroutine(delayTime));
        else
            StopCoroutine(activeBlinkRedToGreen);
    }

    private IEnumerator BlinkRedToGreenCoroutine(float delayTime)
    {
        while (true)
        {
            SwitchRed();
            yield return new WaitForSeconds(delayTime);
            SwitchGreen();
            yield return new WaitForSeconds(delayTime);
        }
    }

   public  void CountDownForTest()
    {
        countDownComplete = false;
        StartCoroutine(CountDownForTestCoroutine());
    }

    private IEnumerator CountDownForTestCoroutine()
    {
        SwitchRed();
        yield return new WaitForSeconds(countdownDelayTime);
        SwitchYellow();
        yield return new WaitForSeconds(countdownDelayTime);
        SwitchGreen();
        countDownComplete = true;
    }
}
[Serializable]
public class TrafficLightObject
{
    public GameObject LightRed;
    public GameObject LightYellow;
    public GameObject LightGreen;
    public GameObject LightDefault;
}
