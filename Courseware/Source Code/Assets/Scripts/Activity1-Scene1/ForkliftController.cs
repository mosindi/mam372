﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ForkliftController : MonoBehaviour
{

    public char Group;
    public Activity1Scene1Controller ac;
    public Texture2D mouseHandCursor;
    public ForklitStatus initialstatus;
    [HideInInspector]
    public ForklitStatus status;
   
    // Use this for initialization
    void Awake()
    {
        ChangeStatus(initialstatus);
        //status = init
    }

    // Update is called once per frame
    void Update()
    {

    }
    void OnMouseEnter()
    {
        if (status == ForklitStatus.Active && HelperController.GameController.isPlayable )
        {
            Vector2 hotspot = new Vector2(Input.mousePosition.x, Input.mousePosition.y);
            gameObject.transform.localScale = new Vector3(0.7f, 0.7f, 0.7f);
         //   Cursor.SetCursor(mouseHandCursor, hotspot, CursorMode.Auto);
        }
    }
    void OnMouseExit()
    {
        if (status == ForklitStatus.Active && HelperController.GameController.isPlayable)
        {
            Vector2 hotspot = new Vector2(Input.mousePosition.x, Input.mousePosition.y);
            gameObject.transform.localScale = new Vector3(0.6f, 0.6f, 0.6f);
          //  Cursor.SetCursor(null, Vector2.zero, CursorMode.Auto);
        }
    }
    void OnMouseDown()
    {
        if (status == ForklitStatus.Active && HelperController.GameController.isPlayable)
        {
          
                ac.SetActiveMotors(Group);
       
        }
    }
    public void ChangeStatus(ForklitStatus s)
    {
        //Debug.Log(ForklitColors[status]);
        gameObject.GetComponent<SpriteRenderer>().color = ForklitColors[s];
        status = s;


    }
   

    // Helpers
   
    public  Dictionary<ForklitStatus, Color> ForklitColors = new Dictionary<ForklitStatus, Color>()
    {
        {ForklitStatus.Active,new Color(255.0f, 255.0f, 255.0f)},
        {ForklitStatus.Passive,new Color(208, 208, 208, 0.5f)},
        {ForklitStatus.Done,new Color(0, 255.0f, 76.0f)}

    };





}
public enum ForklitStatus
{
    Active,
    Done,
    Passive
}