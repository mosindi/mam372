﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;

public class Activity1Scene1MessageController : MonoBehaviour
{
    public Text HudBottomMessageText;
    public HudMessageStruct taskMessagePanel;
    public HudMessageStruct successMessagePanel;
    public HudMessageStruct FailulerMessagePanel;

    // Use this for initialization
    void Start()
    {
        HudBottomMessageText.text = "";
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void ShowMessage(HelperController.MessageType mType, float id = 0, HelperController.ButtonType buttonEvent = HelperController.ButtonType.ClosePanel, bool notification = false)
    {

        switch (mType)
        {
            case HelperController.MessageType.Task:
                if (ActivityMessageListTask.Count > id)
                {
                 //   HelperController.GameController.isPlayable = false;
                    if (notification)
                    {
                        ShowNewTaskNotify((int)id);
                    }
                    HudBottomMessageText.text = ActivityMessageListTask[id];
                }

                break;
            case HelperController.MessageType.Warning:
                break;
            case HelperController.MessageType.Success:
                ShowSuccessNotify((int)id);
                HudBottomMessageText.text = "";
                break;
            case HelperController.MessageType.Failure:
                ShowFaillureNotify((int)id);
                break;
            case HelperController.MessageType.Section:
                ShowSectionNotify((int)id);
                HudBottomMessageText.text = ActivityMessageListSection[id];
                break;
            default:
                break;
        }
        AddEventListenerToButton(mType, buttonEvent);

    }
    private void AddEventListenerToButton(HelperController.MessageType mType, HelperController.ButtonType buttonEvent = HelperController.ButtonType.ClosePanel)
    {
        switch (mType)
        {
            #region Task
            case HelperController.MessageType.Task:
                taskMessagePanel.Button.onClick.RemoveAllListeners();
                switch (buttonEvent)
                {
                    
                    case HelperController.ButtonType.ClosePanel:
                        taskMessagePanel.Button.onClick.AddListener(delegate
                        {
                            taskMessagePanel.Button.transform.parent.gameObject.SetActive(false);
                            HelperController.GameController.isPlayable = true;
                            gameObject.BroadcastMessage("TaskStarted");
                        });
                        break;
                    case HelperController.ButtonType.NextSection:
                        taskMessagePanel.Button.onClick.AddListener(delegate
                        {
                            taskMessagePanel.Button.transform.parent.gameObject.SetActive(false);
                            HelperController.GameController.isPlayable = true;
                            //  Debug.Log("Task Message Pane Button");
                            HelperController.GameController.sceneController.LoadNextScene();
                        });

                        break;
                    default:
                        break;
                }
                break;
            #endregion
            case HelperController.MessageType.Warning:
                break;
            case HelperController.MessageType.Success:
                successMessagePanel.Button.onClick.RemoveAllListeners();
                switch (buttonEvent)
                {
                    case HelperController.ButtonType.ClosePanel:
                        successMessagePanel.Button.onClick.AddListener(delegate
                        {
                            successMessagePanel.Button.transform.parent.gameObject.SetActive(false);
                            HelperController.GameController.isPlayable = true;
                            gameObject.BroadcastMessage("NextTask");
                        });
                        break;
                    case HelperController.ButtonType.NextSection:
                        successMessagePanel.Button.onClick.AddListener(delegate
                        {
                            successMessagePanel.Button.transform.parent.gameObject.SetActive(false);
                            HelperController.GameController.isPlayable = true;
                            HelperController.GameController.sceneController.LoadNextScene();
                        });

                        break;
                    default:
                        break;
                }
                break;
            case HelperController.MessageType.Failure:
                switch (buttonEvent)
                {
                    case HelperController.ButtonType.ClosePanel:
                        FailulerMessagePanel.Button.onClick.AddListener(delegate {
                            FailulerMessagePanel.Button.transform.parent.gameObject.SetActive(false);
                            HelperController.GameController.isPlayable = true;
                            BroadcastMessage("RestartMission");
                        });
                       
                        break;
                    case HelperController.ButtonType.HudToolBox:
                        break;
                    case HelperController.ButtonType.NextSection:
                        break;
                    default:
                        break;
                }

                break;
            case HelperController.MessageType.Section:
                taskMessagePanel.Button.onClick.RemoveAllListeners();
                switch (buttonEvent)
                {

                    case HelperController.ButtonType.ClosePanel:
                        taskMessagePanel.Button.onClick.AddListener(delegate
                        {
                            taskMessagePanel.Button.transform.parent.gameObject.SetActive(false);
                            HelperController.GameController.isPlayable = true;
                            gameObject.BroadcastMessage("NextSectionGo");
                        });
                        break;
                    case HelperController.ButtonType.NextSection:
                        taskMessagePanel.Button.onClick.AddListener(delegate
                        {
                            taskMessagePanel.Button.transform.parent.gameObject.SetActive(false);
                            HelperController.GameController.isPlayable = true;
                            //  Debug.Log("Task Message Pane Button");
                            HelperController.GameController.sceneController.LoadNextScene();
                        });

                        break;
                    default:
                        break;
                }
                break;
            default:
                break;
        }
    }

    private void ShowNewTaskNotify(int id)
    {
        taskMessagePanel.Button.transform.parent.gameObject.SetActive(true);
        taskMessagePanel.Text.text = ActivityMessageListTask[id];
        HelperController.GameController.isPlayable = false;

    }
    private void ShowSuccessNotify(int id)
    {
        successMessagePanel.Button.transform.parent.gameObject.SetActive(true);
        successMessagePanel.Text.text = ActivityMessageListSuccess[id];
        HelperController.GameController.isPlayable = false;

    }
    private void ShowFaillureNotify(int id)
    {
        FailulerMessagePanel.Button.transform.parent.gameObject.SetActive(true);
        FailulerMessagePanel.Text.text = ActivityMessageListFailure[id];
        HelperController.GameController.isPlayable = false;

    }
    private void ShowSectionNotify(int id)
    {
        taskMessagePanel.Button.transform.parent.gameObject.SetActive(true);
        taskMessagePanel.Text.text = ActivityMessageListSection[id];
        HelperController.GameController.isPlayable = false;
    }

    #region Lists
         #region Task
    public static Dictionary<float, string> ActivityMessageListTask = new Dictionary<float, string>()
    {
        {0.0f,"A grubu motorları için 1. gözlemi yap ve raporu doldur." },
        {0.1f,"Üzerinde A grubu yazan taşıyıcıya tıkla ve motorları test araçlarına yükle" },
        {0.2f,"Test kontrol ünitesinden testi başlat ve testi izle. Tekrar et'e basarak testi tekrarlayabilirsin \nSoru : Motorları hızlarına göre en hızlıdan en yavaşa sırala." },
        {0.3f,"Tableti aç ve teste göre soruya cevap vererek raporu doldur" },

        {1.0f,"A grubu için 2. gözlemi yap ve raporu doldur. Bu sefer başlangıç noktaları farklı bitiş noktaları aynı." },
        {1.1f,"Üzerinde A grubu yazan taşıyıcıya tıkla ve motorları test araçlarına yükle. Test araçları başlangıç noktalarına kendileri geçecek." },
        {1.2f,"Test kontrol ünitesinden testi başlat ve testi izle. \nSoru : Motorları hızlarına göre en hızlıdan en yavaşa sırala." },
        {1.3f,"Tableti aç ve teste göre soruya cevap vererek raporu doldur" },

        {2.0f,"B grubu motorları için ilk testi yap ve raporu doldur." },
        {2.1f,"Üzerinde B grubu yazan taşıyıcıya tıkla ve motorları test araçlarına yükle" },
        {2.2f,"Test kontrol ünitesinden testi başlat ve testi izle. \nSoru : Motorları hızlarına göre en hızlıdan en yavaşa sırala." },
        {2.3f,"Tableti aç ve teste göre soruya cevap vererek raporu doldur" },

        {3.0f,"B grubu motorları için ikinci testi yap ve raporu doldur." },
        {3.1f,"Üzerinde B grubu yazan taşıyıcıya tıkla ve motorları test araçlarına yükle" },
        {3.2f,"Test kontrol ünitesinden testi başlat ve testi izle. \nSoru : Motorları hızlarına göre en hızlıdan en yavaşa sırala." },
        {3.3f,"Tableti aç ve teste göre soruya cevap vererek raporu doldur" },

        {4.0f,"C grubu motorları için ilk testi yap ve raporu doldur." },
        {4.1f,"Üzerinde C grubu yazan taşıyıcıya tıkla ve motorları test araçlarına yükle" },
        {4.2f,"Test kontrol ünitesinden testi başlat ve testi izle. \nSoru : Motorları hızlarına göre en hızlıdan en yavaşa sırala." },
        {4.3f,"Tableti aç ve teste göre soruya cevap vererek raporu doldur" },

        {5.0f,"C grubu motorları için ikinci testi yap ve raporu doldur." },
        {5.1f,"Üzerinde C grubu yazan taşıyıcıya tıkla ve motorları test araçlarına yükle" },
        {5.2f,"Test kontrol ünitesinden testi başlat ve testi izle. \nSoru : Motorları hızlarına göre en hızlıdan en yavaşa sırala." },
        {5.3f,"Tableti aç ve teste göre soruya cevap vererek raporu doldur" },

         {6.0f,"Gözlem sonuçlarını değendir. Hangi motoru ne için kullanılacağına karar ver." },
         {6.1f,"Tablet aç ve sınıflandırmayı yap" },
         {6.2f,"En hızlılar fırlatma motoru, en yavaş olanlar ise manevra motoru olacak şekilde sınıflandır. Kontrol et düğmesine basarak sınıflandırmayı gönder." }

    };
    #endregion
    #region Success
    public static Dictionary<int, string> ActivityMessageListSuccess = new Dictionary<int, string>()
    {
        {0,"Doğru cevap tebrikler!" },
        {1,"Görevi başarı ile tamamladın, devam edebiliriz." },
        {2,"Tüm Görevleri başarı ile tamamladın, devam edebiliriz." },
    };
    #endregion
    #region Failure
    public static Dictionary<int, string> ActivityMessageListFailure = new Dictionary<int, string>()
    {
        {0,"Sıralaman doğru değil, daha dikkatli olmalısın! İstersen tekrar izle" },
        {1,"" },
        {2,"" },
    };
    #endregion
    #region Speech
    public static Dictionary<int, string> ActivityMessageListSpeech = new Dictionary<int, string>()
    {
        {0,"Merhaba" },
        {1,"" },
        {2,"" },
    };
    #endregion
    #region Section
    public static Dictionary<float, string> ActivityMessageListSection = new Dictionary<float, string>()
    {
        {0,""},
        {1,"Gözlemleri bitirdik şimdi motorları daha detaylı inceleyelim." }
    };
    #endregion
    #endregion

}
