﻿using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class MessageController : MonoBehaviour
{
    public GameObject Overlay;
    public MessagePanelStruct WarningPanel;
    public MessagePanelStruct PausePanel;
    public MessagePanelStruct CheatPanel;
    public MessagePanelStruct ResetPanel;
    public Dictionary<int, MessageStruct> MessageList;

    void Start()
    {
        MessageList = new Dictionary<int, MessageStruct>();
        GenerateMessages();
    }
    void Update()
    {
        if (!HelperController.GameController.GetComponent<LoaderController>().isSplash && SceneManager.GetActiveScene().name != HelperController.SceneNames[HelperController.Scenes.Loader])
        {
            if (Input.GetKey(KeyCode.LeftShift) || Input.GetKey(KeyCode.RightShift))
            {
                if (Input.GetKeyDown(KeyCode.C))
                {
                    ShowCheatPanel();
                }
            }
        } 
    }
    #region WarningPanel
    public void ShowWarning(int MessageCode, string AcceptButonEvent, string DeclineButonEvent = "HideWarning")
    {
        Overlay.SetActive(true);
      
        WarningPanel.Panel.gameObject.SetActive(true);
        WarningPanel.Header.text = MessageList[MessageCode].Header;
        WarningPanel.Text.text = MessageList[MessageCode].MessageText;
        WarningPanel.ButtonPrimaryText.text = MessageList[MessageCode].AcceptButtonText;
        WarningPanel.ButtonSecondaryText.text = MessageList[MessageCode].DeclineButtonText;

        WarningPanel.ButtonPrimary.onClick.RemoveAllListeners();
        WarningPanel.ButtonSecondary.onClick.RemoveAllListeners();

        WarningPanel.ButtonPrimary.onClick.AddListener(delegate
        {
            BroadcastMessage(AcceptButonEvent);
        });
        WarningPanel.ButtonSecondary.onClick.AddListener(delegate
        {
            BroadcastMessage(DeclineButonEvent);
        });
        HelperController.GameController.PauseGame(true, false);
    }

    public void HideWarning()
    {
        WarningPanel.Panel.gameObject.SetActive(false);
        Overlay.SetActive(false);
        HelperController.GameController.PauseGame(false, false);

    }
    #endregion


    #region PausePanel
    public void ShowPaused(bool status)
    {
        Overlay.SetActive(status);
        PausePanel.Panel.gameObject.SetActive(status);
        if (status)
        {
            PausePanel.ButtonPrimary.onClick.AddListener(delegate
            {
                HelperController.GameController.PauseGame(false,true);
            });
        }
       
    }
    #endregion

    #region resetPanel
    public void ShowResetWarn(bool status)
    {
        Overlay.SetActive(status);
        ResetPanel.Panel.gameObject.SetActive(status);
        if (status)
        {
            ResetPanel.ButtonPrimary.onClick.RemoveAllListeners();
            ResetPanel.ButtonSecondary.onClick.RemoveAllListeners();
            ResetPanel.Cancel.onClick.RemoveAllListeners();
            ResetPanel.ButtonPrimary.onClick.AddListener(delegate
            {

                GameObject.FindGameObjectWithTag(HelperController.Tags.SceneController.ToString()).BroadcastMessage("RestartTask", SendMessageOptions.DontRequireReceiver);
                ShowResetWarn(false);

            });
            ResetPanel.ButtonSecondary.onClick.AddListener(delegate
            {
                HelperController.GameController.sceneController.LoadScene(SceneManager.GetActiveScene().buildIndex);
                ShowResetWarn(false);

            });

            ResetPanel.Cancel.onClick.AddListener(delegate
            {
                ShowResetWarn(false);
            });
        }

    }
    #endregion

    void GenerateMessages()
    {
        MessageList.Add(900,
         new MessageStruct(900,
         MessageType.Warning,
         "Duraklat",
         "Oyun Duraklatıldı.",
         "Devam Et", "Hayır"));

        MessageList.Add(908,
            new MessageStruct(908,
            MessageType.Warning,
            "Dikkat",
            "Çıkmak istediğine emin misin? Çıkarsan tüm ilerlemen kaybolacak!",
            "Evet,\nçık!", "Hayır,\nçıkma"));

        MessageList.Add(909,
            new MessageStruct(909,
            MessageType.Warning,
            "Dikkat",
            "Oyundan tamamen çıkmak istediğine emin misin?",
            "Evet", "Hayır"));

       
    }

    #region Helper
    [Serializable]
    public struct MessagePanelStruct
    {
        public RectTransform Panel;
        public Text Header;
        public Text Text;
        public Button ButtonPrimary;
        public Text ButtonPrimaryText;
        public Button ButtonSecondary;
        public Text ButtonSecondaryText;
        public Button Cancel;
        public MessagePanelStruct(RectTransform _p, Text _h, Text _t, Button _b1, Text _b1t, Button _b2 = null, Text _b2t = null, Button _c=null)
        {
            Panel = _p;
            Header = _h;
            Text = _t;
            ButtonPrimary = _b1;
            ButtonPrimaryText = _b1t;
            ButtonSecondary = _b2;
            ButtonSecondaryText = _b2t;
            Cancel = _c;
        }
    }

    
    [Serializable]
    public struct MessageStruct
    {
        public int Code;
        public MessageType Type;
        public string Header;
        public string MessageText;
        public string AcceptButtonText;
        public string DeclineButtonText;
        public MessageStruct(int _c, MessageType _mType, string _h, string _t, string _a, string _d)
        {
            Code = _c;
            Type = _mType;
            Header = _h;
            MessageText = _t;
            AcceptButtonText = _a;
            DeclineButtonText = _d;
        }

    }

    public enum MessageType
    {
        System,
        Game,
        Error,
        Success,
        Alert,
        Warning
    }

    #endregion


    #region CheatPanel
    public void ShowCheatPanel()
    {

        bool status = !CheatPanel.Panel.gameObject.activeInHierarchy;
        CheatPanel.Panel.gameObject.SetActive(status);
        Overlay.SetActive(status);
        if (status)
        {
            CheatPanel.Header.text = "bu yaptığın hiç doğru değil!";
            CheatPanel.Text.text = null;
            Debug.Log(status);
         
            CheatPanel.ButtonPrimary.onClick.RemoveAllListeners();
            CheatPanel.ButtonPrimary.onClick.AddListener(()=>Cheat());
        }
    }

    private void Cheat()
    {

        string rawCheatCode = CheatPanel.Text.text;
        string[] EnteredCheatCode = CheatPanel.Text.text.Split(':');
        HelperController.CheatAction CheatAction = HelperController.CheatAction.invalid;
        for (int i = 1; i < HelperController.Cheats.Count; i++)
        {
            if (HelperController.Cheats[i].ToString() == EnteredCheatCode[0])
            {
                CheatAction = (HelperController.CheatAction)i;
            }
        }
        switch (CheatAction)
        {
            case HelperController.CheatAction.invalid:
                CheatPanel.Header.text = "Böyle bir hile yok!";
                break;
            case HelperController.CheatAction.jump:
                ShowCheatPanel();
                int id = Convert.ToInt32(EnteredCheatCode[1]);
                id = (id == 1) ? 0 : id;
                HelperController.GameController.sceneController.LoadScene((HelperController.Scenes)id);
                break;
            case HelperController.CheatAction.showanswer:
                break;
            default:
                break;
        }




    }
    #endregion
}
