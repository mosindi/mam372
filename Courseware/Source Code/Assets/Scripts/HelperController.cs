﻿using UnityEngine;
using System.Collections;
using System;
using UnityEngine.UI;
using System.Collections.Generic;

[Serializable]
public class HelperController
{

    #region Enums
    public enum Scenes
    {
        Splash,
        Loader,
        MainMenu,
        Intro_Scene1,
        Intro_Scene2,
        Activity1_Scene1,
        Activity2_Scene1,
        Activity3_Scene1,
        Activity4_Scene1,
        Activity4_Scene2,

    }


    public enum ButtonType
    {
        Start,
        Help,
        Quit,
        HudHelp,
        HudPause,
        HudQuit,
        HudTasks,
        ClosePanel,
        HudToolBox,
        NextSection,
        Continue,
        HudResetLevel

    }
    public enum Tags
    {
        GameController,
        HelpPanel,
        PanelManager,
        SceneController,
        TaskPanel,
        ToolDesk
    }
    public enum Panels
    {
        MainMenu,
        Help,
    }
    public enum CheatAction
    {
        invalid,
        jump,
        showanswer
    }

    public enum MessageType
    {
        Task,
        Warning,
        Success,
        Failure,
        Section
        
    }

    public enum EngineCode
    {
        FA3,    //Fırlatma A-3
        FB2,    //Fırlatma B-2
        FC2,    //Fırlatma C-2
        AA1,    //Mekik Ana A-1
        AB1,    //Mekik Ana B-1
        AC1,    //Mekik Ana C-1
        MA2,    //Manevra A-2
        MB3,    //Manevra B-3
        MC3     //Manevra C-3

    }

    
    #endregion

    #region Structs
    [Serializable]
    public struct MenuItem
    {
        public string Name;
        public ButtonType ButtonType;
        public Button Button;
        public MenuItem(string _n, ButtonType _m, Button _b)
        {
            Name = _n;
            ButtonType = _m;
            Button = _b;
        }

    }
    #endregion

    #region Dictionaries
    public static Dictionary<Scenes, string> SceneNames = new Dictionary<Scenes, string>()
    {
        {Scenes.Splash,"00-Splash" },
        {Scenes.Loader,"01-Loader" },
        {Scenes.MainMenu,"02-MainMenu" },
        {Scenes.Intro_Scene1,"03-Intro-Scene1" },
        {Scenes.Intro_Scene2,"03-Intro-Scene2" }

    };
    public static Dictionary<int, CheatAction> Cheats = new Dictionary<int, CheatAction>()
    {

        {1, CheatAction.jump},
        {2, CheatAction.showanswer }
    };
    #endregion

    #region properties
    public static GameController GameController
    {
        get
        {
            return GameObject.FindGameObjectWithTag(HelperController.Tags.GameController.ToString()).GetComponent<GameController>();
        }
    }
    public static PanelManager PanelManager
    {
        get
        {
            return GameObject.FindGameObjectWithTag(HelperController.Tags.PanelManager.ToString()).GetComponent<PanelManager>();
        }
    }
    public static TaskController TaskController
    {
        get
        {
            return GameObject.FindGameObjectWithTag(HelperController.Tags.TaskPanel.ToString()).GetComponentInChildren<TaskController>(true);
        }
    }
    public static HelpController HelpController
    {
        get
        {
            return GameObject.FindGameObjectWithTag(HelperController.Tags.HelpPanel.ToString()).GetComponentInChildren<HelpController>(true);
        }
    }
    #endregion
    #region methods
    public static GameController GetGC()
    {
        return GameObject.FindGameObjectWithTag(HelperController.Tags.GameController.ToString()).GetComponent<GameController>();
    }

    public static float SpeechbublpeWaitTime(string text, float secondByWord = 0.5f)
    {
        string[] strL = text.Split(' ');
        return (strL.Length * secondByWord);
        
    }
    #endregion
}


[Serializable]
public struct HudMessageStruct
{
    public Text Header;
    public Text Text;
    public Button Button;
    public Text ButtonLabel;

}
[Serializable]
public enum MeasurementToolType
{
    Speed,
    Distance,
    Time,
    None = -1,

}
