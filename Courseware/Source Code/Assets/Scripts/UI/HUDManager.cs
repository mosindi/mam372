﻿using UnityEngine;
using System.Collections;

public class HUDManager : MonoBehaviour
{
    GameController gc;
    // Use this for initialization
    void Start()
    {
        gc = HelperController.GetGC();
    }

    // Update is called once per frame
    void Update()
    {

    }
    public void PassIntro()
    {
        GameObject.FindGameObjectWithTag(HelperController.Tags.SceneController.ToString()).BroadcastMessage("OnClickPassIntro",SendMessageOptions.DontRequireReceiver);
    }
    public void OnClick(int param)
    {
        HelperController.ButtonType button = (HelperController.ButtonType)param;
        switch (button)
        {
            case HelperController.ButtonType.HudHelp:

                GameObject go = GameObject.FindGameObjectWithTag(HelperController.Tags.HelpPanel.ToString());
                Animator anim = go.GetComponentInChildren<Animator>(true);
                HelperController.PanelManager.OpenPanel(anim);

                break;
            case HelperController.ButtonType.HudPause:
                gc.PauseGame(true,true);
                break;
            case HelperController.ButtonType.HudQuit:
                gc.messageController.ShowWarning(908, "EndGame");
                break;
            case HelperController.ButtonType.HudTasks:
                GameObject goTask = GameObject.FindGameObjectWithTag(HelperController.Tags.TaskPanel.ToString());
                
                Animator animTask = goTask.GetComponentInChildren<Animator>(true);
                //animTask.gameObject.SetActive(true);

                HelperController.PanelManager.OpenPanel(animTask);
                break;
            case HelperController.ButtonType.HudToolBox:
                GameObject toolboxContainer = GameObject.Find("ToolBoxContainner");
                toolboxContainer.SetActive(true);
                break;
            case HelperController.ButtonType.HudResetLevel:
                gc.messageController.ShowResetWarn(true);
                break;
            default:
                break;
        }
    }
}
