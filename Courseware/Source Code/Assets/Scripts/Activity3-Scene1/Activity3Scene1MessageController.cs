﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;

public class Activity3Scene1MessageController : MonoBehaviour
{

    public Text HudBottomMessageText;
    public HudMessageStruct taskMessagePanel;
    public HudMessageStruct successMessagePanel;
    public HudMessageStruct FailulerMessagePanel;


    // Use this for initialization
    void Start()
    {
        HudBottomMessageText.text = "";
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void ShowMessage(HelperController.MessageType mType, float id = 0, HelperController.ButtonType buttonEvent = HelperController.ButtonType.ClosePanel, bool notification = false)
    {

        switch (mType)
        {
            case HelperController.MessageType.Task:
                if (ActivityMessageListTask.Count > id)
                {
                    //   HelperController.GameController.isPlayable = false;
                    if (notification)
                    {
                        ShowNewTaskNotify((int)id);
                    }
                    HudBottomMessageText.text = ActivityMessageListTask[id];
                }

                break;
            case HelperController.MessageType.Warning:
                break;
            case HelperController.MessageType.Success:
                ShowSuccessNotify((int)id);
                HudBottomMessageText.text = "";
                break;
            case HelperController.MessageType.Failure:
                ShowFaillureNotify((int)id);
                break;
            case HelperController.MessageType.Section:
                ShowSectionNotify((int)id);
                HudBottomMessageText.text = ActivityMessageListSection[id];
                break;
            default:
                break;
        }
        AddEventListenerToButton(mType, buttonEvent);

    }
    private void AddEventListenerToButton(HelperController.MessageType mType, HelperController.ButtonType buttonEvent = HelperController.ButtonType.ClosePanel)
    {
        switch (mType)
        {
            #region Task
            case HelperController.MessageType.Task:
                taskMessagePanel.Button.onClick.RemoveAllListeners();
                switch (buttonEvent)
                {

                    case HelperController.ButtonType.ClosePanel:
                        taskMessagePanel.Button.onClick.AddListener(delegate
                        {
                            taskMessagePanel.Button.transform.parent.gameObject.SetActive(false);
                            HelperController.GameController.isPlayable = true;
                            gameObject.BroadcastMessage("TaskStarted");
                        });
                        break;
                    case HelperController.ButtonType.NextSection:
                        taskMessagePanel.Button.onClick.AddListener(delegate
                        {
                            taskMessagePanel.Button.transform.parent.gameObject.SetActive(false);
                            HelperController.GameController.isPlayable = true;
                            //  Debug.Log("Task Message Pane Button");
                            HelperController.GameController.sceneController.LoadNextScene();
                        });

                        break;
                    case HelperController.ButtonType.Continue:
                        taskMessagePanel.Button.onClick.AddListener(delegate
                        {
                            taskMessagePanel.Button.transform.parent.gameObject.SetActive(false);
                            HelperController.GameController.isPlayable = true;
                            //   gameObject.BroadcastMessage("TaskStarted");
                        });
                        break;
                    default:
                        break;
                }
                break;
            #endregion
            case HelperController.MessageType.Warning:
                break;
            case HelperController.MessageType.Success:
                successMessagePanel.Button.onClick.RemoveAllListeners();
                switch (buttonEvent)
                {
                    case HelperController.ButtonType.ClosePanel:
                        successMessagePanel.Button.onClick.AddListener(delegate
                        {
                            successMessagePanel.Button.transform.parent.gameObject.SetActive(false);
                            HelperController.GameController.isPlayable = true;
                            gameObject.BroadcastMessage("NextTask");
                        });
                        break;
                    case HelperController.ButtonType.NextSection:
                        successMessagePanel.Button.onClick.AddListener(delegate
                        {
                            successMessagePanel.Button.transform.parent.gameObject.SetActive(false);
                            HelperController.GameController.isPlayable = true;
                            HelperController.GameController.sceneController.LoadNextScene();
                        });

                        break;
                    case HelperController.ButtonType.Continue:
                        successMessagePanel.Button.onClick.AddListener(delegate
                        {
                            successMessagePanel.Button.transform.parent.gameObject.SetActive(false);
                            HelperController.GameController.isPlayable = true;

                        });
                        break;
                    default:
                        break;
                }
                break;
            case HelperController.MessageType.Failure:
                switch (buttonEvent)
                {
                    case HelperController.ButtonType.ClosePanel:
                        FailulerMessagePanel.Button.onClick.AddListener(delegate
                        {
                            FailulerMessagePanel.Button.transform.parent.gameObject.SetActive(false);
                            HelperController.GameController.isPlayable = true;
                            //  BroadcastMessage("RestartMission");
                        });

                        break;
                    case HelperController.ButtonType.HudToolBox:
                        break;
                    case HelperController.ButtonType.NextSection:
                        break;
                    default:
                        break;
                }

                break;
            case HelperController.MessageType.Section:
                taskMessagePanel.Button.onClick.RemoveAllListeners();
                switch (buttonEvent)
                {

                    case HelperController.ButtonType.ClosePanel:
                        taskMessagePanel.Button.onClick.AddListener(delegate
                        {
                            taskMessagePanel.Button.transform.parent.gameObject.SetActive(false);
                            HelperController.GameController.isPlayable = true;
                            gameObject.BroadcastMessage("NextSectionGo");
                        });
                        break;
                    case HelperController.ButtonType.NextSection:
                        taskMessagePanel.Button.onClick.AddListener(delegate
                        {
                            taskMessagePanel.Button.transform.parent.gameObject.SetActive(false);
                            HelperController.GameController.isPlayable = true;
                            //  Debug.Log("Task Message Pane Button");
                            HelperController.GameController.sceneController.LoadNextScene();
                        });

                        break;
                    default:
                        break;
                }
                break;
            default:
                break;
        }
    }

    private void ShowNewTaskNotify(int id)
    {
        taskMessagePanel.Button.transform.parent.gameObject.SetActive(true);
        taskMessagePanel.Text.text = ActivityMessageListTask[id];
        HelperController.GameController.isPlayable = false;

    }
    private void ShowSuccessNotify(int id)
    {
        successMessagePanel.Button.transform.parent.gameObject.SetActive(true);
        successMessagePanel.Text.text = ActivityMessageListSuccess[id];
        HelperController.GameController.isPlayable = false;

    }
    private void ShowFaillureNotify(int id)
    {
        FailulerMessagePanel.Button.transform.parent.gameObject.SetActive(true);
        FailulerMessagePanel.Text.text = ActivityMessageListFailure[id];
        HelperController.GameController.isPlayable = false;

    }
    private void ShowSectionNotify(int id)
    {
        taskMessagePanel.Button.transform.parent.gameObject.SetActive(true);
        taskMessagePanel.Text.text = ActivityMessageListSection[id];
        HelperController.GameController.isPlayable = false;
    }
    #region Lists
    #region Task
    public static Dictionary<float, string> ActivityMessageListTask = new Dictionary<float, string>()
    {
         {0.0f,"Fırlatma motorlarının montajı için uygun fırlatma motorunu seç" },
         {0.1f,"Ekranın solundaki menuden fırlatma motoru düğmesine tıkla." },
         {0.2f,"Raporunu görmek istediğin motorun soldaki menuden seç." },
         {0.25f,"Dünya'dan çıkmak için en hızlı motoru seçmelisin. Uygun motoru \"Montaj'a Gönder\" düğmesine tıklayarak montajı başlat." },
         {0.3f,"Frılatma motorları montaj edilerek fırlatma modülü inşaa ediliyor." },

         {1.0f,"Mekik Ana motorlarının montajı için uygun Mekik Ana motorunu seç" },
         {1.1f,"Ekranın solundaki menuden Mekik Ana motoru düğmesine tıkla." },
         {1.2f,"Raporunu görmek istediğin motorun soldaki menuden seç." },
         {1.25f,"Uzayda Mekik motorları ile yola devam edeceksin.Çok hızlı olursa yakıt yetmez. Çok yavaş olursa da zamanında hedefe ulaşamaz.Uygun motoru \"Montaj'a Gönder\" düğmesine tıklayarak montajı başlat." },
         {1.3f,"Mekik Ana motoru montaj edilerek Mekik modülü inşaa ediliyor." },

         {2.0f,"Manevra motorlarının montajı için uygun manevra motorunu seç" },
         {2.1f,"Ekranın solundaki menuden manevra motoru düğmesine tıkla." },
         {2.2f,"Raporunu görmek istediğin motorun soldaki menuden seç." },
         {2.25f,"Manevra motorları oldukça yavaş olmalı ki hassas kontroller yapılabilmeli. Uygun motoru \"Montaj'a Gönder\" düğmesine tıklayarak montajı başlat." },
         {2.3f,"Manevra motorları montaj edilerek Mekik modülüne yerleştiriliyor." },


    };
    #endregion
    #region Success
    public static Dictionary<int, string> ActivityMessageListSuccess = new Dictionary<int, string>()
    {
        {0,"Doğru cevap tebrikler!" },
        {1,"Görevi başarı ile tamamladın, devam edebiliriz." },
        {2,"Tüm Görevleri başarı ile tamamladın, devam edebiliriz." },
    };
    #endregion
    #region Failure
    public static Dictionary<int, string> ActivityMessageListFailure = new Dictionary<int, string>()
    {
        {0,"Girdiğin değerler doğru değil. Daha dikkatli ol" },
        {1,"Uygun motoru seçmedin. Ekranın aşağısındaki yönergeye dikkat et. " },
        {2,"Fırlatma Motoru seçmelisin. Seçtiğin motor fırlatma motoru değil."},
        {3,"Mekik Ana Motoru seçmelisin. Seçtiğin motor mekik ana motoru değil."},
        {4,"Manevra Motoru seçmelisin. Seçtiğin motor manevra motoru değil."},

    };
    #endregion
    #region Speech
    public static Dictionary<int, string> ActivityMessageListSpeech = new Dictionary<int, string>()
    {
        {0,"Merhaba" },
        {1,"" },
        {2,"" },
    };
    #endregion
    #region Section
    public static Dictionary<float, string> ActivityMessageListSection = new Dictionary<float, string>()
    {
        {0,"" },
        {1,"Fırlatma sahasına git!"}
    };
    #endregion
    #endregion
}
