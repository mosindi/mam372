﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;
using System;

public class Activity3Scene1Controller : MonoBehaviour
{


    public TabletController tabletController;
    public GameObject speechBubble;
    public Text speechBubbleText;
    public Animator agentAnim;
    public GameObject ToolContainer;
    [Range(0, 10)]
    public float textChangeSpeed;
    public Button IntroPassBtn;
    public Animator[] ShuttlePartsAnims;
  
    Activity3Scene1MessageController sceneMessageController;
    Activty3Scene1PanelController panelController;
    private int agentAnimParamStartId;
    private int shuttleAnimParamBuildId;
    private int activeTaskID = -1;
    private string agentAnimParamStart = "Interaction";
    private string shuttleAnimParamBuild = "builded";
    private Coroutine IntroInteractionCoroutine;



    // Use this for initialization
    void Start()
    {
        TaskGenerate();
        sceneMessageController = GetComponent<Activity3Scene1MessageController>();
        panelController = GetComponent<Activty3Scene1PanelController>();
        HelperController.GameController.isPlayable = false;
        shuttleAnimParamBuildId = Animator.StringToHash(shuttleAnimParamBuild);
        agentAnimParamStartId = Animator.StringToHash(agentAnimParamStart);

        speechBubble.SetActive(false);
        IntroInteractionCoroutine = StartCoroutine(IntroSpeakInteractionCoroutine());
    }
    #region Intro
    private IEnumerator IntroSpeakInteractionCoroutine()
    {
        agentAnim.gameObject.SetActive(true);
        speechBubble.SetActive(true);
        agentAnim.SetBool(agentAnimParamStartId, true);
        HelperController.GameController.isPlayable = false;

        speechBubbleText.text = "Sıra geldi uçuş öncesi son görevimiz olan mekiğin inşasına.";
        yield return new WaitWhile(() => speechBubble.activeSelf);
       // yield return new WaitForSeconds(HelperController.SpeechbublpeWaitTime(speechBubbleText.text, textChangeSpeed));
        speechBubble.SetActive(false);
        yield return new WaitForSeconds(0.1f);
        speechBubble.SetActive(true);

        speechBubbleText.text = "Bunun için ekranın aşağısında yer alan yönergeleri takip etmen yeterli olacak.";
        yield return new WaitWhile(() => speechBubble.activeSelf);
        // yield return new WaitForSeconds(HelperController.SpeechbublpeWaitTime(speechBubbleText.text, textChangeSpeed));


        agentAnim.SetBool(agentAnimParamStartId, false);
        speechBubble.SetActive(false);
        IntroPassBtn.gameObject.SetActive(false);
        HelperController.GameController.isPlayable = true;
        StartCoroutine(SpeakInteractionCoroutine());
    }

    private IEnumerator SpeakInteractionCoroutine()
    {
        yield return new WaitForSeconds(0.1f);
        agentAnim.gameObject.SetActive(true);
        speechBubble.SetActive(true);
        agentAnim.SetBool(agentAnimParamStartId, true);
        HelperController.GameController.isPlayable = false;

        speechBubbleText.text = "Sırasıyla kullanılacak motorları seç ve motorun takılması için montaj işlemini başlat.";
        yield return new WaitWhile(() => speechBubble.activeSelf);
        // yield return new WaitForSeconds(HelperController.SpeechbublpeWaitTime(speechBubbleText.text, textChangeSpeed));


        agentAnim.SetBool(agentAnimParamStartId, false);
        speechBubble.SetActive(false);
        agentAnim.gameObject.SetActive(false);
        HelperController.GameController.isPlayable = true;
     
        NextTask();
    }
    public void OnClickPassIntro()
    {
        agentAnim.SetBool(agentAnimParamStartId, false);
        speechBubble.SetActive(false);
        agentAnim.gameObject.SetActive(false);
        HelperController.GameController.isPlayable = true;
        IntroPassBtn.gameObject.SetActive(false);
        StopCoroutine(IntroInteractionCoroutine);
        StartCoroutine(SpeakInteractionCoroutine());
    }
    #endregion
    #region Task
    private void NextTask()
    {
        StopAllCoroutines();
        if (activeTaskID + 1 < taskList.Count)
        {
            activeTaskID++;
            //  StartCoroutine(NextTaskInteraction());
            sceneMessageController.ShowMessage(HelperController.MessageType.Task, activeTaskID, HelperController.ButtonType.ClosePanel, true);
        }
        else
        {
            NextSection();
        }
    }
    public void TaskStarted()
    {

        HelperController.GameController.Playable(true);
        StartCoroutine(TaskProcess());
    }
    private IEnumerator TaskProcess()
    {
        switch (activeTaskID)
        {
            case 0:
                sceneMessageController.ShowMessage(HelperController.MessageType.Task, 0.1f, HelperController.ButtonType.ClosePanel, false);

                panelController.MainPanelButtonSetActive(0, true);
                panelController.MainPanelButtonSetActive(1, false);
                panelController.MainPanelButtonSetActive(2, false);

                panelController.OpenPanel((int)Activty3Scene1PanelController.PanelKeys.Main);
                panelController.AssemblyButton.onClick.RemoveAllListeners();
                panelController.AssemblyButton.onClick.AddListener(delegate
                {
                    Debug.Log("AssemblyButttonClicked");
                    CheckAnswer();
                });
               
                while (panelController.selectedPanel != Activty3Scene1PanelController.PanelKeys.Firlatma)
                {
                    yield return new WaitForEndOfFrame();
                }
                sceneMessageController.ShowMessage(HelperController.MessageType.Task, 0.2f, HelperController.ButtonType.ClosePanel, false);
                while (!panelController.isInfoPanelActive)
                {
                    yield return new WaitForEndOfFrame();
                }
                sceneMessageController.ShowMessage(HelperController.MessageType.Task, 0.25f, HelperController.ButtonType.ClosePanel, false);

                break;
            case 1:
                sceneMessageController.ShowMessage(HelperController.MessageType.Task, 1.1f, HelperController.ButtonType.ClosePanel, false);

                panelController.MainPanelButtonSetActive(0, true);
                panelController.MainPanelButtonSetActive(1, true);
                panelController.MainPanelButtonSetActive(2, false);

                panelController.OpenPanel((int)Activty3Scene1PanelController.PanelKeys.Main);
                panelController.AssemblyButton.onClick.RemoveAllListeners();
                panelController.AssemblyButton.onClick.AddListener(delegate
                {
                    Debug.Log("AssemblyButttonClicked");
                    CheckAnswer();
                });
                while (panelController.selectedPanel != Activty3Scene1PanelController.PanelKeys.Mekik)
                {
                    yield return new WaitForEndOfFrame();
                }
                sceneMessageController.ShowMessage(HelperController.MessageType.Task, 1.2f, HelperController.ButtonType.ClosePanel, false);
                while (!panelController.isInfoPanelActive)
                {
                    yield return new WaitForEndOfFrame();
                }
                sceneMessageController.ShowMessage(HelperController.MessageType.Task, 1.25f, HelperController.ButtonType.ClosePanel, false);

                break;
            case 2:
                sceneMessageController.ShowMessage(HelperController.MessageType.Task, 2.1f, HelperController.ButtonType.ClosePanel, false);

                panelController.MainPanelButtonSetActive(0, true);
                panelController.MainPanelButtonSetActive(1, true);
                panelController.MainPanelButtonSetActive(2, true);

                panelController.OpenPanel((int)Activty3Scene1PanelController.PanelKeys.Main);
                panelController.AssemblyButton.onClick.RemoveAllListeners();
                panelController.AssemblyButton.onClick.AddListener(delegate
                {
                    Debug.Log("AssemblyButttonClicked");
                    CheckAnswer();
                });

                while (panelController.selectedPanel != Activty3Scene1PanelController.PanelKeys.Manevra)
                {
                    yield return new WaitForEndOfFrame();
                }
                sceneMessageController.ShowMessage(HelperController.MessageType.Task, 2.2f, HelperController.ButtonType.ClosePanel, false);
                while (!panelController.isInfoPanelActive)
                {
                    yield return new WaitForEndOfFrame();
                }
                sceneMessageController.ShowMessage(HelperController.MessageType.Task, 2.25f, HelperController.ButtonType.ClosePanel, false);
                break;
            default:
                break;
        }
        yield return null;
    }

    private void CheckAnswer()
    {
        switch (activeTaskID)
        {
            case 0:
                if (panelController.SelectedAssemblyButton == HelperController.EngineCode.FC2)
                {
                    sceneMessageController.ShowMessage(HelperController.MessageType.Task, 0.3f, HelperController.ButtonType.ClosePanel, false);
                    StartCoroutine(AssemblyOperation());
                }
                else if (panelController.SelectedAssemblyButton == HelperController.EngineCode.FB2 || panelController.SelectedAssemblyButton == HelperController.EngineCode.FA3)
                {
                    sceneMessageController.ShowMessage(HelperController.MessageType.Failure, 1, HelperController.ButtonType.ClosePanel, true);
                }
                else
                {
                    sceneMessageController.ShowMessage(HelperController.MessageType.Failure, 2, HelperController.ButtonType.ClosePanel, true);
                }
                break;
            case 1:
                if (panelController.SelectedAssemblyButton == HelperController.EngineCode.AB1)
                {
                    sceneMessageController.ShowMessage(HelperController.MessageType.Task, 1.3f, HelperController.ButtonType.ClosePanel, false);
                    StartCoroutine(AssemblyOperation());
                }
                else if (panelController.SelectedAssemblyButton == HelperController.EngineCode.AA1 || panelController.SelectedAssemblyButton == HelperController.EngineCode.AC1)
                {
                    sceneMessageController.ShowMessage(HelperController.MessageType.Failure, 1, HelperController.ButtonType.ClosePanel, true);
                }
                else
                {
                    sceneMessageController.ShowMessage(HelperController.MessageType.Failure, 3, HelperController.ButtonType.ClosePanel, true);
                }
                break;
            case 2:
                if (panelController.SelectedAssemblyButton == HelperController.EngineCode.MA2)
                {
                    sceneMessageController.ShowMessage(HelperController.MessageType.Task, 2.3f, HelperController.ButtonType.ClosePanel, false);
                    StartCoroutine(AssemblyOperation());
                }
                else if (panelController.SelectedAssemblyButton == HelperController.EngineCode.MB3 || panelController.SelectedAssemblyButton == HelperController.EngineCode.MC3)
                {
                    sceneMessageController.ShowMessage(HelperController.MessageType.Failure, 1, HelperController.ButtonType.ClosePanel, true);
                }
                else
                {
                    sceneMessageController.ShowMessage(HelperController.MessageType.Failure, 4, HelperController.ButtonType.ClosePanel, true);
                }
                break;
            default:
                break;
        }
    }

    private IEnumerator AssemblyOperation()
    {
        panelController.OnClickBackButton();
        HelperController.GameController.isPlayable = false;
        if (activeTaskID == 0)
        {
            ShuttlePartsAnims[0].SetBool(shuttleAnimParamBuildId, true);
            Debug.Log("AnimStart:" + Time.time);
            yield return new WaitForSeconds(3);
            Debug.Log("AnimFinish-1:" + Time.time);

            ShuttlePartsAnims[1].SetBool(shuttleAnimParamBuildId, true);

        }
        else if (activeTaskID == 1)
        {
            ShuttlePartsAnims[2].SetBool(shuttleAnimParamBuildId, true);
        }
        else
        {
            ShuttlePartsAnims[3].SetBool(shuttleAnimParamBuildId, true);
        }


        Debug.Log("AnimFinish-2:" + Time.time);
        yield return new WaitForSeconds(3);

        HelperController.GameController.isPlayable = true;
        TaskComleted();


    }

    public void TaskComleted()
    {
        HelperController.TaskController.TaskComplete(activeTaskID);
        sceneMessageController.ShowMessage(HelperController.MessageType.Success, 1, HelperController.ButtonType.ClosePanel, true);
    }
    public void RestartTask()
    {
        if (activeTaskID > -1)
        {
            panelController.InfoPanelTemplate.Panel.gameObject.SetActive(false);
            StopAllCoroutines();
            activeTaskID--;
            NextTask();
        }

    }
    #endregion

    #region Section
    public void NextSection()
    {
        StopAllCoroutines();
        StartCoroutine(NextSectionProcess());
    }
    private IEnumerator NextSectionProcess()
    {
        agentAnim.gameObject.SetActive(true);
        speechBubble.SetActive(true);
        agentAnim.SetBool(agentAnimParamStartId, true);
        HelperController.GameController.isPlayable = false;

        speechBubbleText.text = "Mekik'in tüm modülleri inşa edildi. Artık Mars görevinin ilk aşaması olan Uluslararası Uzay İstasyonuna uçmaya hazır.";
        yield return new WaitWhile(() => speechBubble.activeSelf);
        // yield return new WaitForSeconds(HelperController.SpeechbublpeWaitTime(speechBubbleText.text, textChangeSpeed));
        speechBubble.SetActive(false);
        yield return new WaitForSeconds(0.1f);
        speechBubble.SetActive(true);
        speechBubbleText.text = "Uçus için fırlatma sahasına gitmemiz gerekiyor.";
        yield return new WaitWhile(() => speechBubble.activeSelf);
        // yield return new WaitForSeconds(HelperController.SpeechbublpeWaitTime(speechBubbleText.text, textChangeSpeed));

        agentAnim.SetBool(agentAnimParamStartId, false);
        speechBubble.SetActive(false);
        agentAnim.transform.parent.gameObject.SetActive(false);
        HelperController.GameController.isPlayable = true;
        sceneMessageController.ShowMessage(HelperController.MessageType.Section, 1f, HelperController.ButtonType.ClosePanel, false);
        yield return new WaitForEndOfFrame();
    }
    public void NextSectionGo()
    {
        StopAllCoroutines();
        HelperController.GameController.sceneController.LoadNextScene();
    }
    #endregion

    #region List
    public List<TaskController.TaskStruct> taskList = new List<TaskController.TaskStruct>();
    private void TaskGenerate()
    {
        taskList.Add(new TaskController.TaskStruct("Mekik'inşaası için Fırlatma Motorunu seç ve Fırlatma modülüne montaja gönder!"));
        taskList.Add(new TaskController.TaskStruct("Mekik'inşaası için Mekik Ana Motorunu seç ve Mekik modülüne montaja gönder"));
        taskList.Add(new TaskController.TaskStruct("Mekik'inşaası için Manevra motorunu seç ve Manevra motorları M-mekik modülüne montaja gönder."));
        HelperController.TaskController.TaskListFillOnSceneStart(taskList);

    }
    #endregion
    #region Helper
    public float SpeechbublpeWaitTime()
    {
        return (speechBubbleText.text.Length * Time.deltaTime / textChangeSpeed) + 1.5f;
    }
    #endregion
}
