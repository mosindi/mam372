﻿using UnityEngine;
using System.Collections;
using System;
using UnityEngine.UI;
using System.Collections.Generic;
using UnityEngine.EventSystems;

public class Activty3Scene1PanelController : MonoBehaviour
{
    public Button[] MainPanelButtons;

    public Animator[] PanelAnims;


    public Activity3Scene1InfoPanel InfoPanelTemplate;
    [SerializeField]
    public Dictionary<HelperController.EngineCode, Activity3Scene1InfoPanel> InfoPanels;

    public Button AssemblyButton;
    [HideInInspector]
    public HelperController.EngineCode SelectedAssemblyButton;
    [HideInInspector]
    public PanelKeys selectedPanel;
    [HideInInspector]
    public bool isInfoPanelActive;
    private int currentOpenId;
    private int m_OpenParameterId;
    const string k_OpenTransitionName = "Open";
    const string k_ClosedStateName = "Closed";



 //   PanelManager pm;

    void Start()
    {
        GenerateInfoPanels();
    }

    public void MainPanelButtonSetActive(int id, bool param)
    {
        MainPanelButtons[id].interactable = param;
    }

    public void OpenPanel(int panelId)
    {
        if (HelperController.GameController.isPlayable && !HelperController.GameController.isPaused)
        {
            
            OpenPanel(PanelAnims[panelId]);

            if (panelId != (int)PanelKeys.Main)
            {
                Button btn = GetBackButton(PanelAnims[panelId]);
                if (btn != null)
                {
                    btn.onClick.RemoveAllListeners();

                    btn.onClick.AddListener(delegate
                    {
                        OnClickBackButton();
                    });
                }
            }
            selectedPanel = (PanelKeys)panelId;
        }
    }
    public void OnClickBackButton()
    {
        CloseAllInfoPanel();
        //CloseCurrent();
        OpenPanel(PanelAnims[(int)PanelKeys.Main]);
    }
    private Button GetBackButton(Animator animator)
    {
        foreach (Button item in animator.gameObject.GetComponentsInChildren<Button>())
        {
            if (item.gameObject.name == "Back")
                return item;
        }
        return null;
    }

    public void OpenInfoPanel(int id)
    {
        if (HelperController.GameController.isPlayable && !HelperController.GameController.isPaused)
        {
            CloseAllInfoPanel();
            StartCoroutine(OpenInfoPanelCoroutine(id));
            
        }

         
    }

    private IEnumerator OpenInfoPanelCoroutine(int id)
    {
        yield return new WaitForSeconds(0.1f);
        InfoPanels[(HelperController.EngineCode)id].SetActive(true);
        SelectedAssemblyButton = (HelperController.EngineCode)id;
        isInfoPanelActive = true;
            
    }

    public void CloseAllInfoPanel()
    {
        foreach (Activity3Scene1InfoPanel item in InfoPanels.Values)
        {
            item.SetActive(false);
        }
        isInfoPanelActive = false;
    }
    void GenerateInfoPanels()
    {
        InfoPanels = new Dictionary<HelperController.EngineCode, Activity3Scene1InfoPanel>();

        for (int i = 0; i < 9; i++)
        {
            Activity3Scene1InfoPanel info = new Activity3Scene1InfoPanel();
            info.DistanceText = InfoPanelTemplate.DistanceText;
            info.TimeText = InfoPanelTemplate.TimeText;
            info.SpeedText = InfoPanelTemplate.SpeedText;
            info.Panel = InfoPanelTemplate.Panel;
            info.Header = InfoPanelTemplate.Header;
            info.SubmitBtn = InfoPanelTemplate.SubmitBtn;
            InfoPanels.Add((HelperController.EngineCode)i, info);
        }
        InfoPanels[HelperController.EngineCode.FA3].SetValue("Fırlatma Motoru A-3", 1500, 3000, 2);
        InfoPanels[HelperController.EngineCode.FB2].SetValue("Fırlatma Motoru B-2", 1000, 3000, 3);
        InfoPanels[HelperController.EngineCode.FC2].SetValue("Fırlatma Motoru C-2", 3000, 3000, 1);

        InfoPanels[HelperController.EngineCode.AA1].SetValue("Mekik Ana Motoru A-1", 600, 600, 1);
        InfoPanels[HelperController.EngineCode.AB1].SetValue("Mekik Ana Motoru B-1", 400, 600, 1.5f);
        InfoPanels[HelperController.EngineCode.AC1].SetValue("Mekik Ana Motoru C-1", 300, 600, 2);

        InfoPanels[HelperController.EngineCode.MA2].SetValue("Manevra Motoru A-2 ", 10, 600, 60);
        InfoPanels[HelperController.EngineCode.MB3].SetValue("Manevra Motoru B-3 ", 12, 600, 50);
        InfoPanels[HelperController.EngineCode.MC3].SetValue("Manevra Motoru C-3 ", 15, 600, 40);

    }

    private Animator m_Open;
    private GameObject m_PreviouslySelected;

    public void OpenPanel(Animator anim)
    {
        m_OpenParameterId = Animator.StringToHash(k_OpenTransitionName);
        if (m_Open == anim)
            return;

        anim.gameObject.SetActive(true);
        var newPreviouslySelected = EventSystem.current.currentSelectedGameObject;

        anim.transform.SetAsLastSibling();

        CloseCurrent();

        m_PreviouslySelected = newPreviouslySelected;

        m_Open = anim;
        m_Open.SetBool(m_OpenParameterId, true);

        /*
        GameObject go = FindFirstEnabledSelectable(anim.gameObject);
        SetSelected(go);
        */
    }
    public void CloseCurrent()
    {
        if (m_Open == null)
            return;

        m_Open.SetBool(m_OpenParameterId, false);
        SetSelected(m_PreviouslySelected);
        StartCoroutine(DisablePanelDeleyed(m_Open));
        m_Open = null;
    }
    private void SetSelected(GameObject go)
    {
        EventSystem.current.SetSelectedGameObject(go);
    }
    IEnumerator DisablePanelDeleyed(Animator anim)
    {
        bool closedStateReached = false;
        bool wantToClose = true;
        while (!closedStateReached && wantToClose)
        {
            if (!anim.IsInTransition(0))
                closedStateReached = anim.GetCurrentAnimatorStateInfo(0).IsName(k_ClosedStateName);

            wantToClose = !anim.GetBool(m_OpenParameterId);

            yield return new WaitForEndOfFrame();
        }

        if (wantToClose)
            anim.gameObject.SetActive(false);
    }

    #region Helper
    public enum PanelKeys
    {
        Main,
        Firlatma,
        Mekik,
        Manevra

    }
    #endregion 
    [Serializable]
    public class Activity3Scene1InfoPanel
    {
        public GameObject Panel;
        public Text Header;
        public Text SpeedText;
        public Text DistanceText;
        public Text TimeText;
        public Button SubmitBtn;
        public string header;
        public float speed;
        public float distance;
        public float time;
        


        public delegate void ButtonAction();

        public void SetActive(bool param)
        {
            Panel.SetActive(param);
            if (param)
            {
                Header.text = header;
                SpeedText.text = speed.ToString();
                DistanceText.text = distance.ToString();
                TimeText.text = time.ToString();
            }
          
          
        }

        public void SetValue(string _header, float _speed, float _distance, float _time)
        {
            header = _header;
            speed = _speed;
            distance = _distance;
            time = _time;
        }

    }

}
