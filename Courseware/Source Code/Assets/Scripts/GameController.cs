﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(SceneController),typeof(MessageController))]
public class GameController : MonoBehaviour {
    [HideInInspector]
    public SceneController sceneController;
    [HideInInspector]
    public MessageController messageController;

    public bool isPaused;
    public bool isPlayable;
    
	// Use this for initialization
	void Start () {
        DontDestroyOnLoad(gameObject);
        sceneController = GetComponent<SceneController>();
        messageController = GetComponent<MessageController>();
        isPlayable = true;
	}
	public void Playable(bool s)
    {
        isPlayable = s;
     

    }
    // Update is called once per frame
    void Update () {
        // Debug.Log("TimeScale:" + Time.timeScale);
       // Debug.Log("isplayable:"+isPlayable);
	}

    public void PauseGame(bool paused, bool pausePanel = false)
    {
  
        Time.timeScale = (paused) ? 0 : 1;
        if (pausePanel)
            messageController.ShowPaused(paused);
        isPaused = (Time.timeScale == 0) ? true : false;
    }
    #region SoundInterface
    public void SoundMuteAll()
    {
      //  Debug.Log("SoundMuteAll");
    }
    public void SoundUnMuteAll()
    {
     //   Debug.Log("SoundUnMuteAll");
    }
    #endregion
}
