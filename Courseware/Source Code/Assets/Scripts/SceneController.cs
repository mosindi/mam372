﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class SceneController : MonoBehaviour
{
    GameController gc;
    LoaderController lc;
    void Start()
    {
        gc = HelperController.GetGC();
        lc = gc.GetComponent<LoaderController>();

    }
    public HelperController.Scenes GetSceneName()
    {
        return (HelperController.Scenes)SceneManager.GetActiveScene().buildIndex;
    }
    public void LoadScene(int id)
    {
        LoadScene((HelperController.Scenes)id);
    }

    public void LoadScene(HelperController.Scenes scene)
    {
        lc.LoadScene(scene);
    }
    public void LoadNextScene()
    {
        int currentBuildindex = SceneManager.GetActiveScene().buildIndex;
        int nextBuildindex = ++currentBuildindex;
        LoadScene((HelperController.Scenes)nextBuildindex);
    }


    public void LoadPrevScene()
    {
    }

    public void StartGame()
    {
        lc.LoadScene((HelperController.Scenes)SceneManager.GetActiveScene().buildIndex + 1);
    }

    public void EndGame()
    {
        HelperController.GameController.messageController.HideWarning();
        lc.LoadScene(HelperController.Scenes.MainMenu);
    }


    public void QuitGame()
    {
        Debug.Log("QuitGame");
#if UNITY_EDITOR
        UnityEditor.EditorApplication.isPlaying = false;
#else
        Application.Quit();
#endif
    }

}
