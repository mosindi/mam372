﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;
using System;

public class TaskController : MonoBehaviour {

    [HideInInspector]
    public int numberOfTask;
    public RectTransform TaskListPanel;
    public Button CloseTaskPanelButton;
    public GameObject TaskPrefab;

    List<TaskStruct> taskList = new List<TaskStruct>();
    List<GameObject> taskObjectList = new List<GameObject>();
    PanelManager pm;
    private bool TaskLoaded = false;


    private int m_OpenParameterId;
    const string k_OpenTransitionName = "Open";
    const string k_ClosedStateName = "Closed";

    void Start()
    {
       
        m_OpenParameterId = Animator.StringToHash(k_OpenTransitionName);
        pm = HelperController.PanelManager;
    }
    
    void OnEnable()
    {
        numberOfTask = taskList.Count;
     //   TaskListPanel.sizeDelta = new Vector2(TaskListPanel.sizeDelta.x, (taskList.Count * 120));
        if (!TaskLoaded)
        {
            GenerateTaskObjects();
        }
        //ClearTaskObjects();
        CloseTaskPanelButton.onClick.RemoveAllListeners();
        CloseTaskPanelButton.onClick.AddListener(
            delegate
            {
               
                pm.CloseCurrent();
                HelperController.GameController.PauseGame(false, false);
                //gameObject.SetActive(false);
            }
        );
        StartCoroutine(PauseGameDelayed());

    }
    void OnLevelWasLoaded(int level)
    {

        TaskListClear();
        ClearTaskObjects();
    }
    private IEnumerator PauseGameDelayed()
    {
        Animator anim = gameObject.GetComponent<Animator>();

        bool closedStateReached = false;
        bool wantToClose = true;
        while (!closedStateReached && wantToClose)
        {
            if (!anim.IsInTransition(0))
                closedStateReached = anim.GetCurrentAnimatorStateInfo(0).IsName(k_ClosedStateName);

            wantToClose = !anim.GetBool("Open");

            yield return new WaitForEndOfFrame();
        }
        yield return new WaitWhile(() => anim.IsInTransition(0));
        HelperController.GameController.PauseGame(true, false);
    }

    private void GenerateTaskObjects()
    {
       
        for (int i = 0; i < taskList.Count; i++)
        {
            GameObject task = Instantiate(TaskPrefab) as GameObject;
            task.transform.SetParent(TaskListPanel.transform,false);
         //   task.GetComponent<RectTransform>().pos
            taskObjectList.Add(task);
        }
        for (int i = 0; i < taskObjectList.Count; i++)
        {
            taskObjectList[i].GetComponentInChildren<Button>(true).interactable = taskList[i].isComplete;
            taskObjectList[i].GetComponentInChildren<Text>(true).text = taskList[i].TaskText;
        }
        TaskLoaded = true;
        TaskListPanel.sizeDelta = new Vector2(TaskListPanel.sizeDelta.x, (taskList.Count * 120));
    }
    private void ClearTaskObjects()
    {
        for (int i = 0; i < taskObjectList.Count; i++)
        {
            DestroyImmediate(taskObjectList[i]);
        }
        taskObjectList.Clear();
    }

    public void TaskAdd(TaskStruct task)
    {
            taskList.Add(task);
    }
    public void TasksAdd(List<TaskStruct> tasks)
    {
        for (int i = 0; i < tasks.Count; i++)
        {
            taskList.Add(tasks[i]);
        }
    }
    public void TaskListClear()
    {
        taskList.Clear();
    }
    public void TaskListFillOnSceneStart(List<TaskStruct> tasks)
    {
     //   Debug.Log("TaskListFillOnSceneStart");
        TaskListClear();
        ClearTaskObjects();
        TasksAdd(tasks);
        GenerateTaskObjects();
        
    }

    public void TaskComplete(int id)
    {
        TaskStruct temp = new TaskStruct(taskList[id].TaskText, true);
        taskList[id] = temp;
        taskObjectList[id].GetComponentInChildren<Button>(true).interactable = taskList[id].isComplete;
      
    }
    public struct TaskStruct
    {
        public string TaskText;
        
        public bool isComplete;
        public TaskStruct(string _t, bool _i =false)
        {
            TaskText = _t;
            isComplete = _i;
        }
    }

}
