﻿using UnityEngine;
using System.Collections;

public class ToolboxController : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
    }
    public void SetToolBoxActive(bool param)
    {
       // Debug.Log(HelperController.GameController.isPlayable);
        if (HelperController.GameController.isPlayable)
        {
            gameObject.SetActive(param);
        }
    }
    public void InsertTool(int id)
    {
        GameObject toolDesk = GameObject.FindGameObjectWithTag(HelperController.Tags.ToolDesk.ToString());
        if (toolDesk !=null)
        {
            toolDesk.BroadcastMessage("ToolInsert", id, SendMessageOptions.DontRequireReceiver);
        }
    }
}
