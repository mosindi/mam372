﻿using UnityEngine;
using System.Collections;

public class Scene1DoorController : MonoBehaviour {

    private Scene1Controller scene1Controller;
	// Use this for initialization
	void Start () {
        scene1Controller = GameObject.FindGameObjectWithTag(HelperController.Tags.SceneController.ToString()).GetComponent<Scene1Controller>();

	}
	
	// Update is called once per frame
	void Update () {
        
	}
    void OnMouseDown()
    {
        if (!HelperController.GameController.isPaused && HelperController.GameController.isPlayable)
        {
           // Debug.Log("OnMouseDown:" + gameObject.name);
            HelperController.GameController.sceneController.LoadNextScene();
        }
       
    }
}
