﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;

public class Scene1Controller : MonoBehaviour {
    public GameObject Objects3d;
    public GameObject objects2d;
    public Animator CamAnimator;
    float IntroAnimationDelayTime;
    bool sceneStarted = false;
    Scene1MessageController sceneMessageController;
    private int activeTaskID = 0;
	
    // Use this for initialization
	void Start () {
        sceneMessageController = GetComponent<Scene1MessageController>();
        CamAnimator = Camera.main.GetComponent<Animator>();

        IntroAnimationDelayTime = CamAnimator.GetCurrentAnimatorClipInfo(0)[0].clip.length;
        StartCoroutine(DelayedIntroAnimation(IntroAnimationDelayTime));
        TasksGenerate();

    }

    private IEnumerator DelayedIntroAnimation(float _time)
    {
        yield return new WaitForSeconds(_time);
        sceneStarted = true;
        Objects3d.SetActive(false);
        objects2d.SetActive(true);
        
        NextTask();

    }

    private void NextTask()
    {
        sceneMessageController.ShowMessage(HelperController.MessageType.Task, activeTaskID);
        activeTaskID++;
    }

    // Update is called once per frame
    void Update () {
        if (!sceneStarted)
        {
            return;
        }
     
    }
    public void OnClick(string s)
    {
        Debug.Log(s);
    }

    public List<TaskController.TaskStruct> taskList = new List<TaskController.TaskStruct>();

    public void TasksGenerate()
    {
        taskList.Add(new TaskController.TaskStruct("Kapıyı tıklayıp binaya giriş yap.", false));
        // taskList.Add(new TaskController.TaskStruct("Kapıyı tıklayıp binaya giriş yap.", false));
        HelperController.TaskController.TaskListFillOnSceneStart(taskList);

        
    }
}
