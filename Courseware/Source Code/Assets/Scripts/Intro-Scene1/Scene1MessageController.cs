﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;
using System;

public class Scene1MessageController : MonoBehaviour
{

    public Text HudBottomMessageText;
    public HudMessageStruct taskMessagePanel;
    public HudMessageStruct successMessagePanel;
    public HudMessageStruct FailulerMessagePanel;
    #region Lists
    public static Dictionary<int, string> ActivityMessageListTask = new Dictionary<int, string>()
    {
        {0,"Yeni Görev: Binaya girebilmek için kapıya tıkla!" },
        {1,"" },
        {2,"" },
    };
    public static Dictionary<int, string> ActivityMessageListSuccess = new Dictionary<int, string>()
    {
        {0,"Doğru cevap tebrikler!" },
        {1,"" },
        {2,"" },
    };
    public static Dictionary<int, string> ActivityMessageListFailure = new Dictionary<int, string>()
    {
        {0,"Yanlış, daha dikkatli olmalısın!" },
        {1,"" },
        {2,"" },
    };
    public static Dictionary<int, string> ActivityMessageListSpeech = new Dictionary<int, string>()
    {
        {0,"Merhaba" },
        {1,"" },
        {2,"" },
    };
    #endregion
    void Start()
    {
        HudBottomMessageText.text = "";
    }
    public void ShowMessage(HelperController.MessageType mType, int id = 0, HelperController.ButtonType buttonEvent = HelperController.ButtonType.ClosePanel)
    {

        switch (mType)
        {
            case HelperController.MessageType.Task:
                if (ActivityMessageListTask.Count > id)
                {
                    //HelperController.GameController.isPlayable = false;
                    ShowNewTaskNotify(id);
                    HudBottomMessageText.text = ActivityMessageListTask[id];
                }

                break;
            case HelperController.MessageType.Warning:
                break;
            case HelperController.MessageType.Success:
                break;
            case HelperController.MessageType.Failure:
                break;
            default:
                break;
        }
        AddEventListenerToButton(mType, buttonEvent);

    }

    private void AddEventListenerToButton(HelperController.MessageType mType, HelperController.ButtonType buttonEvent = HelperController.ButtonType.ClosePanel)
    {
        switch (mType)
        {
            case HelperController.MessageType.Task:
                taskMessagePanel.Button.onClick.RemoveAllListeners();
                switch (buttonEvent)
                {
                    case HelperController.ButtonType.ClosePanel:
                        taskMessagePanel.Button.onClick.AddListener(delegate
                        {
                            HelperController.GameController.isPlayable = true;
                            taskMessagePanel.Button.transform.parent.gameObject.SetActive(false);
                        });
                        break;
                    default:
                        break;
                }
                break;
            case HelperController.MessageType.Warning:
                break;
            case HelperController.MessageType.Success:
                break;
            case HelperController.MessageType.Failure:
                break;
            default:
                break;
        }
    }

    private void ShowNewTaskNotify(int id)
    {
       
        taskMessagePanel.Button.transform.parent.gameObject.SetActive(true);
        taskMessagePanel.Text.text = ActivityMessageListTask[id];
        HelperController.GameController.isPlayable = false;

    }

   
}
